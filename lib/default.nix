# This file defines some useful helper functions.
{ inputs, ... }:

let
  inherit (inputs.nixpkgs.lib) filterAttrs platforms;
  inherit (inputs.nixpkgs.lib.lists) elem;
  inherit (inputs.home-manager.lib) homeManagerConfiguration;
in
{
  mkHome =
    {
      modules ? [ ],
      pkgs,
      lib ? pkgs.lib,
      extraSpecialArgs ? { },
      check ? true,
    }:
    /**
      Synopsis: mkHome _args_

      Return a home-manager configuration using the given args.

      *
    */

    # Pass through the modules argument to ease sharing
    # home configurations with the NixOS module.
    {
      inherit modules;
    }
    //

      homeManagerConfiguration {
        inherit
          modules
          pkgs
          lib
          extraSpecialArgs
          check
          ;
      };

  filterSupportedPkgs =
    system: pkgs:
    /**
      Synopsis: filterSupportedPkgs _system_ _pkgs_

      Filter packages that support _system_ in the attribute set _pkgs_.
      *
    */
    let
      hasUnsupportedPlatform =
        attrs:
        (
          !elem system (attrs.meta.platforms or platforms.all) || elem system (attrs.meta.badPlatforms or [ ])
        );
    in
    filterAttrs (_name: value: !hasUnsupportedPlatform value) pkgs;
}
