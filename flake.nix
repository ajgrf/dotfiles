{
  description = "Alex Griffin's system and home configurations";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    unstable.url = "github:NixOS/nixpkgs/nixos-unstable";

    agenix.url = "github:ryantm/agenix";
    agenix.inputs.nixpkgs.follows = "nixpkgs";
    agenix.inputs.home-manager.follows = "home-manager";
    agenix.inputs.systems.follows = "systems";

    disko.url = "github:nix-community/disko/latest";
    disko.inputs.nixpkgs.follows = "nixpkgs";

    emacs.url = "github:nix-community/emacs-overlay";
    emacs.inputs.nixpkgs.follows = "unstable";
    emacs.inputs.nixpkgs-stable.follows = "nixpkgs";

    flake-utils.url = "github:numtide/flake-utils";
    flake-utils.inputs.systems.follows = "systems";

    hardware.url = "github:NixOS/nixos-hardware";

    home-manager.url = "github:nix-community/home-manager/release-24.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    impermanence.url = "github:nix-community/impermanence";

    lanzaboote.url = "github:nix-community/lanzaboote/v0.4.2";
    lanzaboote.inputs.nixpkgs.follows = "nixpkgs";
    lanzaboote.inputs.flake-compat.follows = "flake-compat";

    lix-module.url = "https://git.lix.systems/lix-project/nixos-module/archive/2.92.0.tar.gz";
    lix-module.inputs.nixpkgs.follows = "nixpkgs";
    lix-module.inputs.flake-utils.follows = "flake-utils";

    nix-index-database.url = "github:Mic92/nix-index-database";
    nix-index-database.inputs.nixpkgs.follows = "nixpkgs";

    plasma-manager.url = "github:nix-community/plasma-manager";
    plasma-manager.inputs.nixpkgs.follows = "nixpkgs";
    plasma-manager.inputs.home-manager.follows = "home-manager";

    quickemu.url = "https://flakehub.com/f/quickemu-project/quickemu/*.tar.gz";
    quickemu.inputs.nixpkgs.follows = "nixpkgs";
    quickemu.inputs.flake-schemas.follows = "flake-schemas";

    wsl.url = "github:nix-community/NixOS-WSL";
    wsl.inputs.nixpkgs.follows = "nixpkgs";
    wsl.inputs.flake-compat.follows = "flake-compat";

    # Transitive dependencies (listed here for deduplicating inputs):
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-schemas.url = "https://flakehub.com/f/DeterminateSystems/flake-schemas/*.tar.gz";
    systems.url = "github:nix-systems/default";
  };

  outputs =
    inputs@{
      self,
      nixpkgs,
      home-manager,
      agenix,
      flake-utils,
      ...
    }:
    let
      supportedSystems = [ "x86_64-linux" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
    in
    rec {
      # Custom helper functions.
      lib = import ./lib { inherit inputs; };

      # Use nixfmt to format this flake (used by `nix fmt`).
      formatter = forAllSystems (system: legacyPackages.${system}.nixfmt-rfc-style);

      # Reusable NixOS modules to export.
      nixosModules = import ./nixos/modules;
      # Reusable home-manager modules to export.
      homeManagerModules = import ./home/modules;

      # Shell for bootstrapping flake-enabled nix and home-manager.
      # Accessible through `nix develop` or `nix-shell` (legacy).
      devShells = forAllSystems (system: rec {
        default =
          with legacyPackages.${system};
          (callPackage ./shell.nix { }).overrideAttrs (old: {
            nativeBuildInputs = old.nativeBuildInputs ++ [
              agenix.packages.${system}.default
              nil
            ];
          });
        bootstrap = nixpkgs.legacyPackages.${system}.callPackage ./shell.nix { };
        clojureDev =
          with legacyPackages.${system};
          mkShell {
            nativeBuildInputs = [
              babashka
              clojure
              clojure-lsp
              cljfmt
              jet
              neil
            ];
          };
        pymongoDev =
          with legacyPackages.${system};
          mkShell {
            nativeBuildInputs = [
              (python3.withPackages (
                python-pkgs: with python-pkgs; [
                  pymongo
                  python-dotenv
                ]
              ))
            ];
          };
      });

      # Custom packages and modifications.
      overlays = {
        default = import ./overlays { inherit inputs; };
        unstable = import ./overlays/unstable.nix { inherit inputs; };
      };

      # Instantiate nixpkgs with the given overlays and configuration.
      legacyPackages = forAllSystems (
        system:
        import nixpkgs {
          inherit system;
          overlays = builtins.attrValues overlays;

          config.allowUnfree = true;
        }
      );

      # Export custom packages.
      packages = forAllSystems (
        system:
        lib.filterSupportedPkgs system (
          import ./pkgs {
            pkgs = import nixpkgs {
              inherit system;
              overlays = [ ];
              config.allowUnfree = true;
            };
          }
        )
      );

      # TODO: generate nixos live installer images integrated with my dotfiles
      nixosConfigurations = {
        # TODO: replace renovate updates with self-hosted cron/ci job
        staunch-coral = nixpkgs.lib.nixosSystem {
          pkgs = legacyPackages.x86_64-linux;
          system = "x86_64-linux";
          specialArgs = {
            inherit inputs;
          };
          modules = (builtins.attrValues nixosModules) ++ [ ./nixos/hosts/staunch-coral ];
        };

        adroit-albacore = nixpkgs.lib.nixosSystem {
          pkgs = legacyPackages.x86_64-linux;
          system = "x86_64-linux";
          specialArgs = {
            inherit inputs;
          };
          modules = (builtins.attrValues nixosModules) ++ [ ./nixos/hosts/adroit-albacore ];
        };

        kindred-wren = nixpkgs.lib.nixosSystem {
          pkgs = legacyPackages.x86_64-linux;
          system = "x86_64-linux";
          specialArgs = {
            inherit inputs;
          };
          modules = (builtins.attrValues nixosModules) ++ [ ./nixos/hosts/kindred-wren ];
        };

        renewing-anemone = nixpkgs.lib.nixosSystem {
          pkgs = legacyPackages.x86_64-linux;
          system = "x86_64-linux";
          specialArgs = {
            inherit inputs;
          };
          modules = (builtins.attrValues nixosModules) ++ [ ./nixos/hosts/renewing-anemone ];
        };
      };

      homeConfigurations = {
        axgfn = lib.mkHome {
          pkgs = legacyPackages.x86_64-linux;
          extraSpecialArgs = {
            inherit inputs;
          };
          modules = (builtins.attrValues homeManagerModules) ++ [ ./home/profiles/home.nix ];
        };

        "axgfn@staunch-coral" = lib.mkHome {
          pkgs = legacyPackages.x86_64-linux;
          extraSpecialArgs = {
            inherit inputs;
          };
          modules = (builtins.attrValues homeManagerModules) ++ [ ./home/profiles/staunch-coral.nix ];
        };

        "axgfn@adroit-albacore" = lib.mkHome {
          pkgs = legacyPackages.x86_64-linux;
          extraSpecialArgs = {
            inherit inputs;
          };
          modules = (builtins.attrValues homeManagerModules) ++ [ ./home/profiles/adroit-albacore.nix ];
        };

        "axgfn@kindred-wren" = lib.mkHome {
          pkgs = legacyPackages.x86_64-linux;
          extraSpecialArgs = {
            inherit inputs;
          };
          modules = (builtins.attrValues homeManagerModules) ++ [ ./home/profiles/kindred-wren.nix ];
        };

        "axgfn@renewing-anemone" = lib.mkHome {
          pkgs = legacyPackages.x86_64-linux;
          extraSpecialArgs = {
            inherit inputs;
          };
          modules = (builtins.attrValues homeManagerModules) ++ [ ./home/profiles/renewing-anemone.nix ];
        };
      };

      # TODO: support macos home configurations and nix-darwin

      checks = forAllSystems (
        system:
        let
          inherit (nixpkgs.lib.attrsets) filterAttrs mapAttrs' nameValuePair;
          checkPackages = mapAttrs' (name: pkg: nameValuePair "package-${name}" pkg) packages.${system};
          checkShells = mapAttrs' (name: shell: nameValuePair "devShell-${name}" shell) devShells.${system};
          checkHosts = mapAttrs' (
            name: host: nameValuePair "nixos-${name}" host.config.system.build.toplevel
          ) (filterAttrs (_name: host: host.pkgs.system == system) nixosConfigurations);
          checkHomes = mapAttrs' (name: home: nameValuePair "home-manager-${name}" home.activationPackage) (
            filterAttrs (_name: home: home.pkgs.system == system) homeConfigurations
          );
        in
        checkPackages // checkShells // checkHosts // checkHomes
      );

      apps = forAllSystems (
        system:
        let
          inherit (flake-utils.lib) mkApp;
          inherit (nixpkgs.lib.strings) concatMapStrings;
          outputs = concatMapStrings (name: ".#checks.${system}.${name} ") (
            builtins.attrNames checks.${system}
          );
        in
        {
          cache = mkApp {
            drv = legacyPackages.${system}.writeShellScriptBin "cache" ''
              nix build --no-link --print-out-paths ${outputs} | cachix push axgfn
            '';
          };
        }
      );

    };
}
