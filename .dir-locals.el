;; Per-directory local variables for GNU Emacs 23 and later.

((nil
  . ((compile-command . "home-manager --flake . switch ")
     (eval . (when (fboundp 'flymake-mode)
               (flymake-mode -1)))
     (eval . (let ((shell-regexp
                    (rx "dot_" (zero-or-more (any alpha "_"))
                        (or "shrc" "shinit" "shenv" "profile" "profile_local" "login" "logout")
                        string-end)))
               (add-to-list 'auto-mode-alist `(,shell-regexp . sh-mode))))
     (eval . (add-to-list 'auto-mode-alist
                          '("\\.tmpl\\'" nil t))))))
