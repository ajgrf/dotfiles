{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

with lib;

let
  cfg = config.services.protonmail-bridge;
in
{
  options.services.protonmail-bridge = {
    enable = mkEnableOption "protonmail-bridge daemon";

    autoStart = mkOption {
      type = types.bool;
      default = true;
      description = "Whether protonmail-bridge should be started automatically.";
    };

    package = mkOption {
      type = types.package;
      default = pkgs.protonmail-bridge;
      description = "The package to use for protonmail-bridge.";
    };
  };

  config = lib.mkIf cfg.enable {
    # Install protonmail-bridge
    home.packages = [ cfg.package ];

    systemd.user.services.protonmail-bridge = {
      Unit.Description = "Proton Mail Bridge";
      Install.WantedBy = lib.optionals cfg.autoStart [ "default.target" ];
      Service = {
        Type = "simple";
        StandardOutput = "journal";
        ExecStart = "${cfg.package}/bin/protonmail-bridge --noninteractive";
      };
    };
  };
}
