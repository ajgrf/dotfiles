{
  chezmoi = ./programs/chezmoi.nix;
  vim-pack = ./programs/vim-pack.nix;
  protonmail-bridge = ./services/protonmail-bridge.nix;
  base-feature = ./features/base.nix;
  desktop-feature = ./features/desktop.nix;
  emacs-feature = ./features/emacs.nix;
  fonts-feature = ./features/fonts.nix;
  mail-feature = ./features/mail.nix;
  plasma-feature = ./features/plasma.nix;
  wsl-feature = ./features/wsl.nix;
}
