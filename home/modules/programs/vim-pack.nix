{
  config,
  pkgs,
  lib,
  ...
}:

with lib;

let
  cfg = config.programs.vim.pack;
in
{
  options.programs.vim.pack = {
    packDir = mkOption {
      type = types.str;
      default = ".vim";
      description = "Directory in $HOME to place packages.";
    };

    plugins = mkOption {
      type = with types; listOf package;
      default = [ ];
      description = "List of vim plugins to place into packDir.";
    };
  };

  config = lib.mkIf (cfg.plugins != [ ]) {
    home.file = builtins.foldl' (
      fileSet: plugin:
      fileSet // { "${cfg.packDir}/pack/home-manager/start/${plugin.name}".source = plugin; }
    ) { } cfg.plugins;
  };
}
