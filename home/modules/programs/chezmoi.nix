{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:

with lib;

let
  cfg = config.programs.chezmoi;
  settingsFormat = pkgs.formats.toml { };

  configFile = settingsFormat.generate "chezmoi.toml" cfg.settings;

  destDir = pkgs.runCommand "chezmoi-directory" { preferLocalBuild = true; } ''
    HOME=${config.home.homeDirectory}
    mkdir -p $out
    ${cfg.package}/bin/chezmoi apply \
      --config ${configFile} \
      --source ${cfg.sourceDir} \
      --destination $out
  '';
in
{
  options.programs.chezmoi = {
    enable = mkEnableOption "chemzoi";

    package = mkOption {
      type = types.package;
      default = pkgs.chezmoi;
      description = "The chezmoi package to use.";
    };

    sourceDir = mkOption {
      type = types.path;
      description = "The source directory to use for generating dotfiles.";
    };

    settings = mkOption {
      type = settingsFormat.type;
      default = { };
      description = "Settings to include in chezmoi's configuration file.";
    };
  };

  config = lib.mkIf cfg.enable {
    # Generate attribute set of files to link into the user home.
    home.file = builtins.listToAttrs (
      let
        destFiles = (builtins.readDir destDir);
      in
      builtins.map (filename: {
        name = filename;
        value = {
          source = destDir + "/" + filename;
          target = ("./" + filename);
          recursive = (destFiles.${filename} == "directory");
        };
      }) (builtins.attrNames destFiles)
    );
  };
}
