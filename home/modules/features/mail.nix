# This feature module installs and configures email utilities.
{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:

let
  cfg = config.features.mail;
in
{
  options.features.mail.enable = lib.mkEnableOption "mail feature";

  config = lib.mkIf cfg.enable {
    # Enable protonmail-bridge service.
    services.protonmail-bridge = {
      enable = true;
      autoStart = false;
      package = pkgs.newerPkg.protonmail-bridge;
    };

    home.packages = with pkgs; [
      isync
      khal
      khard
      mblaze
      msmtp
      mu
      vdirsyncer
    ];
  };
}
