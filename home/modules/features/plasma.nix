# This feature module configures Plasma desktop.
{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:

# BUG: logging out doesn't work

let
  cfg = config.features.plasma;
in
{
  imports = [ inputs.plasma-manager.homeManagerModules.plasma-manager ];

  options.features.plasma = with lib; {
    enable = mkEnableOption "plasma feature";

    # Plasma's digital clock really doesn't want to use a consistent font size.
    clockFontSize = mkOption {
      type = types.int;
      default = 9;
      description = "The font size used for Plasma's digital clock.";
    };

    gapSize = mkOption {
      type = types.int;
      default = 8;
      description = "The size of the gaps between windows under Karousel.";
    };
  };

  config = lib.mkIf cfg.enable {
    programs.plasma = {
      enable = true;

      # Discard changes made outside plasma-manager.
      overrideConfig = true;

      # Update Plasma desktop wallpaper with Bing's picture of the day.
      workspace.wallpaperPictureOfTheDay = {
        provider = "bing";
      };

      # Set matching lock screen wallpaper.
      kscreenlocker.appearance.wallpaperPictureOfTheDay = {
        provider = "bing";
      };

      # Disable splash screen.
      workspace.splashScreen.theme = "None";

      # Use 2x2 grid of virtual desktops.
      kwin.virtualDesktops = {
        number = 4;
        rows = 2;
      };

      # Configure desktop icons.
      desktop.icons = {
        alignment = "left";
        arrangement = "topToBottom";
        folderPreviewPopups = true;
        lockInPlace = true;
        size = 3;
        sorting = {
          descending = false;
          foldersFirst = true;
          mode = "name";
        };
      };

      panels = [
        {
          floating = false;
          height = 32;
          location = "top";
          widgets = [
            "org.kde.plasma.kickoff"
            "org.kde.plasma.pager"
            {
              iconTasks = {
                launchers = [
                  "applications:firefox.desktop"
                  "applications:emacsclient.desktop"
                  "applications:beeper.desktop"
                ];
                behavior.showTasks.onlyInCurrentDesktop = false;
              };
            }
            "org.kde.plasma.panelspacer"
            {
              digitalClock = {
                date = {
                  format.custom = "ddd MMM d  ";
                  position = "besideTime";
                };
                font = {
                  family = "Sans Serif";
                  size = cfg.clockFontSize;
                  weight = 400;
                };
              };
            }
            "org.kde.plasma.panelspacer"
            {
              systemTray = {
                items.shown = [
                  "org.kde.plasma.battery"
                  "org.kde.plasma.bluetooth"
                ];
              };
            }
            "org.kde.plasma.lock_logout"
          ];
        }
      ];

      shortcuts = {
        kwin = {
          "karousel-grid-scroll-focused" = "Meta+Return";
          "karousel-focus-next" = "Meta+J";
          "karousel-focus-previous" = "Meta+K";
          "karousel-column-width-decrease" = "Meta+H";
          "karousel-column-width-increase" = "Meta+L";
          "karousel-column-move-right" = "Meta+Shift+J";
          "karousel-column-move-left" = "Meta+Shift+K";
          "karousel-window-move-previous" = "Meta+Shift+H";
          "karousel-window-move-next" = "Meta+Shift+L";
          "karousel-grid-scroll-right-column" = "Meta+Ctrl+Alt+PgDown";
          "karousel-grid-scroll-left-column" = "Meta+Ctrl+Alt+PgUp";

          "karousel-focus-1" = [ ];
          "karousel-focus-2" = [ ];
          "karousel-focus-3" = [ ];
          "karousel-focus-4" = [ ];
          "karousel-focus-5" = [ ];
          "karousel-focus-6" = [ ];
          "karousel-focus-7" = [ ];
          "karousel-focus-8" = [ ];
          "karousel-focus-9" = [ ];

          "Window Quick Tile Top" = [ ];
          "Window Quick Tile Top Right" = [ ];
          "Window Quick Tile Top Left" = [ ];
          "Window Quick Tile Bottom" = [ ];
          "Window Quick Tile Bottom Right" = [ ];
          "Window Quick Tile Bottom Left" = [ ];

          "Switch One Desktop to the Right" = [
            "Meta+Ctrl+Right"
            "Ctrl+Alt+Right"
          ];
          "Switch One Desktop to the Left" = [
            "Meta+Ctrl+Left"
            "Ctrl+Alt+Left"
          ];
          "Switch One Desktop Up" = [
            "Meta+Ctrl+Up"
            "Ctrl+Alt+Up"
          ];
          "Switch One Desktop Down" = [
            "Meta+Ctrl+Down"
            "Ctrl+Alt+Down"
          ];

          "Window One Desktop to the Right" = [
            "Meta+Ctrl+Shift+Right"
            "Ctrl+Alt+Shift+Right"
          ];
          "Window One Desktop to the Left" = [
            "Meta+Ctrl+Shift+Left"
            "Ctrl+Alt+Shift+Left"
          ];
          "Window One Desktop Up" = [
            "Meta+Ctrl+Shift+Up"
            "Ctrl+Alt+Shift+Up"
          ];
          "Window One Desktop Down" = [
            "Meta+Ctrl+Shift+Down"
            "Ctrl+Alt+Shift+Down"
          ];

          "Window Maximize" = [
            "Meta+PgUp"
            "Meta+Up"
          ];
          "Window Minimize" = [
            "Meta+PgDown"
            "Meta+Down"
          ];
          "Window Close" = [
            "Alt+F4"
            "Meta+Backspace"
          ];

          # These shortcuts conflict with popper-cycle in Emacs.
          "Walk Through Windows of Current Application" = [ ];
          "Walk Through Windows of Current Application (Reverse)" = [ ];
        };

        ksmserver = {
          "Lock Session" = "Ctrl+Alt+L";
        };
      };

      hotkeys.commands = {
        "Alacritty" = {
          name = "Alacritty";
          key = "Meta+Shift+Return";
          command = "alacritty";
        };
        "org-capture" = {
          name = "org-capture";
          key = "Meta+n";
          command = "${config.home.homeDirectory}/.local/bin/org-capture";
        };
      };

      window-rules = [
        {
          description = "Dark titlebar for mpv";
          match = {
            window-class = {
              value = "mpv";
              type = "exact";
              match-whole = false;
            };
          };
          apply = {
            decocolor = {
              value = "BreezeDark";
              apply = "force";
            };
          };
        }
        {
          description = "Dark titlebar for Alacritty";
          match = {
            window-class = {
              value = "Alacritty";
              type = "exact";
              match-whole = false;
            };
          };
          apply = {
            decocolor = {
              value = "BreezeDark";
              apply = "force";
            };
          };
        }
      ];

      # Disable bouncing cursor.
      configFile.klaunchrc = {
        BusyCursorSettings = {
          Bouncing = false;
        };

        FeedbackStyle = {
          BusyCursor = false;
        };
      };

      # Disable file indexing service.
      configFile.baloofilerc = {
        "Basic Settings" = {
          Indexing-Enabled = false;
        };
      };

      # Remove unused titlebar buttons.
      kwin = {
        titlebarButtons.left = [ ];
        titlebarButtons.right = [ "close" ];
      };

      # Configure Karousel, a scrollable tiling KWin script.
      configFile.kwinrc = {
        Plugins = {
          karouselEnabled = true;
          kwin4_effect_geometry_changeEnabled = true;
          diminactiveEnabled = true;
        };

        Effect-diminactive = {
          Strength = 20;
        };

        Windows = {
          DelayFocusInterval = 0;
          FocusPolicy = "FocusFollowsMouse";
          NextFocusPrefersMouse = true;
        };

        Script-karousel = {
          gapsInnerHorizontal = cfg.gapSize;
          gapsInnerVertical = cfg.gapSize;
          gapsOuterTop = cfg.gapSize;
          gapsOuterBottom = cfg.gapSize;
          gapsOuterLeft = cfg.gapSize;
          gapsOuterRight = cfg.gapSize;
          manualResizeStep = 200;
          windowRules = builtins.toJSON [
            {
              class = "ksmserver-logout-greeter";
              tile = false;
            }
            {
              class = "xwaylandvideobridge";
              tile = false;
            }
            {
              class = "(org\\.kde\\.)?plasmashell";
              tile = false;
            }
            {
              class = "(org\\.kde\\.)?kded6";
              tile = false;
            }
            {
              class = "(org\\.kde\\.)?krunner";
              tile = false;
            }
            {
              class = "emacs";
              caption = "org-capture";
              tile = false;
            }
          ];
        };
      };

    };

    home.packages = with pkgs; [
      kdePackages.karousel
      kwin4-effect-geometry-change
    ];
  };
}
