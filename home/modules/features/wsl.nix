# This feature module contains configuration and options for a WSL2 home environment.
{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:

let
  cfg = config.features.wsl;
in
{
  options.features.wsl.enable = lib.mkEnableOption "wsl feature";

  config = lib.mkIf cfg.enable {
    # Enable additional configuration from feature modules.
    features = {
      base.enable = true;
      fonts.enable = true;
      mail.enable = true;
    };

    # Include WSL configuration from chezmoi.
    programs.chezmoi.settings.data.wsl = true;

    # Use Emacs with pgtk support, because WSL2 works better with Wayland apps.
    programs.emacs.package = pkgs.emacs30-pgtk;

    # Some extra miscellaneous packages to install.
    home.packages = with pkgs; [
      mpv
      wslu
    ];

    # I can't get theme changes to live reload, so just use the
    # dark theme all the time.
    gtk = {
      enable = true;
      theme.name = "Adwaita-dark";
    };

    # Create some convenient symlinks to the Windows host.
    home.activation.linkWSLDirectories = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
      WINHOME="/mnt/c/Users/Alex"
      if [[ -d "$WINHOME/Desktop" && ! -L "$HOME/Desktop" ]]; then
        $DRY_RUN_CMD ln $VERBOSE_ARG --no-target-directory -f -s "$WINHOME/Desktop" "$HOME/Desktop"
      fi
      if [[ -d "/mnt/z" && ! -L "$HOME/Share/Egnyte" ]]; then
        $DRY_RUN_CMD mkdir $VERBOSE_ARG -p "$HOME/Share"
        $DRY_RUN_CMD ln $VERBOSE_ARG --no-target-directory -f -s "/mnt/z" "$HOME/Share/Egnyte"
      fi
      if [[ -d "$WINHOME/Org" && ! -L "$HOME/Org" ]]; then
        $DRY_RUN_CMD ln $VERBOSE_ARG --no-target-directory -f -s "$WINHOME/Org" "$HOME/Org"
      fi
    '';
  };
}
