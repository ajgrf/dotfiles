# This feature module contains configuration and options for desktop systems.
{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:

let
  cfg = config.features.desktop;
in
{
  options.features.desktop.enable = lib.mkEnableOption "desktop feature";

  config = lib.mkIf cfg.enable {
    # Enable additional configuration from feature modules.
    features = {
      base.enable = true;
      fonts.enable = true;
      fonts.extraFonts = true;
      mail.enable = true;
      plasma.enable = true;
    };

    # Include desktop config files.
    programs.chezmoi.settings.data.linuxDesktop = true;

    home.sessionVariables = {
      # Enable Wayland support in Chromium- and Electron-based apps.
      "NIXOS_OZONE_WL" = "1";
      # Force Breeze theme for GTK apps now that Adwaita-dark is gone.
      "GTK_THEME" = "Breeze";
      # Set cursor theme for applications like Alacritty.
      "XCURSOR_THEME" = "Breeze";
    };

    # Enable light- and dark-mode transitions without a desktop environment.
    services.darkman = {
      enable = true;

      settings = {
        dbusserver = true;
        portal = true;
        usegeoclue = false;
      };

      lightModeScripts = {
        plasma-theme = ''
          lookandfeeltool -platform offscreen --apply "org.kde.breeze.desktop"
          plasma-apply-desktoptheme breeze-dark
        '';
      };

      darkModeScripts = {
        plasma-theme = ''
          lookandfeeltool -platform offscreen --apply "org.kde.breezedark.desktop"
        '';
      };
    };

    # Sync files with Nextcloud.
    services.nextcloud-client.enable = true;

    # Display notification when Yubikey is waiting for touch.
    xdg.configFile."systemd/user/graphical-session.target.wants/yubikey-touch-detector.service".source = "${pkgs.yubikey-touch-detector}/share/systemd/user/yubikey-touch-detector.service";
    xdg.configFile."systemd/user/yubikey-touch-detector.socket".source = "${pkgs.yubikey-touch-detector}/share/systemd/user/yubikey-touch-detector.socket";

    # Use Emacs with pgtk support for better Wayland support.
    # Also remove plain Emacs desktop entries to fix Plasma taskbar pinning.
    programs.emacs.package = pkgs.emacs30-pgtk.overrideAttrs (old: {
      postInstall =
        (old.postInstall or "")
        + "\n"
        + ''
          rm $out/share/applications/emacs.desktop
          rm $out/share/applications/emacs-mail.desktop
        '';
    });

    # Extra packages to install on desktop systems.
    home.packages = with pkgs; [
      # applications
      alacritty
      newerPkg.anki
      beets
      newerPkg.beeper
      newerPkg.bitwarden
      newerPkg.brave
      catt
      newerPkg.ungoogled-chromium
      newerPkg.digikam
      newerPkg.electrum
      dconf-editor
      kdePackages.kasts
      kdePackages.kdeconnect-kde
      koreader
      ktailctl
      kubectl
      kubernetes-helm
      libreoffice
      (newerPkg.firefox.override {
        nativeMessagingHosts = [
          kdePackages.plasma-browser-integration
          tridactyl-native
        ];
      })
      minikube
      (mpv.override {
        scripts = with mpvScripts; [
          autocrop
          chapterskip
          modernx-zydezu
          mpris
          mpvacious
          thumbfast
        ];
      })
      pdfbook
      poppler_utils
      quickemu
      quodlibet-full
      remmina
      sacad
      signal-desktop
      newerPkg.tor-browser-bundle-bin
      transmission_4-gtk
      virt-manager
      yubikey-manager-qt
      yubikey-touch-detector

      # games
      steam
      lutris
    ];

    # HACK: Fix minikube kvm2 driver.
    home.file = {
      ".minikube/bin/docker-machine-driver-kvm2".source = "${pkgs.docker-machine-kvm2}/bin/docker-machine-driver-kvm2";
    };

    # Create some convenient symlinks to the Windows host.
    home.activation.linkWSLDirectories = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
      if [[ -d "/mnt/egnyte" && ! -L "$HOME/Share/Egnyte" ]]; then
        $DRY_RUN_CMD mkdir $VERBOSE_ARG -p "$HOME/Share"
        $DRY_RUN_CMD ln $VERBOSE_ARG --no-target-directory -f -s "/mnt/egnyte" "$HOME/Share/Egnyte"
      fi
    '';
  };
}
