{
  config,
  pkgs,
  lib,
  ...
}:

let
  cfg = config.features.emacs;
in
{
  options.features.emacs.enable = lib.mkEnableOption "emacs feature";

  config = lib.mkIf cfg.enable {
    programs.emacs.package = lib.mkDefault pkgs.emacs30;

    home.packages = with pkgs; [
      (emacsWithPackagesFromUsePackage {
        package = config.programs.emacs.package;
        config =
          builtins.readFile
            (pkgs.runCommand "emacs-config" { preferLocalBuild = true; } ''
              cat ${../../../config/dot_config/emacs/exact_lisp}/* >$out
            '').outPath;
        extraEmacsPackages =
          epkgs: with epkgs; [
            mu.mu4e
            treesit-grammars.with-all-grammars
          ];
      })

      aspell
      aspellDicts.en
      aspellDicts.en-computers
      aspellDicts.en-science
      exiftool
      fd
      ffmpegthumbnailer
      git
      imagemagick
      mediainfo
      ripgrep
      sqlite
      unzip # for nov.el
    ];

    # Point aspell at my installed dictionaries.
    home.sessionVariables = {
      "ASPELL_CONF" = "dict-dir ${config.home.profileDirectory}/lib/aspell";
    };
  };
}
