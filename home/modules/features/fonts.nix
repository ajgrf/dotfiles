# This feature module installs and configures fonts for display.
{
  config,
  pkgs,
  lib,
  ...
}:

let
  cfg = config.features.fonts;
in
{
  options.features.fonts = {
    enable = lib.mkEnableOption "fonts feature";

    extraFonts = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = "Whether to install a more expansive selection of fonts.";
    };
  };

  config = lib.mkIf cfg.enable {
    # Allow fontconfig to discover fonts installed through nix.
    fonts.fontconfig.enable = true;

    # Fonts to install.
    home.packages =
      with pkgs;
      [
        go-font
        newerPkg.iosevka-flare
        nerdfonts-symbols
        source-sans
        source-han-mono
      ]
      ++ lib.optionals cfg.extraFonts [
        alegreya
        alegreya-sans
        b612
        cantarell-fonts
        charis-sil
        comic-neue
        cooper-hewitt
        dejavu_fonts
        eb-garamond
        etBook
        fira
        fira-mono
        gentium
        ibm-plex
        iosevka-bin
        (iosevka-bin.override { variant = "SS08"; })
        (iosevka-bin.override { variant = "Slab"; })
        (iosevka-bin.override { variant = "Aile"; })
        (iosevka-bin.override { variant = "Etoile"; })
        jost
        lato
        liberation_ttf
        merriweather
        merriweather-sans
        montserrat
        noto-fonts
        open-sans
        paratype-pt-mono
        paratype-pt-sans
        paratype-pt-serif
        raleway
        roboto
        roboto-mono
        roboto-slab
        source-code-pro
        source-serif
        source-han-sans
        source-han-serif
        ubuntu_font_family
        vollkorn
      ];
  };
}
