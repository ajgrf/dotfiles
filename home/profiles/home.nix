# This file defines a minimal home environment containing only CLI utilities.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

{
  # Enable additional configuration from feature modules.
  features.base.enable = true;
}
