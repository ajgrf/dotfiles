# This file contains configuration for a NixOS GUI home environment.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

{
  # Enable additional configuration from feature modules.
  features.desktop.enable = true;
}
