# This file contains configuration specific to my NAS.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

{
  # Enable additional configuration from feature modules.
  features.base.enable = true;
}
