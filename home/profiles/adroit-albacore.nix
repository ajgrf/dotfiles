# This file contains configuration for a NixOS GUI home environment.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

{
  # Enable additional configuration from feature modules.
  features.desktop.enable = true;

  # Scale interface for HiDPI screen.
  scalingFactor = 1.5;

  # Adjust digital clock font for the Framework's screen.
  features.plasma.clockFontSize = 7;
  # Use smaller gaps between windows too.
  features.plasma.gapSize = 4;

  # Enable natural scrolling on touchpad.
  programs.plasma.configFile.kcminputrc = {
    "Libinput/2362/628/PIXA3854:00 093A:0274 Touchpad" = {
      NaturalScroll = true;
    };
  };
}
