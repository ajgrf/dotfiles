# This file contains configuration for a WSL2 home environment.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

{
  # Enable additional configuration from feature modules.
  features.wsl.enable = true;
}
