# My Dotfiles

My personal dotfiles and operating system configurations, managed with
[Nix][nix], [Home Manager][home-manager], and [chezmoi][].

## Bootstrapping

### Nix

This repository is structured as a [Nix flake][flakes]. Run
`nix-shell` to enter a development environment with a flake-enabled
`nix` and `home-manager.`

Then run `nixos-rebuild switch --flake .` to build system
configurations, or `home-manager switch --flake .` for user
environments. Adding symlinks to `flake.nix` at `/etc/nixos/flake.nix`
and `~/.config/home-manager/flake.nix` will let you omit the flake
from these commands.

### chezmoi

On Unix-likes:

``` bash
# With curl:
sh -c "$(curl -fsLS get.chezmoi.io)" -- init -a -S ~/Projects/dotfiles gitlab.com/axgfn

# With wget:
sh -c "$(wget -qO- get.chezmoi.io)" -- init -a -S ~/Projects/dotfiles gitlab.com/axgfn
```

On Windows, first run `Set-ExecutionPolicy Unrestricted` as
Administrator to allow executing scripts. Then as a normal user:

``` powershell
iex "&{$(irm 'https://get.chezmoi.io/ps1')} -- init -a -S ~\Projects\dotfiles gitlab.com/axgfn"
```

## References

- [nix-starter-configs][] for an exceptional flake template.

[nix]: https://nixos.org/
[home-manager]: https://github.com/nix-community/home-manager
[chezmoi]: https://www.chezmoi.io/
[flakes]: https://nixos.wiki/wiki/Flakes
[nix-starter-configs]: https://github.com/Misterio77/nix-starter-configs
