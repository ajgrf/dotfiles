# This file defines an overlay to make the unstable channel
# available everywhere.
{ inputs, ... }:

final: prev: rec {
  unstable = import inputs.unstable {
    inherit (prev) config system;
    overlays = [ inputs.self.overlays.default ];
  };

  # Automatically select the newer version of a package from the stable or
  # unstable channel, preferring the stable package in case they're equal.
  newerPkg =
    let
      inherit (inputs.nixpkgs.lib) versionOlder;
      inherit (inputs.nixpkgs.lib.attrsets) attrByPath mapAttrsRecursiveCond;

      stable = import inputs.nixpkgs {
        inherit (prev) config system;
        overlays = [ inputs.self.overlays.default ];
      };

      cond = (as: !(as ? "type" && as.type == "derivation"));

      f =
        path: value:
        let
          pkgA = value;
          pkgB = attrByPath path null unstable;
        in
        if versionOlder pkgA.version pkgB.version then pkgB else pkgA;
    in
    mapAttrsRecursiveCond cond f stable;
}
