# This file defines an overlay for including custom packages
# or modifying existing packages in nixpkgs.
{ inputs, ... }:

let
  # Include custom packages defined in the 'pkgs' directory.
  additions = final: _prev: import ../pkgs { pkgs = final; };

  # Other overlays that modify nixpkgs.
  modifications = final: prev: {
    beeper = prev.beeper.overrideAttrs (old: {
      postInstall = ''
        substituteInPlace $out/share/applications/beeper.desktop \
          --replace 'Exec=beeper' 'Exec=beeper --default-frame'
      '';
    });

    exiftool1270 = prev.exiftool.overrideAttrs (old: rec {
      name = "perl${prev.perl.version}-${pname}-${version}";
      pname = "Image-ExifTool";
      version = "12.70";
      src = prev.fetchurl {
        url = "https://exiftool.org/Image-ExifTool-${version}.tar.gz";
        hash = "sha256-TLJSJEXMPj870TkExq6uraX8Wl4kmNerrSlX3LQsr/4=";
      };
    });

    iosevka-flare =
      (prev.iosevka.override {
        set = "Flare";
        privateBuildPlan = ''
          [buildPlans.IosevkaFlare]
          family = "Iosevka Flare"
          spacing = "fixed"

          [buildPlans.IosevkaFlare.metricOverride]
          leading = 1500

          [buildPlans.IosevkaFlare.variants]
          inherits = "ss08"

          [buildPlans.IosevkaFlare.variants.design]
          capital-m = "slanted-sides-hanging-serifless"
          a = "double-storey-tailed"
          f = "flat-hook-serifless"
          g = "double-storey-open"
          j = "flat-hook-serifed"
          l = "serifed-semi-tailed"
          r = "corner-hooked-serifed"
          t = "flat-hook-short-neck"
          y = "curly-turn-serifless"
          cyrl-capital-u = "curly-turn-serifless"
          cyrl-u = "curly-turn-serifless"
          cyrl-em = "slanted-sides-hanging-serifless"
          zero = "oval-dotted"
          one = "base"
          two = "straight-neck-serifless"
          eight = "crossing-asymmetric"
          nine = "open-contour"
          at = "compact"
          ampersand = "et-toothless-corner"
          percent = "rings-continuous-slash"

          [buildPlans.IosevkaFlare.widths.Condensed]
          css = "condensed"
          menu = 3
          shape = 500

          [buildPlans.IosevkaFlare.widths.Normal]
          css = "normal"
          menu = 5
          shape = 600
        '';
      }).overrideAttrs
        (old: {
          outputs = [
            "out"
            "dist"
          ];

          buildPhase = ''
            export HOME=$TMPDIR
            runHook preBuild
            npm run build --no-update-notifier --targets contents::$pname -- --jCmd=$NIX_BUILD_CORES --verbose=9
            runHook postBuild
          '';

          postInstall = ''
            mkdir -p "$dist"
            cp -R dist "$dist"
          '';
        });

    nerdfonts-symbols = prev.nerdfonts.override { fonts = [ "NerdFontsSymbolsOnly" ]; };

    quickemu = inputs.quickemu.packages.${prev.system}.quickemu;

    vimPlugins = prev.vimPlugins // {
      sprinkles = prev.vimUtils.buildVimPlugin {
        pname = "sprinkles";
        version = "2022-12-06";
        src = prev.fetchFromGitLab {
          owner = "axgfn";
          repo = "sprinkles";
          rev = "dcd9951b8e5a61e42c1b2ddc0a32c2924a636c03";
          sha256 = "1qqrdx1rg7p6pyxdbx4cnadpnlrar9q0s30lqn1ww1j27w4153wy";
        };
        meta.homepage = "https://gitlab.com/axgfn/sprinkles";
      };

      vim-HiLinkTrace = prev.vimUtils.buildVimPlugin {
        pname = "vim-HiLinkTrace";
        version = "4j";
        src = prev.fetchFromGitHub {
          owner = "gerw";
          repo = "vim-HiLinkTrace";
          rev = "64da6bf463362967876fdee19c6c8d7dd3d0bf0f";
          sha256 = "1yzqhfn97543c8fm2lzn7fa9b9f4bgalyipcd8rxd3ria6l01y85";
        };
        meta.homepage = "https://github.com/gerw/vim-HiLinkTrace";
      };
    };
  };
in
# Compose multiple overlays into a single overlay.
inputs.nixpkgs.lib.composeManyExtensions [
  additions
  inputs.agenix.overlays.default
  inputs.emacs.overlays.default
  inputs.lix-module.overlays.default
  modifications
]
