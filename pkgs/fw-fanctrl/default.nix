{
  lib,
  python3Packages,
  fetchFromGitHub,
  writeText,
  fw-ectool,
  lm_sensors,
}:

let
  setupPy = writeText "setup.py" ''
    from setuptools import setup

    setup(
      name="fw-fanctrl",
      platforms=["linux"],
      scripts=[
        "fanctrl.py",
      ],
    )
  '';
in
python3Packages.buildPythonPackage rec {
  pname = "fw-fanctrl";
  version = "2024-05-25";

  src = fetchFromGitHub {
    owner = "TamtamHero";
    repo = "fw-fanctrl";
    rev = "f0d8d776fe75878d11b6dd92fa34518f388146b5";
    hash = "sha256-2SPATg8Ypj7GPz2GbKNxYKHX76MCFdp1Vq32YWpdkIc=";
  };

  patches = [ ./fw-fanctrl_work-around-bad-amd-ec-temps.patch ];

  doCheck = false;

  postPatch = ''
    substituteInPlace fanctrl.py --replace \
      '"ectool' '"${fw-ectool}/bin/ectool'
    substituteInPlace fanctrl.py --replace \
      '"sensors' '"${lm_sensors}/bin/sensors'
    cp ${setupPy} ${setupPy.name}
  '';

  postInstall = ''
    mv -v $out/bin/fanctrl.py $out/bin/fw-fanctrl
  '';

  meta = with lib; {
    description = "Simple service to control Framework Laptop fans";
    homepage = "https://github.com/TamtamHero/fw-fanctrl";
    license = licenses.bsd3;
    mainProgram = "fw-fanctrl";
    platforms = platforms.linux;
  };
}
