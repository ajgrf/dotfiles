{
  stdenv,
  lib,
  fetchurl,
}:

stdenv.mkDerivation {
  pname = "cups-brother-hl4570cdw";
  version = "1.08";

  # https://www.openprinting.org/ppd-o-matic.php?driver=Postscript-Brother&printer=Brother-HL-4070CDW&show=0
  src = ./Brother-HL-4070CDW-Postscript-Brother.ppd;

  dontUnpack = true;
  dontBuild = true;
  dontConfigure = true;

  installPhase = ''
    mkdir -p $out/share/cups/model/brother
    cp $src  $out/share/cups/model/brother/hl4570cdw.ppd
  '';

  meta = with lib; {
    description = "Brother HL-4570CDW printer driver";
    platforms = platforms.all;
  };
}
