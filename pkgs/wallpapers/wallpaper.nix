{
  stdenv,
  lib,
  fetchurl,
  imagemagick,
  wallpaper,
}:

stdenv.mkDerivation {
  name = "wallpaper-${wallpaper.title}";

  src = fetchurl { inherit (wallpaper) url sha256; };

  nativeBuildInputs = [ imagemagick ];

  dontUnpack = true;
  dontBuild = true;
  dontConfigure = true;

  installPhase = ''
    mkdir -p $out/share/backgrounds
    # Shrink large images and convert.
    convert $src -resize '3440x1440^>' \
      $out/share/backgrounds/${wallpaper.title}.png
  '';

  passthru = {
    inherit (wallpaper) title;
    imagePath = "/share/backgrounds/${wallpaper.title}.png";
  };

  meta.platforms = lib.platforms.all;
}
