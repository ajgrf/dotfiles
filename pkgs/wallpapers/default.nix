{ symlinkJoin, callPackage }:

let
  wallpapers = builtins.map (
    wallpaper: callPackage ./wallpaper.nix { inherit wallpaper; }
  ) (import ./list.nix);
in
builtins.listToAttrs (
  builtins.map (wallpaper: {
    name = wallpaper.title;
    value = "${wallpaper}${wallpaper.imagePath}";
  }) wallpapers
)
// symlinkJoin {
  name = "wallpapers";
  paths = wallpapers;
}
