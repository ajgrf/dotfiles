[
  {
    title = "american-capital";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/14rhuup/the_us_capitol_3440x1440/
    url = "https://i.redd.it/tz5repdpo6ab1.jpg";
    sha256 = "sha256-sWEkSDO3bp5wPTzimdhxyCdxNgZpiH8JRKYfRAArs/Q=";
  }
  {
    title = "atomhawk-christmas";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/zk2pcz/merry_christmas_3440x1440/
    url = "https://i.redd.it/itb408wqnh5a1.jpg";
    sha256 = "0g73bj2wgrspkqc7n3aj1rdzqyi2xyswbgn5rdyazhq7p3zb0ljd";
  }
  {
    title = "autumn-railroad";
    url = "https://w.wallhaven.cc/full/1k/wallhaven-1ko5p1.jpg";
    sha256 = "0v605rg8wbp3405vhm5ycqz0akq88cmrizzvmcbangv1bff8gzdz";
  }
  {
    title = "big-green-leaves";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/10ebmy5/big_green_leaves_enhanced_5160x2160/
    url = "https://i.redd.it/heiqnf0hplca1.jpg";
    sha256 = "sha256-TgN6Si0X9RuApK2yB4wCfj31/temrsVdBSKtXSJiesI=";
  }
  {
    title = "blue-water-mountains";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/lzonu0/landscapes_partvi_%F0%92%8C%8B_5160_2160/
    url = "https://i.redd.it/3eumjf7n9ll61.png";
    sha256 = "110f48zvh4v4r70rzvyr1vyybgmk8qmja3mmnm8csx2vn8vv3vdk";
  }
  {
    title = "cloudland";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/hpx42z/3440_x_1440_wallpaper_dump_122_images/
    url = "https://i.imgur.com/72S1Wf1.jpeg";
    sha256 = "1ziqgqk6sj01fq23wr4k1hs1z7ywarc0z1y5rcn0n6w9pasj8avb";
  }
  {
    title = "colorful-lines";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/zwfucf/colorful_lines_5120x2160/
    url = "https://i.redd.it/q9yqy1pt1g8a1.jpg";
    sha256 = "1x13sccgv7d362cnnlzfjmrsvkpcn2167cqclnhpidj065m5jnri";
  }
  {
    title = "cool-mountain";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/hpx42z/3440_x_1440_wallpaper_dump_122_images/
    url = "https://i.imgur.com/4nZt623.jpeg";
    sha256 = "08gvr6n7rq9i2mq4ysvw9sjfvrs6d7mfg4w1nla14n4il290smpj";
  }
  {
    title = "cyan-lines";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/hpx42z/3440_x_1440_wallpaper_dump_122_images/
    url = "https://i.imgur.com/8cY5lxP.png";
    sha256 = "158kxvxicp84bcbnlqkm453gs6rx5b457r2w87k54cnq5yid91lp";
  }
  {
    title = "desert-man";
    url = "https://i.redd.it/6bgpgjcsnlic1.png";
    sha256 = "0a2g5f75gmla3h6li2m1kgxwj1fk73jd791cafsvb38pymxkh63y";
  }
  {
    title = "full-moon";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/fbufr5/the_moon_at_13760x5760_219/
    url = "https://i.redd.it/ztdy1qofi2k41.jpg";
    sha256 = "0lyynpvvx4izq5fan7i6q08jplis89x8ks78k710vmvn6clqyfd3";
  }
  {
    title = "landscape-pagoda";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/lqdgew/landscapes_partiii_5160x2160/
    url = "https://i.redd.it/bd57moars6j61.png";
    sha256 = "0837xynxzhss5jq25mfz24nn4jkfvrqzx86y1xgf2v62zh4wkzy9";
  }
  {
    title = "little-italy-day";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/h0zoz2/little_italy_day_by_richard_wright_3840_1646/
    url = "https://i.redd.it/uw6vz13dda451.png";
    sha256 = "1mlkkzavb3wj9ygkfahznlhsigki5vga5jmhm1bkbz1j6d1615b3";
  }
  {
    title = "mountain-valley";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/ly9ris/dual_monitor_7680%E4%B9%893094_5600%E4%B9%891800/
    url = "https://i.redd.it/nzplzvwj17l61.jpg";
    sha256 = "0813qqczpvi97bppf0jh79jcibbck01xczyh17kp92ib2skf5hcx";
  }
  {
    title = "ngapali-waves";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/12szezc/shots_i_took_at_ngapali_beach_5120x1440/
    url = "https://i.redd.it/vkppiwd3o1va1.jpg";
    sha256 = "0pj89cfh5mmil2vswijgvj5bl0nf4g4gwxy62khdh9y479h33ky7";
  }
  {
    title = "overcast-hills";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/lqdgew/landscapes_partiii_5160x2160/
    url = "https://i.redd.it/a6c80cyss6j61.png";
    sha256 = "02amcidwdgpyd4klf476gsl81c97y60n4zqc2bz10549jd614j4h";
  }
  {
    title = "santiago";
    url = "https://i.imgur.com/o1Rq9UI.jpeg";
    sha256 = "0mrllp2q1vs1nllbag25d5x68p2p1f97rz7vbmnyg6rj9x4hvjvp";
  }
  {
    title = "sea-railroad-forest";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/hpx42z/3440_x_1440_wallpaper_dump_122_images/
    url = "https://i.imgur.com/aBWkWdZ.jpeg";
    sha256 = "141hgrpyvwrdkznwi28l8pfl9yxzpvm733dvn70lwcqpywg55qlh";
  }
  {
    title = "twilight-mountain";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/hpx42z/3440_x_1440_wallpaper_dump_122_images/
    url = "https://i.imgur.com/7hCDGR1.jpeg";
    sha256 = "0fblixvgsasyw0msibmg3sr5rvxa2bxblc6kpdlbj9yh9i079gh7";
  }
  {
    title = "unity";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/11jk5gv/unity_by_me_3440x1440/
    url = "https://i.redd.it/5i949iwms0ma1.png";
    sha256 = "1p3r4nbc63z3fjw6v72pq2aav9c3wp4jyrnpna3d0c8plhy5z41y";
  }
  {
    title = "winter-crater-lake";
    # https://old.reddit.com/r/WidescreenWallpaper/comments/kyo0ip/crater_lake_in_winter_3440_x_1440/
    url = "https://i.redd.it/dhieyjmbjqb61.jpg";
    sha256 = "1nmnxazd1qsx4kd3qf32q7567vfmz6nwx2d9n43ak8rivhlglwz8";
  }
  {
    title = "winter-log-cabin";
    url = "https://w.wallhaven.cc/full/3k/wallhaven-3kqwe3.jpg";
    sha256 = "0b8zknpknsc6yrc7gjlg4f6ci4hczvnljg2ny44h0grh7a4yc2nk";
  }
]
