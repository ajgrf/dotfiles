{ texlive, writeScriptBin }:

let
  tex = texlive.combine {
    inherit (texlive)
      scheme-minimal
      latex-bin
      epstopdf-pkg
      geometry
      pdfjam
      pdfpages
      pdflscape
      ;
  };
in
writeScriptBin "pdfbook" ''
  ${tex}/bin/pdfjam --landscape --suffix book --signature 4 "$@"
''
