{
  stdenv,
  lib,
  fetchFromGitHub,
  makeWrapper,
  bc,
  ffmpeg,
  jq,
}:

stdenv.mkDerivation rec {
  pname = "aaxtomp3";
  version = "1.3";
  nativeBuildInputs = [ makeWrapper ];

  src = fetchFromGitHub {
    owner = "KrumpetPirate";
    repo = "AAXtoMP3";
    rev = "v${version}";
    sha256 = "024mydqrnssbmdf1n3vmr2xybsrgixc71dqn7zh7yn0vz9b5kbzd";
  };

  installPhase = ''
    mkdir -p $out/bin
    cp AAXtoMP3 $out/bin
  '';

  wrapperPath =
    with lib;
    makeBinPath ([
      bc
      ffmpeg
      jq
    ]);

  postFixup = ''
    wrapProgram $out/bin/AAXtoMP3 \
      --prefix PATH : "${wrapperPath}"
  '';

  meta = with lib; {
    description = "Convert Audible AAX files";
    homepage = "https://github.com/KrumpetPirate/AAXtoMP3";
    license = licenses.wtfpl;
  };
}
