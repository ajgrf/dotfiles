# Custom packages defined here are exported in the flake's
# 'packages' output.
{ pkgs }:

{
  aaxtomp3 = pkgs.callPackage ./aaxtomp3 { };
  cups-brother-hl4570cdw = pkgs.callPackage ./cups-brother-hl4570cdw { };
  fw-fanctrl = pkgs.callPackage ./fw-fanctrl { };
  kwin4-effect-geometry-change = pkgs.libsForQt5.callPackage ./kwin4-effect-geometry-change { };
  pdfbook = pkgs.callPackage ./pdfbook { };
  wallpapers = pkgs.callPackage ./wallpapers { };
}
