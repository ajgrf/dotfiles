# TODO: integrate Chris Titus' winutil script

# Path is already modified by scoop, but the shell won't see it yet
# when bootstrapping.
$SCOOP = if ($env:SCOOP) { $env:SCOOP } else { $env:USERPROFILE + "\scoop" }
$env:Path += ";$SCOOP\shims"

function usage {
    echo "usage: setup-dotfiles.ps1 <task>"
    exit 1
}

function main {
    switch ($args[0]) {
        bootstrap         { bootstrap }
        scoop             { setup-scoop }
        winget            { setup-winget }
        pswindowsupdate   { setup-pswindowsupdate }
        gpg               { setup-gpg }
        ssh               { setup-ssh }
        clean-desktop     { setup-clean-desktop }
        console           { setup-console }
        git-repos         { setup-git-repos }
        doh               { setup-doh }
        system-registry   { setup-system-registry }
        user-registry     { setup-user-registry }
        emacs             { setup-emacs }
        keyboard          { setup-keyboard }
        default           { usage }
    }
}

function New-Shortcut {
    param($File, $Target, $Arguments, $Icon)

    $WScriptShell = New-Object -ComObject WScript.Shell
    $Shortcut = $WScriptShell.CreateShortcut($File)
    $Shortcut.TargetPath = $Target
    $Shortcut.Arguments = $Arguments
    if ($Icon) {
        $Shortcut.IconLocation = $Icon
    }
    $Shortcut.WorkingDirectory = $env:USERPROFILE
    $Shortcut.Save()
    [System.Runtime.InteropServices.Marshal]::FinalReleaseComObject($WScriptShell)
}

function bootstrap {
    setup-scoop
    setup-winget
    setup-pswindowsupdate
    setup-gpg
    setup-ssh
    setup-clean-desktop
    # setup-console
    # setup-system-registry
    # setup-user-registry
}

function setup-scoop {
    $buckets = @(
        @{ name = "extras" }
        @{ name = "java" }
        @{ name = "nonportable" }
        @{ name = "versions" }
        @{ name = "axgfn"; repo = "https://gitlab.com/axgfn/scoop-bucket" }
        @{ name = "chezmoi"; repo = "https://github.com/twpayne/scoop-bucket" }
        @{ name = "clojure"; repo = "https://github.com/littleli/scoop-clojure" }
    )

    $packages = @(
        "chezmoi"
        "concfg"
        "font-go"
        "git"
        "mpv"
        "nextcloud"
        "gsudo"
        "rclone"
        "sysinternals"
        "topgrade"
    )

    if (! (Test-Path -Path $SCOOP\apps\scoop)) {
        echo "Installing scoop..."
        Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
        scoop install git
    }

    foreach ($bucket in $buckets) {
        if (! (Test-Path -Path ($SCOOP + "\buckets\" + $bucket.name))) {
            echo ("Adding " + $bucket.name + " bucket...")
            scoop bucket add $bucket.name $bucket.repo
        }
    }

    foreach ($package in $packages) {
        if (! (Test-Path -Path ($SCOOP + "\apps\" + $package))) {
            scoop install $package
        }
    }

    scoop alias add upgrade 'scoop update *'
}

function setup-winget {
    if (! (Get-Command winget)) {
        echo "Installing winget..."
        scoop install vcredist2022 winget
    }

    echo "Installing winget packages..."
    winget import $env:USERPROFILE\Documents\winget.json
}

function setup-pswindowsupdate {
    if (! (Get-Module -ListAvailable -Name PSWindowsUpdate)) {
        echo "Installing PSWindowsUpdate..."
        gsudo {
            Install-Module PSWindowsUpdate
            Add-WUServiceManager -ServiceID 7971f918-a847-4430-9279-4a52d1efe18d
        }
    }
}

# TODO: switch to usbip for passing through Yubikeys to WSL
function setup-gpg {
    echo "Configuring GnuPG..."

    scoop install gnupg win-gpg-agent

    # Stop Windows ssh-agent
    gsudo {
        Stop-Service ssh-agent
        Set-Service -StartupType Disabled ssh-agent
    }

    # Run win-gpg-agent when Windows starts
    New-Shortcut -File "$env:APPDATA\Microsoft\Windows\Start Menu\Programs\Startup\win-gpg-agent.lnk" `
      -Target "$env:USERPROFILE\scoop\apps\win-gpg-agent\current\agent-gui.exe"
}

function setup-ssh {
    echo "Configuring SSH server..."

    gsudo {
        Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
        Start-Service sshd
        Set-Service -Name sshd -StartupType Automatic

        # Close port - it should only be accessible via Tailscale.
        netsh advfirewall firewall set rule name="OpenSSH SSH Server (sshd)" `
          new enable=no
    }

    # https://www.concurrency.com/blog/may-2019/key-based-authentication-for-openssh-on-windows
    echo ""
    echo "If the user account on the server you are connecting to is in the local"
    echo "Administrators group, the public key must be placed in"
    echo "C:\ProgramData\ssh\administrators_authorized_keys instead of the user's"
    echo ".ssh folder. Additionally, only the Administrators group and SYSTEM"
    echo "account can have access to that file, for security purposes."
    echo ""
}

function setup-clean-desktop {
    echo "Removing desktop shortcuts..."

    $shortcuts = @(
        "Adobe Acrobat 2020"
        "Anki"
        "Brave"
        "Chromium"
        "Firefox"
        "GOG GALAXY"
        "Google Chrome"
        "KDE Connect"
        "Kindle"
        "Microsoft Edge"
        "Nextcloud"
        "Signal"
        "Spotify"
        "Steam"
        "Thunderbird"
        "Veracrypt"
        "WinSCP"
    )

    foreach ($shortcut in $shortcuts) {
        if (Test-Path -Path "$env:PUBLIC\Desktop\$shortcut.lnk") {
            Remove-Item "$env:PUBLIC\Desktop\$shortcut.lnk"
        }
        if (Test-Path -Path "$env:USERPROFILE\Desktop\$shortcut.lnk") {
            Remove-Item "$env:USERPROFILE\Desktop\$shortcut.lnk"
        }
    }
}

function setup-console {
    echo "Configuring Windows console..."
    concfg clean
    concfg import -n basic $env:USERPROFILE\Documents\concfg.json
}

function setup-git-repos {
    $repos = @(
        @{
            dir = "$env:USERPROFILE\Projects\dotfiles"
            url = "https://gitlab.com/axgfn/dotfiles.git"
            pushurl = "git@gitlab.com:axgfn/dotfiles.git"
        }
        @{
            dir = "$env:USERPROFILE\Projects\scoop-axgfn"
            url = "git@gitlab.com:axgfn/scoop-bucket.git"
        }
    )

    echo "Setting up git repositories..."
    foreach ($repo in $repos) {
        if (! (Test-Path -Path $repo.dir)) {
            git clone $repo.url $repo.dir
        } else {
            git -C $repo.dir remote set-url origin $repo.url
            if ($repo.pushurl) {
                git -C $repo.dir remote set-url --push origin $repo.pushurl
            }
        }
    }
}

function setup-doh {
    # Only run on Windows 11 or higher
    $windows_buildno = [System.Environment]::OSVersion.Version.Build
    if ($windows_buildno -ge 22000) {
        echo "Adding DNS-over-HTTPS templates..."
        gsudo {
            netsh dns add encryption server=1.1.1.3 dohtemplate=https://family.cloudflare-dns.com/dns-query
            netsh dns add encryption server=1.0.0.3 dohtemplate=https://family.cloudflare-dns.com/dns-query
        }
    }
}

function setup-system-registry {
    echo "Setting system registry keys..."
    gsudo regedit.exe /s $env:USERPROFILE\Documents\system.reg
}

function setup-user-registry {
    echo "Setting user registry keys..."
    reg.exe import $env:USERPROFILE\Documents\user.reg
}

function setup-emacs {
    echo "Installing Emacs..."

    $EMACSDIR = "$env:USERPROFILE\.config\emacs"

    scoop install emacs gcc ripgrep sqlite

    # Set $env:HOME and make it persistent.
    $env:HOME = $env:USERPROFILE
    [Environment]::SetEnvironmentVariable("HOME", $env:HOME, [System.EnvironmentVariableTarget]::User)
}

main @args
