# This file contains configuration specific to my headless Framework installation.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

{
  imports = [
    ./disk-config.nix

    inputs.disko.nixosModules.disko
    inputs.hardware.nixosModules.framework-11th-gen-intel
  ];

  # Enable additional configuration from feature modules.
  features = {
    actual-budget.enable = true;
    anki-sync-server.enable = true;
    backup.enable = true;
    base.enable = true;
    common.enable = true;
    forgejo.enable = true;
    miniflux.enable = true;
    nextcloud.enable = true;
    paperless.enable = true;
    user.enable = true;
    vaultwarden.enable = true;
  };

  # Erase the root filesystem on boot, using the impermanence module
  # to store some state in /persist.
  features.optin-persistence.enable = true;

  # Enable Secure Boot with self-signed keys.
  features.secure-boot.enable = true;

  # agenix secrets have been prepared for this host.
  trusted = true;

  networking.hostName = "renewing-anemone"; # Define your hostname.

  # Explicitly set PostgreSQL version to use.
  services.postgresql.package = pkgs.postgresql_14;

  # Reboot this machine if necessary after automatic upgrades.
  system.autoUpgrade.allowReboot = true;

  # Bootloader settings.
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

  # Monitor APC UPS backup battery and shut down in the event
  # of a power failure.
  services.apcupsd.enable = true;

  # Enable non-free firmware.
  hardware.enableRedistributableFirmware = true;

  # Enable firmware updates.
  services.fwupd = {
    enable = true;
    extraRemotes = [ "lvfs-testing" ];
    uefiCapsuleSettings.DisableCapsuleUpdateOnDisk = true;
  };

  # Edited results of the hardware scan (nixos-generate-config):

  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.initrd.availableKernelModules = [
    "xhci_pci"
    "thunderbolt"
    "nvme"
    "usb_storage"
    "usbhid"
    "sd_mod"
  ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  # Impermanence requires /persist to be marked as needed for boot.
  fileSystems."/persist".neededForBoot = true;

  # Enable in-memory compressed swap device with the zram module.
  zramSwap.enable = true;

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?

}
