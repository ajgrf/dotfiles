# This file contains configuration specific to my desktop installation.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

{
  imports = [
    ./disk-config.nix

    inputs.disko.nixosModules.disko
    inputs.hardware.nixosModules.common-pc
    inputs.hardware.nixosModules.common-pc-ssd
    inputs.hardware.nixosModules.common-cpu-amd
    inputs.hardware.nixosModules.common-gpu-amd
  ];

  # Enable additional configuration from feature modules.
  features.desktop.enable = true;

  # Erase the root filesystem on boot, using the impermanence module
  # to store some state in /persist.
  features.optin-persistence.enable = true;

  # Enable Secure Boot with self-signed keys.
  features.secure-boot.enable = true;

  # agenix secrets have been prepared for this host.
  trusted = true;

  networking.hostName = "staunch-coral";

  # Enable Wake-on-LAN.
  networking.interfaces.eno1.wakeOnLan.enable = true;

  # Bootloader settings.
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

  # Install piper for configuring gaming mouse.
  services.ratbagd.enable = true;
  environment.systemPackages = with pkgs; [ piper ];

  # Enable Bluetooth driver for the Xbox wireless controller.
  hardware.xpadneo.enable = true;
  # https://atar-axis.github.io/xpadneo/#high-latency-or-lost-button-events-with-bluetooth-le
  hardware.bluetooth.settings = {
    LE = {
      MinConnectionInterval = 7;
      MaxConnectionInterval = 9;
      ConnectionLatency = 0;
    };
  };

  # Enable hardware acceleration for ollama.
  # HACK: disable gpu acceleration until rocm builds again
  # services.ollama = {
  #   acceleration = "rocm";
  #   rocmOverrideGfx = "10.3.0";
  # };

  # Enable non-free firmware.
  hardware.enableRedistributableFirmware = true;

  # Prevent system from resuming immediately after initiating suspend.
  systemd.tmpfiles.rules = [ "w /proc/acpi/wakeup - - - - GPP0" ];

  # Edited results of the hardware scan (nixos-generate-config):

  boot.initrd.availableKernelModules = [
    "nvme"
    "xhci_pci"
    "ahci"
    "usb_storage"
    "usbhid"
    "sd_mod"
  ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];

  # Impermanence requires /persist to be marked as needed for boot.
  fileSystems."/persist".neededForBoot = true;

  # Enable in-memory compressed swap device with the zram module.
  zramSwap.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
