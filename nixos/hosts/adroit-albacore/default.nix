# This file contains configuration specific to my Framework Laptop installation.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

{
  imports = [
    ./disk-config.nix

    inputs.disko.nixosModules.disko
    inputs.hardware.nixosModules.framework-13-7040-amd
  ];

  # Enable additional configuration from feature modules.
  features.desktop.enable = true;

  # Erase the root filesystem on boot, using the impermanence module
  # to store some state in /persist.
  features.optin-persistence.enable = true;

  # Enable Secure Boot with self-signed keys.
  features.secure-boot.enable = true;

  # agenix secrets have been prepared for this host.
  trusted = true;

  networking.hostName = "adroit-albacore";

  # Bootloader settings.
  boot.loader = {
    timeout = 2;
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

  # Set DPI for fonts and UI (icons not affected).
  services.xserver.dpi = 144;

  # Start fingerprint service at boot instead of on first use.
  systemd.services.fprintd = {
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "simple";
  };

  # Install tool for interacting with the embedded controller.
  environment.systemPackages = with pkgs; [ fw-ectool ];

  # Enable hardware acceleration for ollama.
  # HACK: disable gpu acceleration until rocm builds again
  # services.ollama = {
  #   acceleration = "rocm";
  #   rocmOverrideGfx = "11.0.0";
  # };

  # Enable non-free firmware.
  hardware.enableRedistributableFirmware = true;

  # Enable software fan control.
  hardware.fw-fanctrl.enable = true;

  # Enable firmware updates.
  services.fwupd.enable = true;

  # Edited results of the hardware scan (nixos-generate-config):

  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.initrd.availableKernelModules = [
    "xhci_pci"
    "thunderbolt"
    "nvme"
    "usb_storage"
    "usbhid"
    "sd_mod"
  ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];

  # Impermanence requires /persist to be marked as needed for boot.
  fileSystems."/persist".neededForBoot = true;

  # Enable in-memory compressed swap device with the zram module.
  zramSwap.enable = true;

  powerManagement.cpuFreqGovernor = lib.mkDefault "schedutil";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
