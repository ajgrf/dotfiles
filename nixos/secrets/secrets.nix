let
  axgfn = "age1xgc5kjqumrrvz52pxumf3pe4ecgyyd7nyj4nppx3gl5crdm9nprqhaesxn";
  staunch-coral = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILQFsTMoiv3iyM439BIMwqTMmWj19s8CEb89wuwU9uTM";
  adroit-albacore = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOBGURbVOur8xDvRm6SFsI8gYvjU7HPCd7FDp4tUSQMp";
  plucky-lemur = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOHb8SSYzDStBRf2AXgt6lD8sJs9zQLZC8n6f9fGAzwZ";
  renewing-anemone = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKum257DtUNJ2l+ENeRSOHWz+whcWh+Ua0ubAhWAbI29";
in
{
  "user-password.age".publicKeys = [
    axgfn
    staunch-coral
    adroit-albacore
    plucky-lemur
    renewing-anemone
  ];
  "acme-creds.age".publicKeys = [
    axgfn
    renewing-anemone
  ];
  "anki-password.age".publicKeys = [
    axgfn
    renewing-anemone
  ];
  "miniflux-creds.age".publicKeys = [
    axgfn
    renewing-anemone
  ];
  "nextcloud-adminpass.age".publicKeys = [
    axgfn
    renewing-anemone
  ];
  "nextcloud-dbpass.age".publicKeys = [
    axgfn
    renewing-anemone
  ];
  "paperless-adminpass.age".publicKeys = [
    axgfn
    renewing-anemone
  ];
  "restic-password.age".publicKeys = [
    axgfn
    renewing-anemone
  ];
  "restic-s3creds.age".publicKeys = [
    axgfn
    renewing-anemone
  ];
  "rclone.conf.age".publicKeys = [
    axgfn
    staunch-coral
    adroit-albacore
    plucky-lemur
  ];
}
