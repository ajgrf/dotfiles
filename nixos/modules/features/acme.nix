{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

with lib;

let
  cfg = config.features.acme;
in
{
  options.features.acme.enable = mkEnableOption "acme feature";

  config = lib.mkIf cfg.enable {
    assertions = [
      {
        assertion = config.trusted;
        message = "acme feature requires agenix secrets to be prepared.";
      }
    ];

    security.acme.acceptTerms = true;
    security.acme.defaults = {
      email = "alex.griffin@axgfn.com";
      dnsProvider = "porkbun";
      credentialsFile = config.age.secrets.acme-creds.path;
    };

    age.secrets.acme-creds.file = ../../secrets/acme-creds.age;
  };
}
