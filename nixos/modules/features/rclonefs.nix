# This file sets up automounts to remote storage using rclone.
{
  config,
  lib,
  pkgs,
  ...
}:

# TODO: add Proton Drive mount

let
  cfg = config.features.rclonefs;
  package = pkgs.runCommand "mount.rclone" { preferLocalBuild = true; } ''
    mkdir -p $out/bin
    ln -s ${pkgs.rclone}/bin/rclone $out/bin/mount.rclone
  '';
in
{
  options.features.rclonefs.enable = lib.mkEnableOption "rclonefs feature";

  config = lib.mkIf cfg.enable {
    assertions = [
      {
        assertion = config.trusted;
        message = "rclonefs automount requires agenix secrets to be prepared.";
      }
    ];

    # Add mount helper to the system path so that `mount' can find it.
    system.fsPackages = [ package ];

    fileSystems."/mnt/egnyte" = {
      device = "egnyte:";
      fsType = "rclone";
      options = [
        "allow_other"
        "_netdev"
        "x-systemd.automount"
        "x-gvfs-hide"

        # rclone options
        "args2env"
        "cache-dir=/var/cache/rclone"
        ("config=" + config.age.secrets."rclone.conf".path)
        "env.PATH=/bin:/usr/bin:${pkgs.fuse3}/bin"
        ("gid=" + builtins.toString config.ids.gids.users)
        ("uid=" + builtins.toString config.user.uid)
        "vfs-cache-mode=writes"
      ];
    };

    age.secrets."rclone.conf".file = ../../secrets/rclone.conf.age;
  };
}
