# This feature module contains configuration I want on all my desktop machines.
{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.features.desktop;
in
{
  options.features.desktop.enable = lib.mkEnableOption "desktop feature";

  config = lib.mkIf cfg.enable {
    # Enable additional configuration from feature modules.
    features = {
      base.enable = true;
      common.enable = true;
      keyboard.enable = true;
      plymouth.enable = true;
      rclonefs.enable = config.trusted;
      user.enable = true;
    };

    # TODO: set up opensnitch application firewall

    # Open ports in the firewall applications.
    networking.firewall = {
      allowedTCPPortRanges = [
        # soco-cli
        {
          from = 1400;
          to = 1499;
        }
        {
          from = 54000;
          to = 54099;
        }
        # kdeconnect
        {
          from = 1714;
          to = 1764;
        }
        # catt
        {
          from = 45000;
          to = 47000;
        }
      ];
      allowedUDPPorts = [ 1900 ];
      allowedUDPPortRanges = [
        {
          from = 1714;
          to = 1764;
        }
      ];
    };

    # A delay helps laptops connect to wifi before starting
    # 'persistent' upgrades on boot.
    system.autoUpgrade.randomizedDelaySec = "5min";

    # List packages installed in system profile. To search, run:
    # $ nix search wget
    environment.systemPackages = with pkgs; [
      ffmpegthumbnailer
      via
    ];

    # Let the VIA keyboard configurator access hardware.
    services.udev.packages = [ pkgs.via ];

    # Run ollama server for using local LLMs.
    services.ollama = {
      enable = true;
      package = pkgs.newerPkg.ollama;
    };

    # Only allow ssh connections over tailscale.
    services.openssh.openFirewall = false;

    # Enable CUPS to print documents.
    services.printing = {
      enable = true;
      drivers = with pkgs; [ cups-brother-hl4570cdw ];
    };

    # Enable scanner support.
    hardware.sane = {
      enable = true;
      extraBackends = [ pkgs.sane-airscan ];
    };

    # Enable sound with PipeWire.
    hardware.pulseaudio.enable = false;
    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };

    # Enable Bluetooth.
    hardware.bluetooth.enable = true;

    # 32-bit graphics & sound support for games.
    hardware.graphics.enable32Bit = true;

    # Enable the X11 windowing system.
    services.xserver.enable = true;

    # Enable the Simple Desktop Display Manager (SDDM).
    services.displayManager.sddm = {
      enable = true;
      wayland.enable = true;
    };

    # Enable the Plasma desktop session.
    services.desktopManager.plasma6.enable = true;
    # Don't install these optional KDE packages.
    environment.plasma6.excludePackages = with pkgs.kdePackages; [
      elisa
      kate
      konsole
    ];

    # Set Plasma as the default session.
    services.displayManager.defaultSession = "plasma";

    # Reverse scrolling direction so that it feels like dragging the screen
    # with the touchpad.
    services.libinput.touchpad.naturalScrolling = true;

    # Enable KDE Partition Manager.
    programs.partition-manager.enable = true;

    # Install ADB Android Debug Bridge.
    programs.adb.enable = true;

    # Enable dconf D-Bus service.
    programs.dconf.enable = true;

    # Use Qt pinentry interface.
    programs.gnupg.agent.pinentryPackage = pkgs.pinentry-qt;

    # Enable Guix build daemon service.
    services.guix.enable = true;
    services.guix.gc = {
      enable = true;
      extraArgs = [ "--delete-generations=10d" ];
      dates = "weekly";
    };

    # Enable libvirt daemon.
    virtualisation.libvirtd.enable = true;
    # Enable redirection of USB devices to virtual machines.
    virtualisation.spiceUSBRedirection.enable = true;
    # Configure kvm module for macOS and Windows vms.
    boot.extraModprobeConfig = ''
      options kvm ignore_msrs=1 report_ignored_msrs=0
    '';

    # Enable Docker.
    virtualisation.docker = {
      enable = true;
      rootless = {
        enable = true;
        setSocketVariable = true;
      };
    };

    # Support running (and building) aarch64 binaries on x86_64.
    # TODO: enable appimage binfmt registration
    boot.binfmt.emulatedSystems = lib.optionals (pkgs.system == "x86_64-linux") [ "aarch64-linux" ];

    # Extra groups
    users.users.${config.user.name}.extraGroups = [
      "adbusers"
      "libvirtd"
      "lp"
      "scanner"
    ];

    # Disable alternate sleep states. Somehow this prevents libvirt and qemu
    # from crashing the desktop session.
    systemd.sleep.extraConfig = ''
      AllowSuspend=yes
      AllowHibernation=no
      AllowHybridSleep=no
      AllowSuspendThenHibernate=no
    '';
  };
}
