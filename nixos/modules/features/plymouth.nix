# This feature module configures the Plymouth boot splash screen.
{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.features.plymouth;
in
{
  options.features.plymouth.enable = lib.mkEnableOption "plymouth feature";

  config = lib.mkIf cfg.enable {
    boot = {
      # Enable systemd in initrd.
      initrd.systemd.enable = true;

      # Enable boot splash screen.
      plymouth.enable = true;

      # Inhibit boot messages.
      consoleLogLevel = 0;
      initrd.verbose = false;
      kernelParams = [
        "quiet"
        "splash"
      ];
    };
  };
}
