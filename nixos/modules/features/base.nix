# This feature module contains configuration and options for all systems.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

with lib;

let
  cfg = config.features.base;
in
{
  imports = [ inputs.agenix.nixosModules.age ];

  options = {
    features.base.enable = mkEnableOption "base feature";

    trusted = mkOption {
      type = types.bool;
      default = false;
      description = "Whether agenix secrets have been prepared for this host.";
    };

    user = {
      name = mkOption {
        type = types.str;
        default = "axgfn";
        description = "The name of the default user account.";
      };

      fullName = mkOption {
        type = types.str;
        default = "Alex Griffin";
        description = "The full name of the default user.";
      };

      uid = mkOption {
        type = with types; nullOr int;
        default = 1000;
        description = "The UID of the default user account.";
      };
    };
  };

  config = lib.mkIf cfg.enable {
    # Embed git revision for `nixos-version --json`.
    system.configurationRevision = mkIf (inputs.self ? rev) inputs.self.rev;

    # Point automatic upgrades at this flake.
    system.autoUpgrade.flake = "gitlab:axgfn/dotfiles";

    nix = {
      # Enable support for flakes.
      package = pkgs.nix;
      extraOptions = "extra-experimental-features = nix-command flakes";

      # Protect nix-shells from garbage collection.
      settings.keep-outputs = true;
      settings.keep-derivations = true;

      # Automatically garbage collect the nix store.
      gc.automatic = true;
      gc.dates = "weekly";
      gc.options = "--delete-older-than 14d";

      # Add flake inputs to Nix registry.
      registry = builtins.mapAttrs (_: value: { flake = value; }) inputs;

      # Map registry entries to channels.
      nixPath = lib.mapAttrsToList (name: value: "${name}=${value.to.path}") config.nix.registry;

      # Allow admins additional rights when connecting to the Nix daemon.
      settings.trusted-users = [
        "root"
        "@wheel"
      ];

      # Add nix-community binary cache.
      settings.substituters = [
        "https://axgfn.cachix.org"
        "https://nix-community.cachix.org"
      ];
      settings.trusted-public-keys = [
        "axgfn.cachix.org-1:KhdL12TE3/KvPwSdY8NzPZHRG4soijKazT6A6sP0438="
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
    };
  };
}
