# This module configures the Miniflux feed reader.
{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.features.miniflux;
  hostName = "reader.griffin1.org";
in
{
  options.features.miniflux.enable = lib.mkEnableOption "miniflux module";

  config = lib.mkIf cfg.enable {
    # Enable additional configuration from feature modules.
    features.acme.enable = true;

    # Enable the Miniflux feed reader.
    services.miniflux = {
      enable = true;
      adminCredentialsFile = config.age.secrets.miniflux-creds.path;
      config = {
        BASE_URL = "https://${hostName}/";
        FETCH_YOUTUBE_WATCH_TIME = "1";
        LISTEN_ADDR = "localhost:8123";
      };
    };

    # Serve Miniflux under reader subdomain.
    services.nginx = {
      enable = true;
      virtualHosts.${hostName} = {
        locations."^~ /".proxyPass = "http://${config.services.miniflux.config.LISTEN_ADDR}/";
        enableACME = true;
        acmeRoot = null;
        forceSSL = true;
      };
    };

    age.secrets.miniflux-creds.file = ../../secrets/miniflux-creds.age;
  };
}
