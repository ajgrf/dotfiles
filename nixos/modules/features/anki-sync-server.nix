# This feature module configures a private Anki sync server.
{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.features.anki-sync-server;
  acfg = config.services.anki-sync-server;
  hostName = "anki.griffin1.org";
in
{
  options.features.anki-sync-server.enable = lib.mkEnableOption "anki-sync-server feature";

  config = lib.mkIf cfg.enable {
    assertions = [
      {
        assertion = config.trusted;
        message = "anki-sync-server feature requires agenix secrets to be prepared.";
      }
    ];

    # Enable additional configuration from feature modules.
    features.acme.enable = true;

    # Serve Anki sync server under paperless subdomain.
    services.nginx = {
      enable = true;
      virtualHosts.${hostName} = {
        locations."^~ /".proxyPass = "http://${acfg.address}:${toString acfg.port}/";
        enableACME = true;
        acmeRoot = null;
        forceSSL = true;
      };
    };

    # Enable private Anki sync server.
    services.anki-sync-server = {
      enable = true;
      package = pkgs.anki-sync-server;
      address = "127.0.0.1";
      users = [
        {
          username = "axgfn";
          passwordFile = config.age.secrets.anki-password.path;
        }
      ];
    };

    age.secrets.anki-password.file = ../../secrets/anki-password.age;
  };
}
