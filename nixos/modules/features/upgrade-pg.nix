{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  cfg = config.features.upgrade-pg;
in
{
  options.features.upgrade-pg = {
    enable = lib.mkEnableOption "upgrade-pg feature";

    package = mkOption {
      type = types.package;
      default = pkgs.postgresql;
      description = "PostgreSQL package to upgrade to.";
    };
  };

  config = lib.mkIf cfg.enable {
    # Explicitly enable postgresql so that dependent services can be
    # disabled when this feature is enabled.
    services.postgresql.enable = true;

    containers.temp-pg.config.services.postgresql = {
      enable = true;
      package = cfg.package;
      # set a custom new dataDir
      #dataDir = "/some/data/dir";
    };

    environment.systemPackages =
      let
        newpg = config.containers.temp-pg.config.services.postgresql;
      in
      [
        (pkgs.writeScriptBin "upgrade-pg-cluster" ''
          set -eux

          export OLDDATA="${config.services.postgresql.dataDir}"
          export NEWDATA="${newpg.dataDir}"
          export OLDBIN="${config.services.postgresql.package}/bin"
          export NEWBIN="${newpg.package}/bin"

          systemctl stop postgresql    # old one

          install -d -m 0700 -o postgres -g postgres "$NEWDATA"
          cd "$NEWDATA"
          sudo -u postgres $NEWBIN/initdb -D "$NEWDATA"

          sudo -u postgres $NEWBIN/pg_upgrade \
            --old-datadir "$OLDDATA" --new-datadir "$NEWDATA" \
            --old-bindir $OLDBIN --new-bindir $NEWBIN \
            "$@"
        '')
      ];
  };
}
