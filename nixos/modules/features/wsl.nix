# This feature module contains configuration I want in WSL2.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

let
  cfg = config.features.wsl;
in
{
  imports = [ inputs.wsl.nixosModules.wsl ];

  options.features.wsl.enable = lib.mkEnableOption "wsl feature";

  config = lib.mkIf cfg.enable {
    # Enable additional configuration from feature modules.
    features = {
      base.enable = true;
      user.enable = true;
    };

    # Set up WSL2 environment.
    wsl = {
      enable = true;
      defaultUser = config.user.name;

      # Enable Windows executable interoperability.
      interop.register = true;

      # Ensure hostname is the same before and after system activation.
      wslConf.network.hostname = config.networking.hostName;
    };

    # Run tzupdate service to auto-detect the time zone.
    services.tzupdate.enable = true;

    # Install manpages and other documentation.
    documentation.enable = true;

    # Upgrade NixOS automatically.
    system.autoUpgrade.enable = true;

    # Run ollama server for using local LLMs.
    services.ollama = {
      enable = true;
      package = pkgs.newerPkg.ollama;
    };

    # Needed for gsettings & GTK theming to work.
    programs.dconf.enable = true;
    services.dbus.packages = with pkgs; [ dconf-editor ];
    fonts.packages = with pkgs; [ cantarell-fonts ];
    environment.systemPackages = with pkgs; [
      glib
      adwaita-icon-theme
      gnome-themes-extra
      gsettings-desktop-schemas
    ];

    # HACK: add Emacs to the Windows start menu
    system.activationScripts = {
      copy-emacs-launcher =
        let
          emacs = pkgs.emacs30-pgtk;
        in
        lib.stringAfter [ ] ''
          mkdir -p /usr/share/applications /usr/share/icons

          # Open Emacs with my normal environment variables.
          ${pkgs.gnused}/bin/sed -e 's_sh -c_bash -lc_' \
            -e 's_/nix/store/.*/bin/emacsclient_emacsclient_' \
            ${emacs}/share/applications/emacsclient.desktop \
            >/usr/share/applications/emacsclient.desktop

          ln -sf ${emacs}/share/icons/hicolor -T /usr/share/icons/hicolor
        '';
    };
  };
}
