# This feature module configures an Actual Budget container.
{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.features.actual-budget;
  hostName = "actual.griffin1.org";
in
{
  options.features.actual-budget.enable = lib.mkEnableOption "actual-budget feature";

  config = lib.mkIf cfg.enable {
    # Serve Actual under actual subdomain.
    services.nginx = {
      enable = true;
      virtualHosts.${hostName} = {
        locations."^~ /".proxyPass = "http://localhost:5006/";
        enableACME = true;
        acmeRoot = null;
        forceSSL = true;
      };
    };

    virtualisation.oci-containers = {
      backend = "docker";
      containers.actual-budget = {
        image = "actualbudget/actual-server:23.3.1";
        extraOptions = [ ];
        ports = [ "5006:5006" ];
        volumes = [ "/depot/actual:/data" ];
        environment = { };
      };
    };
  };

}
