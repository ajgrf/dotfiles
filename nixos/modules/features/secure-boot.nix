# This feature module contains configuration for enabling Secure Boot
# with the Lanzaboote project.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

let
  cfg = config.features.secure-boot;
in
{
  imports = [
    inputs.lanzaboote.nixosModules.lanzaboote
  ];

  options.features.secure-boot.enable = lib.mkEnableOption "secure-boot feature";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      sbctl
    ];

    # Lanzaboote currently replaces the systemd-boot module.
    # This setting is usually set to true in configuration.nix
    # generated at installation time. So we force it to false
    # for now.
    boot.loader.systemd-boot.enable = lib.mkForce false;

    boot.lanzaboote = {
      enable = true;
      configurationLimit = 10;
      pkiBundle = "/etc/secureboot";
    };
  };
}
