# This module contains my preferred keyboard remappings.
{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.features.keyboard;
in
{
  options.features.keyboard.enable = lib.mkEnableOption "keyboard feature";

  config = lib.mkIf cfg.enable {
    # Use altgr-intl as the base layout.
    services.xserver.xkb = {
      layout = "us";
      variant = "altgr-intl";
      options = builtins.concatStringsSep "," [
        "caps:ctrl_modifier"
        "compose:menu"
        "grp:shifts_toggle"
      ];
    };

    # Apply keyboard layout to the console too.
    console.useXkbConfig = true;

    # Remap the homepage and explorer media keys on the Truly
    # Ergonomic CLEAVE keyboard.
    services.udev.extraHwdb = ''
      evdev:input:b0003v05ACp0256*
        KEYBOARD_KEY_c0223=brightnessdown
        KEYBOARD_KEY_c0194=brightnessup
    '';
  };
}
