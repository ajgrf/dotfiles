# This feature module sets up opt-in persistence with the impermanence module.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

# TODO: persist nixos containers

with lib;

let
  cfg = config.features.optin-persistence;
  rootDevice = config.fileSystems."/".device;
  rootFsType = config.fileSystems."/".fsType;

  phase1Systemd = config.boot.initrd.systemd.enable;
  wipeScript = ''
    mkdir -p /mnt
    mount ${rootDevice} /mnt
    if test -e /mnt/@root; then
        timestamp=$(date --date="@$(stat -c %Y /mnt/@root)" "+%Y%m%dT%H%M%S")
        mv /mnt/@root "/mnt/@$timestamp--root"
    fi

    delete_subvolume_recursively() {
        IFS=$'\n'
        for i in $(btrfs subvolume list -o "$1" | cut -f 9- -d ' '); do
            delete_subvolume_recursively "/mnt/$i"
        done
        btrfs subvolume delete "$1"
    }

    for i in $(find /mnt/ -maxdepth 1 -mtime +10 -name '@????????T??????--root'); do
        delete_subvolume_recursively "$i"
    done

    btrfs subvolume create /mnt/@root
    umount /mnt
  '';
in
{
  imports = [ inputs.impermanence.nixosModules.impermanence ];

  options.features.optin-persistence = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        If enabled, configure the system for a tmpfs root, or a btrfs root
        which will be rolled back to a blank snapshot on boot. Use the
        impermanence module to store some state in /persist.
      '';
    };

    rootDeviceUnit = mkOption {
      type = types.str;
      default = "dev-disk-by\\x2dpartlabel-disk\\x2dmain\\x2drootvol.device";
      description = "systemd device unit to wait for before wiping.";
    };
  };

  config = mkMerge [
    (mkIf (cfg.enable && rootFsType == "btrfs") {
      boot.initrd = {
        postDeviceCommands = mkIf (!phase1Systemd) (mkBefore wipeScript);
        systemd.services.wipe-root = mkIf phase1Systemd {
          description = "Roll back btrfs rootfs";
          wantedBy = [ "initrd.target" ];
          requires = [ cfg.rootDeviceUnit ];
          after = [
            cfg.rootDeviceUnit
            "systemd-cryptsetup@cryptvol.service"
          ];
          before = [ "sysroot.mount" ];
          unitConfig.DefaultDependencies = "no";
          serviceConfig.Type = "oneshot";
          script = wipeScript;
        };
      };
    })

    (mkIf cfg.enable {
      assertions = [
        {
          assertion = rootFsType == "btrfs" || rootFsType == "tmpfs";
          message = "Root filesystem with opt-in persistence must be tmpfs or btrfs.";
        }
      ];

      environment.persistence."/persist" = {
        files = [ "/etc/machine-id" ];

        directories =
          [
            "/etc/nixos"
            "/var/lib/nixos"
            "/var/lib/systemd"
            "/var/log"
          ]

          ++ optionals config.features.secure-boot.enable [
            config.boot.lanzaboote.pkiBundle
          ]

          ++ optionals config.networking.networkmanager.enable [
            {
              directory = "/etc/NetworkManager/system-connections";
              mode = "0700";
            }
          ]

          ++ optionals config.services.mullvad-vpn.enable [
            "/etc/mullvad-vpn"
            "/var/cache/mullvad-vpn"
          ]

          ++ optionals config.services.tailscale.enable [ "/var/lib/tailscale" ]

          ++ optionals config.networking.wireless.iwd.enable [
            {
              directory = "/var/lib/iwd";
              mode = "0700";
            }
          ]

          ++ optionals config.hardware.bluetooth.enable [
            {
              directory = "/var/lib/bluetooth";
              mode = "0700";
            }
          ]

          ++ optionals config.services.printing.enable [
            {
              directory = "/var/lib/cups";
              mode = "0775";
            }
          ]

          ++ optionals config.services.accounts-daemon.enable [
            {
              directory = "/var/lib/AccountsService";
              mode = "0775";
            }
          ]

          ++ optionals config.services.fprintd.enable [
            {
              directory = "/var/lib/fprint";
              mode = "0700";
            }
          ]

          ++ optionals config.services.displayManager.sddm.enable [
            {
              directory = "/var/lib/sddm";
              user = "sddm";
              group = "sddm";
              mode = "0750";
            }
          ]

          ++ optionals config.security.sudo.enable [
            {
              directory = "/var/db/sudo";
              mode = "0711";
            }
          ]

          ++ optionals config.services.guix.enable [ (config.services.guix.stateDir + "/guix") ]

          ++ optionals config.virtualisation.libvirtd.enable [ "/var/lib/libvirt" ]

          ++ optionals config.services.postgresql.enable [
            {
              directory = config.services.postgresql.dataDir;
              user = "postgres";
              group = "postgres";
              mode = "0750";
            }
          ]

          ++ optionals config.services.postgresqlBackup.enable [
            {
              directory = config.services.postgresqlBackup.location;
              user = "postgres";
              group = "root";
              mode = "0700";
            }
          ]

          ++ optionals config.features.upgrade-pg.enable [
            {
              directory = config.containers.temp-pg.config.services.postgresql.dataDir;
              user = "postgres";
              group = "postgres";
              mode = "0750";
            }
          ]

          ++ optionals (config.security.acme.certs != { }) [
            {
              directory = "/var/lib/acme";
              user = "acme";
              group = "acme";
            }
          ]

          ++ optionals config.services.portunus.enable [
            {
              directory = config.services.portunus.stateDir;
              user = config.services.portunus.user;
              group = config.services.portunus.group;
              mode = "0750";
            }
          ]

          ++ optionals config.services.nextcloud.enable [
            {
              directory = config.services.nextcloud.datadir;
              user = "nextcloud";
              group = "nextcloud";
              mode = "0755";
            }
          ]

          ++ optionals config.services.syncthing.enable [
            {
              directory = config.services.syncthing.configDir;
              user = config.services.syncthing.user;
              group = config.services.syncthing.group;
              mode = "0700";
            }
            {
              directory = config.services.syncthing.dataDir;
              user = config.services.syncthing.user;
              group = config.services.syncthing.group;
              mode = "0700";
            }
          ]

          ++ optionals config.services.radicale.enable [
            {
              directory = "/var/lib/radicale/collections";
              user = "radicale";
              group = "radicale";
              mode = "0750";
            }
          ]

          ++ optionals config.services.rss-bridge.enable [
            {
              directory = config.services.rss-bridge.dataDir;
              user = config.services.rss-bridge.user;
              group = config.services.rss-bridge.group;
              mode = "0755";
            }
          ]

          ++ optionals config.services.anki-sync-server.enable [
            {
              directory = "/var/lib/private/anki-sync-server";
              user = "anki-sync-server";
              group = "anki-sync-server";
              mode = "0755";
            }
          ]

          ++ optionals config.services.forgejo.enable [
            {
              directory = config.services.forgejo.stateDir;
              user = config.services.forgejo.user;
              group = config.services.forgejo.group;
              mode = "0750";
            }
            {
              directory = config.services.forgejo.repositoryRoot;
              user = config.services.forgejo.user;
              group = config.services.forgejo.group;
              mode = "0750";
            }
          ]

          ++ optionals config.services.gitea.enable [
            {
              directory = config.services.gitea.stateDir;
              user = "gitea";
              group = "gitea";
              mode = "0750";
            }
            {
              directory = config.services.gitea.repositoryRoot;
              user = "gitea";
              group = "gitea";
              mode = "0750";
            }
          ]

          ++ optionals config.services.ollama.enable [
            {
              directory = "/var/lib/private/ollama";
              user = "ollama";
              group = "ollama";
              mode = "0755";
            }
          ]

          ++ optionals config.services.paperless.enable [
            rec {
              directory = config.services.paperless.dataDir;
              user = config.services.paperless.user;
              group = config.users.users.${user}.group;
            }
            # consumptionDir
            # mediaDir
          ]

          ++ optionals config.services.vaultwarden.enable [
            {
              directory = "/var/lib/bitwarden_rs";
              user = "vaultwarden";
              group = "vaultwarden";
              mode = "0700";
            }
          ]

          ++ optionals config.features.actual-budget.enable [ "/depot/actual" ]

          ++ map (backup: {
            directory = "/var/cache/restic-backups-" + backup;
            mode = "0700";
          }) (builtins.attrNames config.services.restic.backups);
      };
    })

    (mkIf (cfg.enable && config.services.openssh.enable) {
      services.openssh.hostKeys = [
        {
          path = "/persist/etc/ssh/ssh_host_ed25519_key";
          type = "ed25519";
        }
        {
          path = "/persist/etc/ssh/ssh_host_rsa_key";
          type = "rsa";
          bits = 4096;
        }
      ];
    })

    (mkIf (cfg.enable && config.services.tzupdate.enable) {
      systemd.services.persist-timezone = {
        description = "copy /etc/localtime symlink to /persist";
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = true;
          ExecStart =
            "-${pkgs.coreutils}/bin/cp --no-dereference --force " + "/persist/etc/localtime /etc/localtime";
          ExecStop =
            "-${pkgs.coreutils}/bin/cp --no-dereference --force " + "/etc/localtime /persist/etc/localtime";
        };
      };
    })

    (mkIf (cfg.enable && config.services.guix.enable) {
      fileSystems."/gnu" = {
        device = "/persist/gnu";
        fsType = "none";
        options = [ "bind" ];
        depends = [ "/persist" ];
      };
    })
  ];
}
