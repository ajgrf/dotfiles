# This feature module configures automated backups with restic.
{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.features.backup;

  hostName = config.networking.hostName;
  passwordFile = config.age.secrets.restic-password.path;
  repository = "s3:s3.us-west-001.backblazeb2.com/ajgbackup/restic";
  environmentFile = config.age.secrets.restic-s3creds.path;
in
{
  options.features.backup.enable = lib.mkEnableOption "backup feature";

  config = lib.mkIf cfg.enable {
    assertions = [
      {
        assertion = config.trusted;
        message = "backup feature requires agenix secrets to be prepared.";
      }
    ];

    # Back up my data.
    services.restic.backups = {
      persist = {
        inherit passwordFile repository environmentFile;
        paths = [ "/persist" ];
        extraBackupArgs = [
          "--exclude=/persist/home"
          "--exclude=/persist/nix"
          "--exclude=/persist/var/cache"
          "--exclude=/persist/var/lib/postgresql"
        ];
        timerConfig = {
          OnCalendar = "*-*-* 00:00:00";
        };
      };

      # Forget & prune snapshots on Mondays.
      prune = {
        inherit passwordFile repository environmentFile;
        paths = [ ];
        pruneOpts = [
          "--host ${hostName}"
          "--keep-daily 14"
          "--keep-weekly 8"
          "--keep-monthly 24"
        ];
        timerConfig = {
          OnCalendar = "Mon *-*-* 12:00:00";
        };
      };
    };

    # Back up nightly database dumps.
    services.postgresqlBackup = {
      enable = config.services.postgresql.enable;
      startAt = "*-*-* 23:55:00";
    };

    age.secrets.restic-password.file = ../../secrets/restic-password.age;
    age.secrets.restic-s3creds.file = ../../secrets/restic-s3creds.age;
  };
}
