# This feature module contains configuration I want on my desktop and NAS machines.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

# TODO: get protonvpn to coexist with tailscale

let
  cfg = config.features.common;
in
{
  options.features.common.enable = lib.mkEnableOption "common feature";

  config = lib.mkIf cfg.enable {
    # TODO: enable apparmor
    # TODO: enable clamav antivirus daemon

    # Enable systemd in initrd.
    boot.initrd.systemd.enable = true;

    # Disable editing the kernel command-line-before boot.
    boot.loader.systemd-boot.editor = false;

    # Select internationalisation properties.
    i18n.defaultLocale = "en_US.UTF-8";

    # Run tzupdate service to auto-detect the time zone.
    services.tzupdate.enable = true;

    # Use NetworkManager to configure network interfaces.
    networking.networkmanager = {
      enable = true;
      wifi = {
        backend = "iwd";
        macAddress = "random";
      };
    };

    # Magic VPN service. Authenticate with `sudo tailscale up`.
    services.tailscale = {
      enable = true;
      package = pkgs.newerPkg.tailscale;
    };

    # Allow all traffic arriving by Tailscale.
    networking.firewall.trustedInterfaces = [ "tailscale0" ];

    # Block adware, malware, gambling, and porn sites.
    networking.hostFiles = [
      (pkgs.writeText "hosts" ''
        0.0.0.0 warzone.com
        0.0.0.0 www.warzone.com
      '')
      "${pkgs.stevenblack-blocklist}/alternates/gambling-porn/hosts"
    ];

    # List packages installed in system profile. To search, run:
    # $ nix search wget
    environment.systemPackages = with pkgs; [
      psmisc
      vim
      wol
    ];

    # Enable the OpenSSH daemon.
    services.openssh = {
      enable = true;
      authorizedKeysFiles = [ "%h/.ssh/authorized_keys2" ];
    };

    # Upgrade NixOS automatically.
    system.autoUpgrade.enable = true;

    # Enable Yubikey support.
    services.pcscd.enable = true;
    services.udev.packages = [ pkgs.yubikey-personalization ];
    # Allow remote Smartcard access for the default user.
    security.polkit.extraConfig = ''
      polkit.addRule(function (action, subject) {
        if (
          (action.id == "org.debian.pcsc-lite.access_pcsc" ||
            action.id == "org.debian.pcsc-lite.access_card") &&
          subject.user == "${config.user.name}"
        ) {
          return polkit.Result.YES;
        }
      });
    '';

    # Enable GnuPG agent with socket-activation for every user session.
    programs.gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };
}
