# This feature module contains configuration for the default NixOS user.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

let
  cfg = config.features.user;
in
{
  imports = [ inputs.home-manager.nixosModules.home-manager ];

  options.features.user.enable = lib.mkEnableOption "user feature";

  config = lib.mkMerge [
    (lib.mkIf cfg.enable {
      # Define a user account. Don't forget to set a password with ‘passwd’.
      users.users.${config.user.name} = {
        description = config.user.fullName;
        isNormalUser = true;
        extraGroups = [
          "wheel"
          "networkmanager"
        ];
        uid = config.user.uid;
      };

      # Build the default user's home environment as part of the
      # system configuration.
      home-manager =
        let
          inherit (inputs.self) homeConfigurations;
          user = config.user.name;
          host = config.networking.hostName;
        in
        {
          backupFileExtension = "hm-bak";
          extraSpecialArgs = {
            inherit inputs;
          };
          useGlobalPkgs = true;
          useUserPackages = false;
          users.${user}.imports = homeConfigurations."${user}@${host}".modules;
        };

    })

    (lib.mkIf (cfg.enable && config.trusted) {
      users.users.${config.user.name}.hashedPasswordFile = config.age.secrets.user-password.path;
      age.secrets.user-password.file = ../../secrets/user-password.age;
    })
  ];
}
