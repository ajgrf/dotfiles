# This feature module configures a Paperless-ngx instance.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

let
  cfg = config.features.paperless;
  pcfg = config.services.paperless;
  hostName = "paperless.griffin1.org";
in
{
  options.features.paperless.enable = lib.mkEnableOption "paperless feature";

  config = lib.mkIf cfg.enable {
    assertions = [
      {
        assertion = config.trusted;
        message = "paperless feature requires agenix secrets to be prepared.";
      }
    ];

    # Enable additional configuration from feature modules.
    features.acme.enable = true;

    # Serve Paperless-ngx under paperless subdomain.
    services.nginx = {
      enable = true;
      clientMaxBodySize = "50m";
      virtualHosts.${hostName} = {
        locations."^~ /".proxyPass = "http://${pcfg.address}:${toString pcfg.port}/";
        enableACME = true;
        acmeRoot = null;
        forceSSL = true;
      };
    };

    # Enable Paperless-ngx service to scan, index, and archive
    # all of your physical documents.
    services.paperless = {
      enable = true;
      passwordFile = config.age.secrets.paperless-adminpass.path;
      settings = {
        PAPERLESS_URL = "https://${hostName}";
        PAPERLESS_OCR_USER_ARGS = "{\"continue_on_soft_render_error\": true}";
        PAPERLESS_DATE_ORDER = "DMY";
        PAPERLESS_FILENAME_DATE_ORDER = "YMD";
      };
    };

    age.secrets.paperless-adminpass = {
      file = ../../secrets/paperless-adminpass.age;
      owner = pcfg.user;
    };
  };
}
