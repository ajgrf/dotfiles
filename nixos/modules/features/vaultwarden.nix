# This feature module configures a Vaultwarden instance.
{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.features.vaultwarden;
  hostName = "vault.griffin1.org";
  port = 8222;
in
{
  options.features.vaultwarden.enable = lib.mkEnableOption "vaultwarden feature";

  config = lib.mkIf cfg.enable {
    # Enable additional configuration from feature modules.
    features.acme.enable = true;

    # Serve Vaultwarden under vault subdomain.
    services.nginx = {
      enable = true;
      virtualHosts.${hostName} = {
        locations."^~ /".proxyPass = "http://localhost:${toString port}/";
        enableACME = true;
        acmeRoot = null;
        forceSSL = true;
      };
    };

    # Enable Vaultwarden, an unofficial Bitwarden-compatible server
    # and password manager.
    services.vaultwarden = {
      enable = true;
      config = {
        domain = "https://${hostName}";
        httpRequestBlockNonGlobalIps = false;
        rocketAddress = "127.0.0.1";
        rocketPort = port;
      };
    };
  };
}
