# This feature module configures a Forgejo instance.
{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.features.forgejo;
  hostName = "git.griffin1.org";
in
{
  options.features.forgejo.enable = lib.mkEnableOption "forgejo feature";

  config = lib.mkIf cfg.enable {
    assertions = [
      {
        assertion = config.trusted;
        message = "forgejo feature requires agenix secrets to be prepared.";
      }
    ];

    # Enable additional configuration from feature modules.
    features.acme.enable = true;

    # Serve Forgejo under git subdomain.
    services.nginx = {
      enable = true;
      virtualHosts.${hostName} = {
        locations."^~ /".proxyPass = "http://localhost:3000/";
        enableACME = true;
        acmeRoot = null;
        forceSSL = true;
      };
    };

    services.forgejo = {
      enable = true;
      repositoryRoot = "/depot/git";
      # Run as a user named git for nicer clone URLs.
      user = "git";

      settings = {
        DEFAULT = {
          APP_NAME = "Forgejo";
        };

        # Enable push to create.
        repository = {
          ENABLE_PUSH_CREATE_USER = true;
          ENABLE_PUSH_CREATE_ORG = true;
        };

        server = {
          ROOT_URL = "https://${hostName}/";
        };

        # Enable code search.
        indexer = {
          REPO_INDEXER_ENABLED = true;
          REPO_INDEXER_PATH = "indexers/repos.bleve";
          MAX_FILE_SIZE = 1048576;
          REPO_INDEXER_INCLUDE = "";
          REPO_INDEXER_EXCLUDE = "resources/bin/**";
        };
      };
    };

    # Create git user.
    users.users.git = {
      home = config.services.forgejo.stateDir;
      useDefaultShell = true;
      group = config.services.forgejo.group;
      isSystemUser = true;
    };
  };
}
