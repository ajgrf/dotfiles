# This feature module configures a Nextcloud instance.
{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.features.nextcloud;
  hostName = "nextcloud.griffin1.org";
in
{
  options.features.nextcloud.enable = lib.mkEnableOption "nextcloud feature";

  config = lib.mkIf cfg.enable {
    assertions = [
      {
        assertion = config.trusted;
        message = "nextcloud feature requires agenix secrets to be prepared.";
      }
    ];

    # Enable additional configuration from feature modules.
    features.acme.enable = true;

    # Serve Nextcloud under nextcloud subdomain.
    services.nginx = {
      enable = true;
      virtualHosts.${hostName} = {
        default = true;
        enableACME = true;
        acmeRoot = null;
        forceSSL = true;
      };
    };

    services.nextcloud = {
      inherit hostName;

      enable = true;
      package = pkgs.nextcloud30;
      https = true;

      extraApps = with pkgs.nextcloud30Packages.apps; {
        inherit
          calendar
          gpoddersync
          memories
          music
          previewgenerator
          ;

        # HACK: work around hash mismatch on nextcloud contacts app
        contacts = pkgs.fetchNextcloudApp {
          appName = "contacts";
          appVersion = "6.1.2";
          url = "https://github.com/nextcloud-releases/contacts/releases/download/v6.1.2/contacts-v6.1.2.tar.gz";
          hash = "sha256-Slk10WZfUQGsYnruBR5APSiuBd3jh3WG1GIqKhTUdfU=";
          homepage = "https://github.com/nextcloud/contacts";
          license = "agpl3Plus";
        };

        external = pkgs.fetchNextcloudApp {
          appName = "external";
          appVersion = "5.5.2";
          url = "https://github.com/nextcloud-releases/external/releases/download/v5.5.2/external-v5.5.2.tar.gz";
          hash = "sha256-yDTs4DaBP5m3SRaWU5hDDmZcfHh0OdVunwgTG7Y5CLs=";
          homepage = "https://github.com/nextcloud/external";
          description = "This application allows an admin to add additional links into the Nextcloud menus. Following a link, the external website appears in the Nextcloud frame. It is also possible to add links only for a given language, device type or user group.

More information is available in the External sites documentation.";
          license = "agpl3Plus";
        };
      };

      # Raise the upload size limit to improve the reliability of syncing.
      maxUploadSize = "10240M";

      config = {
        # Nextcloud PostegreSQL database configuration, recommended over using SQLite
        dbtype = "pgsql";
        dbuser = "nextcloud";
        dbhost = "/run/postgresql";
        dbname = "nextcloud";
        dbpassFile = config.age.secrets.nextcloud-dbpass.path;

        adminuser = "admin";
        adminpassFile = config.age.secrets.nextcloud-adminpass.path;
      };

      # Fix warning about misconfigured OPCache module.
      phpOptions."opcache.interned_strings_buffer" = "16";

      settings = {
        default_locale = "en_US";
        default_phone_region = "US";

        # Enable preview support for HEIC and MOV files.
        enabledPreviewProviders = [
          # Default providers:
          "OC\\Preview\\BMP"
          "OC\\Preview\\GIF"
          "OC\\Preview\\JPEG"
          "OC\\Preview\\Krita"
          "OC\\Preview\\MarkDown"
          "OC\\Preview\\MP3"
          "OC\\Preview\\OpenDocument"
          "OC\\Preview\\PNG"
          "OC\\Preview\\TXT"
          "OC\\Preview\\XBitmap"
          # Extra providers:
          "OC\\Preview\\HEIC"
          "OC\\Preview\\Movie"
        ];

        # Install exiftool for Memories.
        "memories.exiftool" = lib.getExe pkgs.exiftool1270;
        "memories.exiftool_no_local" = false;

        # Run daily maintenance jobs at 19:00 UTC.
        maintenance_window_start = 19;

        # Allow admin access from any domain or IP address.
        trusted_domains = [ "*" ];

        # Delete files from trash after 30 days.
        trashbin_retention_obligation = "auto, 30";
      };
    };

    systemd.services.nextcloud-cron-previewgenerator = {
      serviceConfig = {
        ExecStart = "${config.services.nextcloud.occ}/bin/nextcloud-occ preview:pre-generate";
        Type = "oneshot";
        User = "nextcloud";
        Group = "nextcloud";
      };
    };

    systemd.timers.nextcloud-cron-previewgenerator = {
      wantedBy = [ "timers.target" ];
      after = [ "nextcloud-setup.service" ];
      timerConfig = {
        OnBootSec = "10m";
        OnUnitActiveSec = "10m";
        Unit = "nextcloud-cron-previewgenerator.service";
      };
    };

    services.postgresql = {
      enable = true;
      ensureDatabases = [ "nextcloud" ];
      ensureUsers = [
        {
          name = "nextcloud";
          ensureDBOwnership = true;
        }
      ];
    };

    # Ensure that postgres is running *before* running the setup.
    systemd.services.nextcloud-setup = {
      requires = [ "postgresql.service" ];
      after = [ "postgresql.service" ];
    };

    # Install ffmpeg for video previews.
    environment.systemPackages = with pkgs; [ ffmpeg ];

    age.secrets.nextcloud-dbpass = {
      file = ../../secrets/nextcloud-dbpass.age;
      owner = "nextcloud";
    };
    age.secrets.nextcloud-adminpass = {
      file = ../../secrets/nextcloud-adminpass.age;
      owner = "nextcloud";
    };
  };
}
