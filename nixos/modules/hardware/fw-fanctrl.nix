{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  cfg = config.hardware.fw-fanctrl;
  jsonFormat = pkgs.formats.json { };
  configFile = jsonFormat.generate "config.json" cfg.settings;
in
{
  options.hardware.fw-fanctrl = {
    enable = mkEnableOption "fw-fanctrl daemon";

    package = mkPackageOption pkgs "fw-fanctrl" { };
    ectoolPackage = mkPackageOption pkgs "fw-ectool" { };

    settings = mkOption {
      type = jsonFormat.type;
      default = {
        defaultStrategy = "default";
        strategyOnDischarging = "";
        strategies = {
          sleep = {
            fanSpeedUpdateFrequency = 5;
            movingAverageInterval = 40;
            speedCurve = [
              {
                temp = 0;
                speed = 0;
              }
            ];
          };
          default = {
            fanSpeedUpdateFrequency = 5;
            movingAverageInterval = 30;
            speedCurve = [
              {
                temp = 0;
                speed = 0;
              }
              {
                temp = 50;
                speed = 0;
              }
              {
                temp = 60;
                speed = 25;
              }
              {
                temp = 70;
                speed = 40;
              }
              {
                temp = 80;
                speed = 50;
              }
              {
                temp = 90;
                speed = 80;
              }
              {
                temp = 100;
                speed = 100;
              }
            ];
          };
        };
      };
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.services.fw-fanctrl = {
      description = "Framework Laptop fan controller";
      after = [ "multi-user.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart = "${getExe' cfg.package "fw-fanctrl"} --run --config ${configFile} --no-log";
        ExecStopPost = "${getExe' cfg.ectoolPackage "ectool"} autofanctrl";
        Restart = "always";
        RestartSec = "1s";
      };
    };

    powerManagement.resumeCommands = ''
      ${getExe' cfg.package "fw-fanctrl"} --resume
    '';

    powerManagement.powerDownCommands = ''
      ${getExe' cfg.package "fw-fanctrl"} --pause
    '';
  };
}
