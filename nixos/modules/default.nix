{
  fw-fanctrl = ./hardware/fw-fanctrl.nix;
  acme-feature = ./features/acme.nix;
  actual-budget-feature = ./features/actual-budget.nix;
  anki-sync-server-feature = ./features/anki-sync-server.nix;
  backup-feature = ./features/backup.nix;
  base-feature = ./features/base.nix;
  common-feature = ./features/common.nix;
  desktop-feature = ./features/desktop.nix;
  forgejo-feature = ./features/forgejo.nix;
  keyboard-feature = ./features/keyboard.nix;
  secure-boot-feature = ./features/secure-boot.nix;
  miniflux-feature = ./features/miniflux.nix;
  nextcloud-feature = ./features/nextcloud.nix;
  optin-persistence-feature = ./features/optin-persistence.nix;
  paperless-feature = ./features/paperless.nix;
  plymouth-feature = ./features/plymouth.nix;
  rclonefs-feature = ./features/rclonefs.nix;
  upgrade-pg-feature = ./features/upgrade-pg.nix;
  user-feature = ./features/user.nix;
  vaultwarden-feature = ./features/vaultwarden.nix;
  wsl-feature = ./features/wsl.nix;
}
