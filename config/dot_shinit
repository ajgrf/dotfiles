export GPG_TTY=$(tty)

alias ls='LC_COLLATE=C ls --color=auto --group-directories-first -v'
alias la='ls -A'
alias ll='ls --time-style=long-iso -lah'
unalias l >/dev/null 2>&1 || true

alias doch='eval sudo $(fc -ln -1)'

# pretty print working directory
ppwd() {
	local dir="${PWD}/" n="${1:-255}"

	case "$dir" in
	"${HOME}/"*)
		dir="~/${dir#$HOME/}"
		;;
	esac
	dir="${dir%/}"

	if test "${#dir}" -gt "$n"; then
		n=$((n - 3))
		while test "${#dir}" -gt "$n"; do
			dir="${dir#?}"
		done
		dir="...${dir}"
	fi

	echo "$dir"
}

prompt() {
	local e=$? user="${USER}" hostname="${HOSTNAME%%.*}" env="" S='$'
	test "$e" -eq 0 && e=""
	test "${USER_ID:=$(id -u)}" -eq 0 && S='#'

	if type git >/dev/null 2>&1; then
		branch=$(git symbolic-ref --short HEAD 2>/dev/null)
		env="${branch:+($branch)}"
	fi
	env="${env}${GUIX_ENVIRONMENT:+[env]}"

	case "$PATH" in
	/nix/store*) env="${env}[nix]" ;;
	*:/nix/store*) env="${env}[nix]" ;;
	esac

	echo -n "${e:+$e|}${user}@${hostname}:$(ppwd 28)${env:+ $env}$S "
}
PS1='$(prompt || echo "> ")'

case "$TERM" in
xterm* | alacritty* | rxvt* | st* | screen* | tmux*)
	printf '\e]0;%s\a' "${USER}@${HOSTNAME%%.*} ($0)"
	;;
esac
