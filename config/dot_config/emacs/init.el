;;; init.el -*- lexical-binding: t; -*-

;; TODO: add verb package for interacting with rest apis
;; TODO: add ready-player-mode to open media files in emacs

;; This file just sets up the load-path and loads the rest of the
;; configuration from other files.

(add-to-list 'load-path
             (expand-file-name "lisp" user-emacs-directory))

(require '+core)
(require '+editor)
(require '+ui)
(require '+popup)
(require '+leader)
(require '+completion)
(require '+meow)
(require '+snippets)
(require '+workspaces)
(require '+dired)
(when HAS-GIT
  (require '+git))
(require '+org)
(require '+denote)
(unless IS-ANDROID
  (require '+eat)
  (require '+eshell)
  (require '+shell))
(when (executable-find "dtach")
  (require '+detached))
(when (executable-find "pass")
  (require '+auth))
(require '+llm)
(unless (or IS-ANDROID IS-TERMUX)
  (require '+pdf))
(require '+youtube-dl)
(require '+check)
(require '+lsp)
(require '+lang-cc)
(require '+lang-clojure)
(require '+lang-emacs-lisp)
(require '+lang-go)
(require '+lang-haskell)
(require '+lang-javascript)
(require '+lang-ledger)
(require '+lang-markdown)
(require '+lang-misc)
(require '+lang-nix)
(require '+lang-scheme)
(require '+lang-sh)
(require '+lang-web)
(when (executable-find "mu")
  (require '+mu4e))
(when (executable-find "vdirsyncer")
  (require '+vdir))
(unless (or IS-ANDROID IS-TERMUX)
  (require '+emms)
  (require '+elfeed)
  (require '+nov))
