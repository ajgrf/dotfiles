;;; early-init.el -*- lexical-binding: t; -*-

;; This file is for settings that should be set as early as possible
;; during Emacs startup, before the package system and GUI is
;; initialized.

(defun display-startup-time-h ()
  "Display the time taken for Emacs to initialize.
Includes more in the measurement than `emacs-init-time', such as
`after-init-hook'."
  (unless (bound-and-true-p after-startup-time)
    (setq after-startup-time (current-time)))
  (let ((elapsed (float-time (time-subtract after-startup-time before-init-time))))
    (message "[Emacs initialized in %.03fs]" elapsed)))
(add-hook 'emacs-startup-hook #'display-startup-time-h 90)

;; Speed up startup with some tricks from Doom Emacs.

;; Save values to be restored later.
(setq default-file-name-handler-alist file-name-handler-alist
      default-gc-threshold gc-cons-threshold
      default-gc-percentage gc-cons-percentage)

;; Special handlers are not needed to read files during startup.
;; Keep only the compression handler, because Emacs 29+ will error otherwise.
(setq file-name-handler-alist
      (list (rassq 'jka-compr-handler default-file-name-handler-alist)))

;; Suppress the garbage collector during startup.
(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 0.6)

(defun restore-startup-values-h ()
  "Restore values that were changed to speed up Emacs startup."
  (setq file-name-handler-alist default-file-name-handler-alist
        gc-cons-threshold default-gc-threshold
        gc-cons-percentage default-gc-percentage))
(add-hook 'emacs-startup-hook #'restore-startup-values-h)

;; Load newer lisp files over old compiled files.
(setq load-prefer-newer noninteractive)

;; Native compilation settings.
(when (featurep 'native-compile)
  ;; Silence compiler warnings.
  (setq native-comp-async-report-warnings-errors 'silent)

  ;; JIT compile lisp files.
  (setq native-comp-deferred-compilation t)

  ;; Move native compilation cache to `no-littering-var-directory'.
  (when (fboundp 'startup-redirect-eln-cache)
    (startup-redirect-eln-cache
     (convert-standard-filename
      (expand-file-name  "var/eln-cache/" user-emacs-directory)))))

;; Make tools installed by Termux available to the Android Emacs port.
(when (eq system-type 'android)
  (setenv "PATH" (format "%s:%s" "/data/data/com.termux/files/usr/bin"
                         (getenv "PATH")))
  (push "/data/data/com.termux/files/usr/bin" exec-path))

;; Set initial frame parameters. Doing it here is faster and prevents jarring
;; redisplays during startup. Also, avoid calling `menu-bar-mode', `tool-bar-mode',
;; and `scroll-bar-mode' because they do extra work.
(unless (eq system-type 'android)
  (push '(menu-bar-lines . 0)   default-frame-alist)
  (push '(tool-bar-lines . 0)   default-frame-alist)
  (push '(vertical-scroll-bars) default-frame-alist)
  ;; Set these to nil so that reactivating them doesn't require
  ;; toggling the modes twice.
  (setq menu-bar-mode nil
        tool-bar-mode nil
        scroll-bar-mode nil)

  ;; Set initial window geometry.
  (push '(width  . 85) default-frame-alist)
  (push '(height . 43) default-frame-alist)

  ;; Set initial font.
  (push '(font . "Iosevka Flare-12") default-frame-alist)

  ;; Reduce the size of fringes while we're at it.
  (push '(left-fringe  . 4) default-frame-alist)
  (push '(right-fringe . 4) default-frame-alist)

  ;; Hide mode-line too.
  (setq-default mode-line-format nil)

  ;; Avoid white flash on startup.
  ;; https://protesilaos.com/emacs/dotemacs/#h:a18a059d-4e62-4fd7-8c0b-1135a771a7aa
  (set-face-attribute 'default nil :background "#000000" :foreground "#ffffff")
  (set-face-attribute 'mode-line nil :background "#000000" :foreground "#ffffff" :box 'unspecified)

  ;; New frames shouldn't retain the faces above.
  (defun re-enable-frame-theme-h (_frame)
    "Re-enable active theme upon FRAME creation."
    (when-let ((theme (car custom-enabled-themes)))
      (enable-theme theme)))
  (add-hook 'after-make-frame-functions #'re-enable-frame-theme-h))

;; Make UTF-8 the default coding system.
(set-language-environment "UTF-8")
(setq default-input-method nil)
