;;; +nov.el -*- lexical-binding: t; -*-

;; Featureful EPUB reader mode.
(use-package nov
  :ensure t
  :mode ("\\.epub\\'" . nov-mode)
  :hook (nov-mode . visual-line-mode)
  :config
  (setopt nov-text-width t)

  (defun +nov-set-font-h ()
    (face-remap-add-relative 'variable-pitch
                             :family "Gentium Plus"
                             :height 1.0))
  (add-hook 'nov-mode-hook #'+nov-set-font-h))

(provide '+nov)
