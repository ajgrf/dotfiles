;;; +lang-nix.el -*- lexical-binding: t; -*-

;; Major mode for editing Nix files.
(use-package nix-mode
  :ensure t
  :defer t)

;; Major mode for Nix expressions, powered by tree-sitter.
(use-package nix-ts-mode
  :ensure t
  :if (treesit-available-p)
  :defer t
  :init
  (add-to-list 'major-mode-remap-alist '(nix-mode . nix-ts-mode))
  (add-to-list 'treesit-language-source-alist
               '(nix "https://github.com/nix-community/tree-sitter-nix.git"))
  :config
  (eval-after-load 'eglot
    '(if-let ((pair (assoc 'nix-mode eglot-server-programs)))
         (setf (car pair) '(nix-mode nix-ts-mode)))))

;; Decrypt and encrypt agenix secrets.
(use-package agenix
  :ensure t
  :hook (agenix-pre-mode . envrc-mode)
  :custom
  (agenix-key-files '("/dev/shm/key.txt")))

(provide '+lang-nix)
