;;; +lang-emacs-lisp.el -*- lexical-binding: t; -*-

;; Emacs Lisp mode.
(use-package elisp-mode
  :defer t
  :bind ( :map emacs-lisp-mode-map
          ("C-c RET" . macrostep-expand)
          ("C-c C-z" . ielm)
          ("C-c C-k" . eval-buffer))
  :config
  (defun +lang-emacs-lisp-set-tab-width-h ()
    (setq-local tab-width 8))
  (add-hook 'emacs-lisp-mode-hook #'+lang-emacs-lisp-set-tab-width-h))

;; Interactive macro expander.
(use-package macrostep
  :ensure t
  :commands macrostep-expand)

(provide '+lang-emacs-lisp)
