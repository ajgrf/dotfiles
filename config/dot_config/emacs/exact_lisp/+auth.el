;;; +auth.el -*- lexical-binding: t; -*-

;; Authentication sources for Gnus and Emacs.
(use-package auth-source
  :defer t
  :init
  ;; Disable password cache expiration.
  (setopt auth-source-cache-expiry nil))

;; Integrate auth-source with password-store.
(use-package auth-source-pass
  :if (not IS-TERMUX)
  :config
  (auth-source-pass-enable))

;; https://old.reddit.com/r/emacs/comments/15fsme8/use_only_one_password_in_authinfo_with_mu4e_and/
(defun +auth-get-secret (host &optional user)
  "Wrapper for `auth-source-search' to get the secret for HOST."
  (let* ((auth-info (auth-source-search :host host
                                        :user user
                                        :require '(:secret)))
         (secret (funcall (plist-get (car auth-info) :secret))))
    secret))

(provide '+auth)
