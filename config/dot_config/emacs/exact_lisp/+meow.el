;;; +meow.el -*- lexical-binding: t; -*-

;; TODO: add meow-tree-sitter package

(use-package meow
  :ensure t
  :config
  ;; Yank from and kill to the system clipboard.
  (setopt meow-use-clipboard t)

  (defun +meow-setup ()
    "Set up meow key bindings."
    (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
    (meow-motion-define-key
     '("j" . meow-next)
     '("k" . meow-prev)
     '("<escape>" . ignore))
    (meow-leader-define-key
     ;; Use SPC (0-9) for digit arguments.
     '("1" . meow-digit-argument)
     '("2" . meow-digit-argument)
     '("3" . meow-digit-argument)
     '("4" . meow-digit-argument)
     '("5" . meow-digit-argument)
     '("6" . meow-digit-argument)
     '("7" . meow-digit-argument)
     '("8" . meow-digit-argument)
     '("9" . meow-digit-argument)
     '("0" . meow-digit-argument)
     '("/" . meow-keypad-describe-key)
     '("?" . meow-cheatsheet))
    (meow-normal-define-key
     '("1" . meow-expand-1)
     '("2" . meow-expand-2)
     '("3" . meow-expand-3)
     '("4" . meow-expand-4)
     '("5" . meow-expand-5)
     '("6" . meow-expand-6)
     '("7" . meow-expand-7)
     '("8" . meow-expand-8)
     '("9" . meow-expand-9)
     '("0" . meow-expand-0)
     '("-" . negative-argument)
     '(";" . meow-reverse)
     '("," . meow-inner-of-thing)
     '("." . meow-bounds-of-thing)
     '("[" . meow-beginning-of-thing)
     '("]" . meow-end-of-thing)
     '("{" . backward-paragraph)
     '("}" . forward-paragraph)
     '("<" . meow-forward-barf)
     '(">" . meow-forward-slurp)
     '("a" . meow-append)
     '("b" . meow-back-word)
     '("B" . meow-back-symbol)
     '("c" . meow-change)
     '("C" . meow-comment)
     '("d" . meow-kill)
     '("e" . meow-next-word)
     '("E" . meow-next-symbol)
     '("f" . meow-block)
     '("F" . meow-to-block)
     '("g" . meow-cancel-selection)
     '("G" . meow-grab)
     '("h" . meow-left)
     '("H" . meow-left-expand)
     '("i" . meow-insert)
     '("j" . meow-next)
     '("J" . meow-next-expand)
     '("k" . meow-prev)
     '("K" . meow-prev-expand)
     '("l" . meow-right)
     '("L" . meow-right-expand)
     '("m" . meow-join)
     '("M" . recenter-top-bottom)
     '("n" . meow-search)
     '("o" . meow-open-below)
     '("O" . meow-open-above)
     '("p" . meow-yank)
     '("P" . meow-replace)
     '("q" . meow-quit)
     '("r" . repeat)
     '("R" . undo-redo)
     '("S" . embrace-commander)
     '("t" . meow-till)
     '("T" . meow-find)
     '("u" . meow-undo)
     '("U" . meow-undo-in-selection)
     '("v" . meow-line)
     '("V" . meow-goto-line)
     '("w" . meow-mark-word)
     '("W" . meow-mark-symbol)
     '("x" . meow-delete)
     '("X" . meow-backward-delete)
     '("y" . meow-save)
     '("Y" . meow-swap-grab)
     '("/" . meow-visit)
     '("'" . meow-pop-selection)
     '("C-z" . meow-ignore-mode)
     '("<escape>" . ignore)))

  (+meow-setup)

  (defun +meow-auto-normal-state ()
    "Switch meow to normal state if it makes sense."
    (when (and meow-mode
               (not (minibufferp))
               (not (eq major-mode 'eat-mode))
               meow-insert-mode)
      (meow-normal-mode)))

  (defvar +meow-idle-interval (if IS-ANDROID 10 30)
    "Number of seconds to wait before forcing meow normal state.")

  (defvar +meow--normal-timer
    (run-with-idle-timer +meow-idle-interval t
                         #'+meow-auto-normal-state)
    "The meow idle timer.")

  ;; Define dummy state for disabling meow in certain modes.
  (defvar meow-ignore-state-keymap (make-keymap))
  (meow-define-state ignore
    "Meow IGNORE state minor mode."
    :lighter " [E]"
    :keymap meow-ignore-state-keymap)

  (meow-define-keys 'ignore
    '("C-z" . meow-normal-mode))

  ;; Set initial state for certain major modes.
  (dolist (item '((difftastic-mode . motion)
                  (eat-mode . insert)
                  (emms-playlist-mode . motion)
                  (eshell-mode . insert)
                  (mu4e-view-mode . motion)))
    (add-to-list 'meow-mode-state-list item))

  (meow-global-mode +1))

(provide '+meow)
