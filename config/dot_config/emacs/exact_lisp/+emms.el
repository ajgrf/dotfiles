;;; +emms.el -*- lexical-binding: t; -*-

;; Emacs MultiMedia System - play audio and video within Emacs.
(use-package emms
  :ensure t
  :commands emms
  :bind ( :map mode-specific-map
          ("U" . emms)
          :map emms-playlist-mode-map
          ("DEL" . +emms-reset-playback-speed)
          ("["   . +emms-decrease-playback-speed)
          ("]"   . +emms-increase-playback-speed)
          ("B a" . emms-browse-by-artist)
          ("B b" . emms-browse-by-album)
          ("B c" . emms-browse-by-composer)
          ("B g" . emms-browse-by-genre)
          ("B p" . emms-browse-by-performer)
          ("B y" . emms-browse-by-year)
          ("S d" . +emms-playlist-sort-by-info-playing-time)
          ("S z" . emms-shuffle)
          ("g"   . emms-uniq)
          ("i"   . nil)
          ("i d" . emms-insert-directory-tree)
          ("i f" . emms-insert-file)
          ("i p" . emms-insert-playlist)
          ("i u" . emms-insert-url)
          ("l"   . emms-metaplaylist-mode-go)
          ("t r" . emms-toggle-repeat-playlist)
          ("t t" . emms-toggle-repeat-track)
          ("u"   . emms-playlist-mode-undo)
          ([remap emms-stop] . +emms-stop-watch-later)
          ;; Remap next/prev track bindings.
          ("n"   . next-line)
          ("p"   . previous-line)
          ("f"   . emms-next)
          ("b"   . emms-previous)
          ("F"   . emms-show))
  :init
  (setopt emms-playlist-buffer-name "*EMMS Playlist*")
  :config
  (defun +emms-setup ()
    "My custom Emms setup script."
    (require 'emms-source-file)
    (require 'emms-source-playlist)
    (require 'emms-player-simple)
    (require 'emms-player-mpv)
    (require 'emms-playlist-mode)
    (require 'emms-info)
    (require 'emms-info-exiftool)
    (require 'emms-info-native)
    (require 'emms-cache)
    (require 'emms-mode-line)
    (require 'emms-playing-time)
    (require 'emms-playlist-sort)
    (require 'emms-browser)
    (require 'emms-bookmarks)
    (require 'emms-metaplaylist-mode)
    (require 'emms-history)
    (require 'emms-i18n)
    (require 'emms-volume)

    (setopt emms-player-list '(emms-player-mpv)
            emms-info-functions '(emms-info-exiftool)
            emms-track-description-function #'+emms-info-track-description
            emms-mode-line-format " \u266B [%s]"
            emms-playing-time-style 'downtime
            emms-playlist-uniq-function #'+emms-playlist-uniq-fn)
    (setq emms-mode-line-icon-enabled-p nil)

    (add-to-list 'emms-track-initialize-functions #'emms-info-initialize-track)
    (emms-cache 1)
    (emms-mode-line-mode 1)
    (emms-mode-line-blank)
    (emms-playing-time-mode 1)
    (add-hook 'emms-player-started-hook #'emms-last-played-update-current)
    (add-hook 'emms-player-finished-hook #'+emms-trash-tmp-files-h)
    (add-hook 'emms-playlist-mode-hook #'+emms-disable-show-paren-h)
    (emms-history-load))

  (defun +emms-disable-show-paren-h ()
    (show-paren-local-mode -1))

  (defun +emms-playlist-uniq-fn ()
    "Remove duplicate and non-existent tracks from the current buffer."
    (emms-playlist-ensure-playlist-buffer)
    (let ((hash (make-hash-table :test 'equal))
          (track-points nil))
      (save-excursion
        (goto-char (point-min))
        (emms-walk-tracks
          (setq track-points (cons (point) track-points)))
        ;; Walking tracks in reverse order now.
        (dolist (track-point track-points)
          (let* ((track (emms-playlist-track-at track-point))
                 (track-desc (funcall emms-track-description-function track))
                 (track-file (emms-track-name track)))
            (when (or (gethash track-desc hash)
                      (and (emms-track-file-p track)
                           (not (file-exists-p track-file))))
              (goto-char track-point)
              (emms-playlist-mode-kill-track))
            (puthash track-desc t hash))))))

  (defun +emms-playlist-total-playing-time ()
    "Return the total playing time of all items in the current playlist."
    (interactive)
    (require 'ts)
    (emms-playlist-ensure-playlist-buffer)
    (let ((total-playing-time 0))
      (save-excursion
        (goto-char (point-min))
        (emms-walk-tracks
          (let* ((track (emms-playlist-track-at (point)))
                 (track-playing-time (emms-track-get track 'info-playing-time)))
            (setq total-playing-time (+ track-playing-time total-playing-time)))))
      (message (ts-human-format-duration total-playing-time))))

  (defun +emms-trash-tmp-files-h ()
    "Send files in the Desktop or Downloads folder to the trash after
EMMS finishes playing them."
    (let* ((current-track (emms-playlist-current-selected-track))
           (track-file (emms-track-name current-track)))
      (when (and (+emms-track-tmp-p current-track)
                 (file-exists-p track-file))
        (move-file-to-trash track-file))))

  (defun +emms-track-tmp-p (track)
    "Return t if TRACK is a file in the Desktop or Downloads folder."
    (require 'xdg)
    (let* ((track-file (emms-track-name track))
           (track-dir (file-name-directory track-file))
           (desktop-dir (xdg-user-dir "DESKTOP"))
           (download-dir (xdg-user-dir "DOWNLOAD")))
      (or (file-in-directory-p track-dir desktop-dir)
          (file-in-directory-p track-dir download-dir))))

  (defun +emms-info-track-description (track)
    "Return a description of TRACK."
    (let ((description (emms-info-track-description track))
          (duration (emms-browser-track-duration track)))
      (cond
       ((and (+emms-track-tmp-p track)
             (not (string-empty-p duration)))
        (concat "(" duration ") " description))
       (t description))))

  (defun +emms-stop-watch-later ()
    "Save file position before stopping a track with the mpv backend."
    (interactive)
    (when (emms-player-mpv-proc-playing-p)
      (emms-player-mpv-cmd '(write-watch-later-config)))
    (emms-stop))

  (defun +emms-reset-playback-speed ()
    "Reset playback speed with the mpv backend."
    (interactive)
    (when (emms-player-mpv-proc-playing-p)
      (emms-player-mpv-cmd '(osd-msg set speed 1/1.0))))

  (defun +emms-decrease-playback-speed ()
    "Decrease playback speed 10% with the mpv backend."
    (interactive)
    (when (emms-player-mpv-proc-playing-p)
      (emms-player-mpv-cmd '(osd-msg multiply speed 1/1.1))))

  (defun +emms-increase-playback-speed ()
    "Increase playback speed 10% with the mpv backend."
    (interactive)
    (when (emms-player-mpv-proc-playing-p)
      (emms-player-mpv-cmd '(osd-msg multiply speed 1.1))))

  (defun +emms-playlist-sort-by-info-playing-time ()
    "Sort emms playlist by info-playing-time, increasingly.
With a prefix argument, decreasingly."
    (interactive)
    (emms-playlist-sort
     '(lambda (a b)
        (funcall
         (if current-prefix-arg '> '<)
         (emms-track-get a 'info-playing-time)
         (emms-track-get b 'info-playing-time)))))

  (+emms-setup))

;; Display the emms mode line as a ticker.
(use-package emms-mode-line-cycle
  :ensure t
  :after emms
  :config
  (setopt emms-mode-line-cycle-max-width 24)
  (emms-mode-line-cycle 1))

;; Control Sonos speakers with EMMS.
(when (and HAS-GIT (executable-find "sonos"))
  (use-package emms-player-sonos
    :vc (emms-player-sonos :url "https://gitlab.com/axgfn/emms-player-sonos.git")
    :after emms))

(provide '+emms)
