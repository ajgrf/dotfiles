;;; +lang-markdown.el -*- lexical-binding: t; -*-

;; Major mode for Markdown-formatted text.
(use-package markdown-mode
  :ensure t
  :mode ("README\\.md\\'" . gfm-mode)
  :config
  (setopt markdown-command "pandoc"))

;; Edit regions in separate buffers.
(use-package edit-indirect
  :ensure t
  :defer t)

(provide '+lang-markdown)
