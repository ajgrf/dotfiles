;;; +mu4e.el -*- lexical-binding: t; -*-

;; TODO: add consult-mu package

;; =mu4e= is an email client for Emacs based on the =mu= (maildir-utils)
;; search engine.
(use-package mu4e
  :commands mu4e
  :bind ( :map mode-specific-map
          ("M" . +mu4e))
  :init
  (defun +mu4e ()
    "Open mu4e in a dedicated tab."
    (interactive)
    (let ((workspace "*mu4e*")
          (tabs (mapcar (lambda (tab)
                          (alist-get 'name tab))
                        (tab-bar-tabs))))
      (if (member workspace tabs)
          (tab-bar-switch-to-tab workspace)
        (tab-bar-new-tab)
        (tab-bar-rename-tab workspace)
        (mu4e))))
  :config
  (require 'xdg)

  ;; Use =mu4e= for e-mail in Emacs.
  (setopt mail-user-agent 'mu4e-user-agent
          message-mail-user-agent 'mu4e-user-agent)

  ;; File locations
  (setq mu4e-attachment-dir (concat (xdg-user-dir "DESKTOP") "/"))

  ;; Retrieval
  (setopt mu4e-get-mail-command "mail-sync"
          mu4e-change-filenames-when-moving t
          ;; update mail in the background
          mu4e-update-interval 900)

  ;; Sending
  (setopt sendmail-program (executable-find "msmtp")
          send-mail-function #'smtpmail-send-it
          message-sendmail-f-is-evil t
          message-sendmail-extra-arguments '("--read-envelope-from")
          message-send-mail-function #'message-send-mail-with-sendmail)

  ;; Interface
  (setopt mu4e-completing-read-function #'completing-read
          mu4e-confirm-quit nil
          message-kill-buffer-on-exit t
          mu4e-use-fancy-chars nil
          mu4e-headers-include-related nil
          ;; Remove 'lists' column.
          mu4e-headers-fields '((:flags . 6)
                                (:human-date . 9)
                                (:from-or-to . 20)
                                (:subject)))

  (setq mu4e-headers-thread-single-orphan-prefix '("─>" . "─▶")
        mu4e-headers-thread-orphan-prefix '("┬>" . "┬▶ ")
        mu4e-headers-thread-last-child-prefix '("└>" . "╰▶")
        mu4e-headers-thread-child-prefix '("├>" . "├▶")
        mu4e-headers-thread-connection-prefix '("│" . "│ "))

  (defun +mu4e-close-tab-a (&optional _bury)
    "Close mu4e tab when quitting mu4e."
    (tab-bar-close-tab-by-name "*mu4e*"))
  (advice-add 'mu4e-quit :after #'+mu4e-close-tab-a)

  ;; Composing
  (setopt mml-attach-file-at-the-end t)

  ;; About me
  (setopt user-full-name "Alex Griffin"
          user-mail-address "alex.griffin@axgfn.com"
          ;; mu4e-compose-signature "Alex Griffin"
          org-msg-signature (concat "\n\nThanks,\n"
                                    "#+BEGIN_SIGNATURE\n"
                                    "-- \n"
                                    "Alex Griffin\n"
                                    "#+END_SIGNATURE\n"))

  ;; Contexts
  (setopt mu4e-context-policy 'pick-first
          mu4e-compose-context-policy 'ask-if-none)

  (setq mu4e-contexts
        `(,(make-mu4e-context
            :name "proton"
            :enter-func (lambda () (mu4e-message "Switched to %s" "proton"))
            :leave-func (lambda () (mu4e-clear-caches))
            :match-func
            (lambda (msg)
              (when msg
                (string-prefix-p (format "/%s" "proton")
                                 (mu4e-message-field msg :maildir) t)))
            :vars '((mu4e-drafts-folder   . "/proton/Drafts")
                    (mu4e-sent-folder     . "/proton/Sent")
                    (mu4e-refile-folder   . "/proton/Archive")
                    (mu4e-trash-folder    . "/proton/Trash")
                    (+mu4e-reply-folder   . "/proton/Folders/1 Reply")
                    (+mu4e-do-folder      . "/proton/Folders/2 Do")
                    (+mu4e-capture-folder . "/proton/Folders/3 Capture")
                    (+mu4e-remind-folder  . "/proton/Folders/4 Remind")
                    (+mu4e-review-folder  . "/proton/Folders/5 Review")
                    (smtpmail-smtp-user   . "alex.griffin@axgfn.com")
                    (user-mail-address    . "alex.griffin@axgfn.com")))
          ,(make-mu4e-context
            :name "focus"
            :enter-func (lambda () (mu4e-message "Switched to %s" "focus"))
            :leave-func (lambda () (mu4e-clear-caches))
            :match-func
            (lambda (msg)
              (when msg
                (string-prefix-p (format "/%s" "focus")
                                 (mu4e-message-field msg :maildir) t)))
            :vars '((mu4e-drafts-folder   . "/focus/[Gmail]/Drafts")
                    (mu4e-sent-folder     . "/focus/[Gmail]/Sent Mail")
                    (mu4e-refile-folder   . "/focus/[Gmail]/All Mail")
                    (mu4e-trash-folder    . "/focus/[Gmail]/Trash")
                    (+mu4e-reply-folder   . "/focus/1 Reply")
                    (+mu4e-do-folder      . "/focus/2 Do")
                    (+mu4e-capture-folder . "/focus/3 Capture")
                    (+mu4e-remind-folder  . "/focus/4 Remind")
                    (+mu4e-review-folder  . "/focus/5 Review")
                    (smtpmail-smtp-user   . "alex.griffin@focusengineeringinc.com")
                    (user-mail-address    . "alex.griffin@focusengineeringinc.com")
                    ;; don't save message to Sent Messages, Gmail/IMAP takes care of this
                    (mu4e-sent-messages-behavior . delete)))))

  ;; Don't set IMAP T flag (trashed).
  ;; https://github.com/djcb/mu/issues/1136
  (setf (plist-get (alist-get 'trash mu4e-marks) :action)
        (lambda (docid msg target)
          (mu4e--server-move docid (mu4e--mark-check-target target) "-N"))) ; Instead of "+T-N"

  ;; Bookmarks
  (setopt mu4e-bookmarks
          '((:name "Unified inbox"
                   :query "maildir:/proton/Inbox OR maildir:/focus/Inbox"
                   :key ?i)
            (:name "Unread messages"
                   :query "flag:unread AND NOT flag:trashed"
                   :key ?u)
            (:name "Flagged messages"
                   :query "flag:flagged"
                   :key ?f)
            (:name "Last 7 days"
                   :query "date:7d..now"
                   :hide-unread t
                   :key ?w)
            (:name "Junk messages"
                   :query "maildir:/proton/Spam OR maildir:/focus/[Gmail]/Spam"
                   :hide-unread t
                   :key ?j)
            (:name "Active messages"
                   :query "maildir:/proton/Inbox OR \"maildir:/proton/Folders/1 Reply\" OR \"maildir:/proton/Folders/2 Do\" OR \"maildir:/proton/Folders/3 Capture\" OR \"maildir:/proton/Folders/4 Remind\" OR \"maildir:/proton/Folders/5 Review\" OR maildir:/focus/Inbox OR \"maildir:/focus/1 Reply\" OR \"maildir:/focus/2 Do\" OR \"maildir:/focus/3 Capture\" OR \"maildir:/focus/4 Remind\" OR \"maildir:/focus/5 Review\""
                   :key ?a)
            (:name "1 Reply"
                   :query "\"maildir:/proton/Folders/1 Reply\" OR \"maildir:/focus/1 Reply\""
                   :key ?1)
            (:name "2 Do"
                   :query "\"maildir:/proton/Folders/2 Do\" OR \"maildir:/focus/2 Do\""
                   :key ?2)
            (:name "3 Capture"
                   :query "\"maildir:/proton/Folders/3 Capture\" OR \"maildir:/focus/3 Capture\""
                   :key ?3)
            (:name "4 Remind"
                   :query "\"maildir:/proton/Folders/4 Remind\" OR \"maildir:/focus/4 Remind\""
                   :key ?4)
            (:name "5 Review"
                   :query "\"maildir:/proton/Folders/5 Review\" OR \"maildir:/focus/5 Review\""
                   :key ?5))
          mu4e-maildir-shortcuts
          '((:maildir "/proton/Inbox"
                      :key ?p)
            (:maildir "/focus/Inbox"
                      :key ?f)))

  ;; Add custom marks for moving messages to action folders.

  (defun +mu4e--get-folder (foldervar msg)
    "Within the mu-context of MSG, get message folder FOLDERVAR.
If FOLDER is a string, return it, if it is a function, evaluate
this function with MSG as parameter which may be nil, and return
the result."
    (unless (member foldervar
                    '(mu4e-sent-folder mu4e-drafts-folder
                                       mu4e-trash-folder mu4e-refile-folder
                                       +mu4e-reply-folder
                                       +mu4e-do-folder
                                       +mu4e-capture-folder
                                       +mu4e-remind-folder
                                       +mu4e-review-folder))
      (mu4e-error "Invalid folder"))
    ;; get the value with the vars for the relevant context let-bound
    (with-mu4e-context-vars (mu4e-context-determine msg nil)
        (let* ((folder (symbol-value foldervar))
               (val
                (cond
                 ((stringp   folder) folder)
                 ((functionp folder) (funcall folder msg))
                 (t (mu4e-error "Unsupported type for %S" folder)))))
          (or val (mu4e-error "%S evaluates to nil" foldervar)))))

  (add-to-list 'mu4e-marks
               '(stack-reply
                 :char   "1"
                 :prompt "stack-reply"
                 :dyn-target (lambda (target msg)
                               (+mu4e--get-folder '+mu4e-reply-folder msg))
                 :action (lambda (docid msg target)
                           (mu4e--server-move docid (mu4e--mark-check-target target) "-N"))))
  (add-to-list 'mu4e-marks
               '(stack-do
                 :char   "2"
                 :prompt "stack-do"
                 :dyn-target (lambda (target msg)
                               (+mu4e--get-folder '+mu4e-do-folder msg))
                 :action (lambda (docid msg target)
                           (mu4e--server-move docid (mu4e--mark-check-target target) "-N"))))
  (add-to-list 'mu4e-marks
               '(stack-capture
                 :char   "3"
                 :prompt "stack-capture"
                 :dyn-target (lambda (target msg)
                               (+mu4e--get-folder '+mu4e-capture-folder msg))
                 :action (lambda (docid msg target)
                           (mu4e--server-move docid (mu4e--mark-check-target target) "-N"))))
  (add-to-list 'mu4e-marks
               '(stack-remind
                 :char   "4"
                 :prompt "stack-remind"
                 :dyn-target (lambda (target msg)
                               (+mu4e--get-folder '+mu4e-remind-folder msg))
                 :action (lambda (docid msg target)
                           (mu4e--server-move docid (mu4e--mark-check-target target) "-N"))))
  (add-to-list 'mu4e-marks
               '(stack-review
                 :char   "5"
                 :prompt "stack-review"
                 :dyn-target (lambda (target msg)
                               (+mu4e--get-folder '+mu4e-review-folder msg))
                 :action (lambda (docid msg target)
                           (mu4e--server-move docid (mu4e--mark-check-target target) "-N"))))

  (mu4e~headers-defun-mark-for stack-reply)
  (mu4e~headers-defun-mark-for stack-do)
  (mu4e~headers-defun-mark-for stack-capture)
  (mu4e~headers-defun-mark-for stack-remind)
  (mu4e~headers-defun-mark-for stack-review)
  (mu4e--view-defun-mark-for stack-reply)
  (mu4e--view-defun-mark-for stack-do)
  (mu4e--view-defun-mark-for stack-capture)
  (mu4e--view-defun-mark-for stack-remind)
  (mu4e--view-defun-mark-for stack-review)

  (define-key mu4e-headers-mode-map "1" 'mu4e-headers-mark-for-stack-reply)
  (define-key mu4e-headers-mode-map "2" 'mu4e-headers-mark-for-stack-do)
  (define-key mu4e-headers-mode-map "3" 'mu4e-headers-mark-for-stack-capture)
  (define-key mu4e-headers-mode-map "4" 'mu4e-headers-mark-for-stack-remind)
  (define-key mu4e-headers-mode-map "5" 'mu4e-headers-mark-for-stack-review)
  (define-key mu4e-view-mode-map "1" 'mu4e-view-mark-for-stack-reply)
  (define-key mu4e-view-mode-map "2" 'mu4e-view-mark-for-stack-do)
  (define-key mu4e-view-mode-map "3" 'mu4e-view-mark-for-stack-capture)
  (define-key mu4e-view-mode-map "4" 'mu4e-view-mark-for-stack-remind)
  (define-key mu4e-view-mode-map "5" 'mu4e-view-mark-for-stack-review)

  ;; Store Org links to the query in mu4e-headers-mode, not a particular
  ;; message.
  (setq mu4e-org-link-query-in-headers-mode t))

;; Support links to mu4e messages from Org.
(use-package mu4e-org
  :after org
  :config (setq mu4e-org-link-query-in-headers-mode t))

;; Faces for individual mu4e columns.
(use-package mu4e-column-faces
  :ensure t
  :after mu4e
  :config (mu4e-column-faces-mode))

;; Org mode to send and reply to email in HTML.
(use-package org-msg
  :ensure t
  :after mu4e
  :config
  (setopt org-msg-options "html-postamble:nil H:5 num:nil ^:{} toc:nil author:nil email:nil \\n:t"
          org-msg-startup "hidestars indent inlineimages"
          org-msg-greeting-name-limit 3
          org-msg-default-alternatives '((new . (utf-8 html))
                                         (reply-to-text . (utf-8))
                                         (reply-to-html . (utf-8 html)))
          org-msg-convert-citation t)

  (defun +mu4e-kill-org-msg-window-h ()
    "Return window configuration to how it was prior to email composition."
    (when (bound-and-true-p org-msg-mode)
      (switch-to-buffer "*Org ASCII Export*")
      (kill-buffer-and-window)))
  (add-hook 'message-sent-hook #'+mu4e-kill-org-msg-window-h)

  ;; Copy CSS style from Doom Emacs.
  (defvar +mu4e-org-msg-accent-color "#eb5630"
    "Accent color to use in org-msg's generated CSS.
Must be set before org-msg is loaded to take effect.")
  (setopt org-msg-enforce-css
          (let* ((font-family '(font-family . "-apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, Oxygen, Ubuntu, Cantarell,\
        \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", Arial, sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\";"))
                 (monospace-font '(font-family . "SFMono-Regular, Menlo, Monaco, Consolas, \"Liberation Mono\", \"Courier New\", monospace;"))
                 (font-size '(font-size . "11pt"))
                 (font `(,font-family ,font-size))
                 (line-height '(line-height . "1.2"))
                 (theme-color +mu4e-org-msg-accent-color)
                 (bold '(font-weight . "bold"))
                 (color `(color . ,theme-color))
                 (table `((margin-top . "6px") (margin-bottom . "6px")
                          (border-left . "none") (border-right . "none")
                          (border-top . "2px solid #222222")
                          (border-bottom . "2px solid #222222")))
                 (ftl-number `(,color ,bold (text-align . "left")))
                 (inline-modes '(asl c c++ conf cpp csv diff ditaa emacs-lisp
                                     fundamental ini json makefile man org plantuml
                                     python sh xml))
                 (inline-src `((background-color . "rgba(27,31,35,.05)")
                               (border-radius . "3px")
                               (padding . ".2em .4em")
                               (font-size . "90%") ,monospace-font
                               (margin . 0)))
                 (code-src
                  (mapcar (lambda (mode)
                            `(code ,(intern (concat "src src-" (symbol-name mode)))
                                   ,inline-src))
                          inline-modes))
                 (base-quote '((padding-left . "5px") (margin-left . "10px")
                               (margin-top . "20px") (margin-bottom . "0")
                               (font-style . "italic") (background . "#f9f9f9")))
                 (quote-palette '("#6A8FBF" "#bf8f6a" "#6abf8a" "#906abf"
                                  "#6aaebf" "#bf736a" "#bfb66a" "#bf6a94"
                                  "#6abf9b" "#bf6a7d" "#acbf6a" "#6a74bf"))
                 (quotes
                  (mapcar (lambda (x)
                            (let ((c (nth x quote-palette)))
                              `(div ,(intern (format "quote%d" (1+ x)))
                                    (,@base-quote
                                     (color . ,c)
                                     (border-left . ,(concat "3px solid "
                                                             (org-msg-lighten c)))))))
                          (number-sequence 0 (1- (length quote-palette))))))
            `((del nil ((color . "grey") (border-left . "none")
                        (text-decoration . "line-through") (margin-bottom . "0px")
                        (margin-top . "10px") (line-height . "11pt")))
              (a nil (,color))
              (a reply-header ((color . "black") (text-decoration . "none")))
              (div reply-header ((padding . "3.0pt 0in 0in 0in")
                                 (border-top . "solid #e1e1e1 1.0pt")
                                 (margin-bottom . "20px")))
              (span underline ((text-decoration . "underline")))
              (li nil (,line-height (margin-bottom . "0px")
                                    (margin-top . "2px")
                                    (max-width . "47em")))
              (nil org-ul ((list-style-type . "disc")))
              (nil org-ol (,@font ,line-height (margin-bottom . "0px")
                                  (margin-top . "0px") (margin-left . "30px")
                                  (padding-top . "0px") (padding-left . "5px")))
              (nil signature (,@font (margin-bottom . "20px")))
              (blockquote nil ((padding . "2px 12px") (margin-left . "10px")
                               (margin-top . "10px") (margin-bottom . "0")
                               (border-left . "3px solid #ccc")
                               (font-style . "italic")
                               (background . "#f9f9f9")))
              (p blockquote  ((margin . "0") (padding . "4px 0")))
              ,@quotes
              (code nil (,font-size ,monospace-font (background . "#f9f9f9")))
              ,@code-src
              (nil linenr ((padding-right . "1em")
                           (color . "black")
                           (background-color . "#aaaaaa")))
              (pre nil ((line-height . "1.2")
                        (color . ,(face-foreground 'default))
                        (background-color . ,(face-background 'default))
                        (margin . "4px 0px 8px 0px")
                        (padding . "8px 12px")
                        (width . "max-content")
                        (min-width . "50em")
                        (border-radius . "5px")
                        (font-size . "0.9em")
                        (font-weight . "500")
                        ,monospace-font))
              (div org-src-container ((margin-top . "10px")))
              (nil figure-number ,ftl-number)
              (nil table-number)
              (caption nil ((text-align . "left")
                            (background . ,theme-color)
                            (color . "white")
                            ,bold))
              (nil t-above ((caption-side . "top")))
              (nil t-bottom ((caption-side . "bottom")))
              (nil listing-number ,ftl-number)
              (nil figure ,ftl-number)
              (nil org-src-name ,ftl-number)
              (img nil ((vertical-align . "middle")
                        (max-width . "100%")))
              (img latex-fragment-inline ((margin . "0 0.1em")))
              (table nil (,@table ,line-height (border-collapse . "collapse")))
              (th nil ((border . "none") (border-bottom . "1px solid #222222")
                       (background-color . "#EDEDED") (font-weight . "500")
                       (padding . "3px 10px")))
              (td nil (,@table (padding . "1px 10px")
                               (background-color . "#f9f9f9") (border . "none")))
              (td org-left ((text-align . "left")))
              (td org-right ((text-align . "right")))
              (td org-center ((text-align . "center")))
              (kbd nil ((border . "1px solid #d1d5da") (border-radius . "3px")
                        (box-shadow . "inset 0 -1px 0 #d1d5da")
                        (background-color . "#fafbfc") (color . "#444d56")
                        (font-size . "0.85em")
                        (padding . "1px 4px") (display . "inline-block")))
              (div outline-text-4 ((margin-left . "15px")))
              (div outline-4 ((margin-left . "10px")))
              (h4 nil ((margin-bottom . "0px") (font-size . "11pt")))
              (h3 nil ((margin-bottom . "0px")
                       ,color (font-size . "14pt")))
              (h2 nil ((margin-top . "20px") (margin-bottom . "20px")
                       ,color (font-size . "18pt")))
              (h1 nil ((margin-top . "20px") (margin-bottom . "0px")
                       ,color (font-size . "24pt")))
              (p nil ((text-decoration . "none") (line-height . "1.4")
                      (margin-top . "10px") (margin-bottom . "0px")
                      ,font-size (max-width . "50em")))
              (b nil ((font-weight . "500") (color . ,theme-color)))
              (div nil (,@font (line-height . "12pt"))))))

  (org-msg-mode +1))

(provide '+mu4e)
