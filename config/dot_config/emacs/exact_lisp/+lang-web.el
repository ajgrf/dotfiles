;;; +lang-web.el -*- lexical-binding: t; -*-

;; Major mode for editing web templates.
(use-package web-mode
  :ensure t
  :mode ("\\.ejs\\'")
  :init
  (setopt web-mode-code-indent-offset 2
          web-mode-markup-indent-offset 2))

;; Emmet provides dynamic snippets for HTML and CSS files.
(use-package emmet-mode
  :ensure t
  :hook (sgml-mode web-mode css-mode)
  :init
  (setopt emmet-indentation 2
          emmet-move-cursor-between-quotes t
          emmet-preview-default nil))

;; Major mode to edit CSS files.
(use-package css-mode
  :defer t
  :init
  (setq css-fontify-colors nil))

;; Preview any color in your buffer in real time.
(use-package colorful-mode
  :ensure t
  :hook (css-mode css-ts-mode less-css-mode scss-mode))

(provide '+lang-web)
