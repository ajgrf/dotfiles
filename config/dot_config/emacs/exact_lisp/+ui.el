;;; +ui.el -*- lexical-binding: t; -*-

;; TODO: add breadcrumb package
;; BUG: ugly margins on android emacs

(defvar +ui-light-theme 'modus-operandi-tinted
  "Default light theme to use.")

(defvar +ui-dark-theme 'modus-vivendi-tinted
  "Default dark theme to use.")

(defvar +ui-terminal-theme 'modus-vivendi
  "Fallback theme to use on terminals with limited color support.")

(defvar +ui-unicode-font (font-spec :family "Source Han Mono")
  "Fallback font for Unicode glyphs.")

(defvar +ui-use-unicode
  (cond (IS-TERMUX nil)
        ((and IS-ANDROID
              (not (file-exists-p "~/fonts")))
         nil)
        (t))
  "Whether to use unicode characters for various interface elements.")

(defvar +ui-after-enable-theme-hook nil
  "Normal hook run after enabling a theme.")

(defun +ui-run-after-enable-theme-hook-a (&rest _args)
  "Run `+ui-after-enable-theme-hook'."
  (run-hooks '+ui-after-enable-theme-hook))
(advice-add 'enable-theme :after #'+ui-run-after-enable-theme-hook-a)

;; Tools for declaring and initializing options.
(use-package custom
  :defer t
  :init
  ;; Declare all themes as safe, rather than prompting the user when
  ;; loading new themes.
  (setopt custom-safe-themes t))

;; Set font configurations using presets.
(use-package fontaine
  :ensure t
  :unless (and IS-ANDROID
               (not (file-exists-p "~/fonts")))
  :hook (on-init-ui . +ui-load-fonts-h)
  :hook (kill-emacs . fontaine-store-latest-preset)
  :config
  (setopt fontaine-presets
          '((regular :default-height 120)
            (large   :default-height 160)
            (fira    :default-family "Fira Mono")
            (go      :default-family "Go Mono")
            (t       :default-family "Iosevka Flare"
                     :default-height 120
                     :fixed-pitch-serif-family "Go Mono"
                     :variable-pitch-family "Source Sans 3"
                     :variable-pitch-height 1.25)))

  (defun +ui-load-fonts-h ()
    "Load font settings."
    (interactive)
    (when (display-graphic-p)
      (fontaine-set-preset (or (fontaine-restore-latest-preset)
                               'regular))
      (when (fboundp 'set-fontset-font)
        (set-fontset-font t 'unicode +ui-unicode-font)))))

;; Display emojis in Emacs.
(use-package emojify
  :ensure t
  :hook (on-init-ui . +ui-enable-emojify-h)
  :config
  (defun +ui-enable-emojify-h ()
    (when (display-graphic-p)
      (global-emojify-mode +1)))

  (setopt emojify-emoji-styles '(unicode)))

;; Use a variable pitch, keeping fixed pitch where it's sensible.
(use-package mixed-pitch
  :ensure t
  :unless IS-WINDOWS
  :hook (( org-mode markdown-mode TeX-mode rst-mode
           mu4e-compose-mode message-mode) . mixed-pitch-mode)
  :config
  (setopt mixed-pitch-set-height t
          mixed-pitch-variable-pitch-cursor nil))

(defun +ui/toggle-theme (&optional variant)
  "Toggle between light and dark themes."
  (interactive)
  (cond ((or (eq variant 'dark)
             (memq +ui-light-theme custom-enabled-themes))
         (mapc #'disable-theme custom-enabled-themes)
         (load-theme +ui-dark-theme :no-confirm))
        ((or (eq variant 'light)
             (memq +ui-dark-theme custom-enabled-themes))
         (mapc #'disable-theme custom-enabled-themes)
         (load-theme +ui-light-theme :no-confirm))
        (t
         (call-interactively #'toggle-theme))))

;; Seamless integration with Darkman.
(use-package darkman
  :ensure t
  :hook (on-init-ui . +ui-enable-darkman-h)
  :config
  (defun +ui-enable-darkman-h ()
    (when (and (display-graphic-p)
               (executable-find "darkman"))
      (darkman-mode +1)))

  (setopt darkman-themes `( :light ,+ui-light-theme
                            :dark  ,+ui-dark-theme)))

;; Set the theme based on the time of day.
(use-package circadian
  :ensure t
  :hook (on-init-ui . +ui-enable-circadian-h)
  :init
  ;; Geolocation used for theme-switching.
  (if-let ((latitude  (getenv "LATITUDE"))
           (longitude (getenv "LONGITUDE")))
      (setopt calendar-latitude  (string-to-number latitude)
              calendar-longitude (string-to-number longitude)))
  :config
  (defun +ui-enable-circadian-h ()
    (unless (and (display-graphic-p)
                 (executable-find "darkman"))
      (circadian-setup)))

  (let* ((location-set? (and calendar-latitude calendar-longitude t))
         (day   (if location-set? :sunrise "7:30"))
         (night (if location-set? :sunset "19:30")))
    (setopt circadian-themes
            (cond ((display-graphic-p)
                   (list (cons day   +ui-light-theme)
                         (cons night +ui-dark-theme)))
                  ;; Whether the display supports 24-bit true color.
                  ((= (display-color-cells) 16777216)
                   (list (cons "00:00" +ui-dark-theme)))
                  (t
                   (list (cons "00:00" +ui-terminal-theme)))))))

;; Elegant, highly legible and customizable themes.
(use-package modus-themes
  :ensure t
  :init
  (setopt modus-themes-italic-constructs t
          modus-themes-headings '((0 . ((height . 1.5)))
                                  (1 . ((height . 1.2)))
                                  (t . (regular)))
          modus-operandi-palette-overrides
          '((bg-mode-line-active     bg-blue-subtle)
            (border-mode-line-active blue))
          modus-operandi-tinted-palette-overrides
          '((bg-hl-line bg-dim)
            (comment yellow-faint)
            (cursor fg-main))
          modus-operandi-deuteranopia-palette-overrides modus-operandi-palette-overrides
          modus-operandi-tritanopia-palette-overrides modus-operandi-palette-overrides
          modus-vivendi-palette-overrides
          '((bg-mode-line-active     bg-blue-subtle)
            (border-mode-line-active bg-mode-line-active))
          modus-vivendi-tinted-palette-overrides modus-vivendi-palette-overrides
          modus-vivendi-deuteranopia-palette-overrides modus-vivendi-palette-overrides
          modus-vivendi-tritanopia-palette-overrides modus-vivendi-palette-overrides
          modus-themes-common-palette-overrides
          '((string                green-cooler)
            (date-scheduled        yellow-faint)
            (keybind               blue-cooler)
            (bg-prompt             bg-cyan-nuanced)
            (fg-region             unspecified)
            (bg-completion         bg-active)
            (bg-completion-match-0 bg-magenta-subtle)
            (bg-completion-match-1 bg-blue-subtle)
            (bg-completion-match-2 bg-green-subtle)
            (bg-completion-match-3 bg-yellow-subtle)
            (fg-completion-match-0 fg-main)
            (fg-completion-match-1 fg-main)
            (fg-completion-match-2 fg-main)
            (fg-completion-match-3 fg-main)
            (fg-heading-0          fg-main)
            (fg-heading-1          fg-main)
            (fg-heading-2          magenta-warmer)
            (fg-heading-3          blue)
            (fg-heading-4          cyan)
            (fg-heading-5          green-cooler)
            (fg-heading-6          yellow-cooler)
            (fg-heading-7          red-warmer)
            (fg-heading-8          magenta))))

(defun +ui-custom-faces-h ()
  "Apply custom faces after loading a theme."
  (set-face-attribute 'region nil :extend nil)
  (custom-set-faces
   '(dirvish-hl-line ((t :inherit hl-line :extend t)))))
(add-hook '+ui-after-enable-theme-hook #'+ui-custom-faces-h)

(defun +ui-export-terminal-colors ()
  "Export a terminal emulator color scheme based on the current Emacs theme."
  (require 'ansi-color)
  (let ((colors '("black" "red" "green" "yellow" "blue" "magenta" "cyan" "white"
                  "bright-black" "bright-red" "bright-green" "bright-yellow"
                  "bright-blue" "bright-magenta" "bright-cyan" "bright-white")))
    (concat
     (format "background = \"%s\"\n" (face-attribute 'default :background))
     (format "foreground = \"%s\"\n" (face-attribute 'default :foreground))
     (format "cursor = \"%s\"\n" (face-attribute 'cursor :background))
     (mapconcat (lambda (color)
                  (format "%s = \"%s\""
                          (string-replace "-" "_" color)
                          (face-attribute (intern (concat "ansi-color-" color))
                                          :foreground)))
                colors
                "\n"))))

;; Colorful and legible themes.
(use-package ef-themes
  :ensure t
  :defer t
  :init
  (setopt ef-themes-headings '((0 . ((height . 1.5)))
                               (1 . ((height . 1.2)))
                               (t . (regular))))

  (defun +ui-load-random-theme ()
    "Wrap `ef-themes-load-random' to automatically restrict variants
based on the current theme."
    (let* ((current-theme (car custom-enabled-themes))
           (variant (cond ((eq current-theme +ui-light-theme)
                           'light)
                          ((eq current-theme +ui-dark-theme)
                           'dark))))
      (ef-themes-load-random variant))))

;; Like the default theme but more consistent.
(use-package standard-themes
  :ensure t
  :defer t)

(use-package emacs
  :config
  ;; Don't show the normal Emacs welcome screen or related messages.
  (setopt inhibit-startup-message t)
  (advice-add #'display-startup-echo-area-message :override #'ignore)

  ;; Empty the scratch buffer and remove `lisp-interaction-mode' from it.
  (setopt initial-scratch-message nil
          initial-major-mode 'fundamental-mode)

  ;; Set the window title format.
  (setq frame-title-format "%b - GNU Emacs"
        icon-title-format frame-title-format)

  ;; Don't resize frames and windows by even character widths.
  (setopt frame-resize-pixelwise t
          window-resize-pixelwise t)

  ;; Scroll one line at a time.
  (setopt scroll-conservatively 101)

  ;; Move point to top/bottom of buffer if no more scrolling is possible.
  (setopt scroll-error-top-bottom t)

  ;; Middle-click pastes at point, not at click.
  (setopt mouse-yank-at-point t)

  ;; Disable that infernal beep!
  (setopt ring-bell-function #'ignore
          visible-bell nil)

  ;; Don't blink the cursor.
  (setopt visible-cursor nil)
  (blink-cursor-mode -1)

  ;; Disable native GUI dialogs.
  (setopt use-dialog-box nil)

  ;; Disable the on-screen keyboard input method.
  (when IS-ANDROID
    (setopt overriding-text-conversion-style nil))

  ;; Prompt for `y' or `n' instead of `yes' or `no'.
  (advice-add #'yes-or-no-p :override #'y-or-n-p)

  ;; Ask for confirmation before quitting Emacs.
  (setopt confirm-kill-emacs #'y-or-n-p))

;; Setting up the tool bar.
(use-package tool-bar
  :if IS-ANDROID
  :custom (tool-bar-position 'bottom)
  :config (modifier-bar-mode +1))

;; Touch screen support for X and Android.
(use-package touch-screen
  :if IS-ANDROID
  :custom
  (touch-screen-display-keyboard t)
  :config
  (defun +ui-display-on-screen-keyboard ()
    "Display the on-screen keyboard."
    (interactive)
    (frame-toggle-on-screen-keyboard (selected-frame) nil))
  (add-hook 'window-setup-hook #'+ui-display-on-screen-keyboard))

;; Terminal mode Emacs improvements.
(use-package term/xterm
  :defer t
  :init
  ;; Update terminal window title.
  (setopt xterm-set-window-title t)

  ;; Enable the mouse in terminal Emacs
  (add-hook 'tty-setup-hook #'xterm-mouse-mode))

;; Sidebar showing a "mini-map" of a buffer.
(use-package minimap
  :ensure t
  :defer t
  :config
  (setopt minimap-window-location 'right
          minimap-width-fraction 0.09
          minimap-minimum-width 15
          minimap-update-delay 0.1))

;; Fill-column for visual-line-mode.
(use-package visual-fill-column
  :ensure t
  :hook ((org-mode markdown-mode nov-mode mu4e-compose-mode message-mode)
         . visual-fill-column-mode)
  :init (setopt visual-fill-column-center-text t))

;; Enable line numbers.
(use-package display-line-numbers
  :hook (( prog-mode ledger-mode TeX-mode rst-mode conf-mode
           git-timemachine-mode) . display-line-numbers-mode)
  :config (setq-default display-line-numbers-width 3))

;; Highlight the current line.
(use-package hl-line
  :hook (( prog-mode text-mode conf-mode special-mode org-agenda-mode
           dired-mode emms-playlist-mode) . hl-line-mode)
  :config
  (defun +ui-disable-hl-line-in-visual-line-mode-h ()
    "Turn off hl-line in `visual-line-mode'."
    (if visual-line-mode
        (hl-line-mode -1)
      (hl-line-mode 1)))
  (add-hook 'visual-line-mode-hook #'+ui-disable-hl-line-in-visual-line-mode-h)

  ;; Temporarily disable `hl-line' when selection is active, since it doesn't
  ;; serve much purpose when the selection is so much more visible.
  (defvar +ui--hl-line-mode nil)

  (defun +ui-truly-disable-hl-line-h ()
    (unless hl-line-mode
      (setq-local +ui--hl-line-mode nil)))
  (add-hook 'hl-line-mode-hook #'+ui-truly-disable-hl-line-h)

  (defun +ui-disable-hl-line-h ()
    (when hl-line-mode
      (hl-line-mode -1)
      (setq-local +ui--hl-line-mode t)))
  (add-hook 'activate-mark-hook #'+ui-disable-hl-line-h)

  (defun +ui-enable-hl-line-maybe-h ()
    (when +ui--hl-line-mode
      (hl-line-mode +1)))
  (add-hook 'deactivate-mark-hook #'+ui-enable-hl-line-maybe-h))

;; Use more prominent line highlighting in some modes.
(use-package lin
  :ensure t
  :hook (on-first-buffer . lin-global-mode)
  ;; Work around typo in lin 0.4.0
  :config
  (add-to-list 'lin-mode-hooks 'emms-playlist-mode-hook)
  (add-to-list 'lin-mode-hooks 'mu4e-headers-mode-hook))

;; Pulse highlight on demand or after select functions.
(use-package pulsar
  :ensure t
  :hook (on-first-input . pulsar-global-mode)
  :config
  (setopt pulsar-pulse-region-functions pulsar-pulse-region-common-functions)
  (add-to-list 'pulsar-pulse-functions 'tab-previous))

;; Increase the padding/spacing of frames and windows.
(use-package spacious-padding
  :ensure t
  :if EMACS30+
  :config
  (setopt spacious-padding-widths
          '( :internal-border-width 25
             :header-line-width 4
             :mode-line-width 1
             :tab-width 4
             :right-divider-width 25
             :scroll-bar-width 8))

  (spacious-padding-mode +1))

;; Disambiguate buffer names by adding its file path to the name when needed.
(use-package uniquify
  :config (setopt uniquify-buffer-name-style 'forward))

;; Don't open new frames for ediff.
(use-package ediff
  :defer t
  :init (setopt ediff-window-setup-function #'ediff-setup-windows-plain))

;; A minimal and modern mode-line.
(use-package doom-modeline
  :ensure t
  :commands doom-modeline-mode
  :hook (doom-modeline-mode . column-number-mode)   ; cursor column in modeline
  :init
  ;; Pad the mode-line a bit.
  (setopt doom-modeline-height 30)
  ;; Don't show modified buffer names in red.
  (setopt doom-modeline-highlight-modified-buffer-name nil)
  ;; Don't use icons if unicode is disabled.
  (unless +ui-use-unicode
    (setopt doom-modeline-icon nil))
  ;; Display minor modes, most of which will be hidden by minions.
  (setopt doom-modeline-minor-modes t)
  ;; Only show file encoding if it's non-UTF-8 and different line endings
  ;; than the current OSes preference
  (setopt doom-modeline-buffer-encoding 'nondefault
          doom-modeline-default-eol-type (cond (IS-MAC 2)
                                               (IS-WINDOWS 1)
                                               (0)))
  ;; Must be enabled after setting fonts to ensure the correct height
  ;; is used on HiDPI displays.
  (add-hook 'after-init-hook #'doom-modeline-mode 90)
  :config
  ;; doom-modeline modifies these faces when Emacs loses focus, causing
  ;; problems with faces that inherit from them.
  (dolist (face '(success warning error))
    (delete face doom-modeline--remap-faces))

  ;; Scale mode-line height. This undoes upstream
  ;; commit 68689f4c5e46e6ac3d4669966d506b8b5609cd72.
  (defun +ui-doom-modeline--create-bar-image (face width height)
    "Create the bar image.

Use FACE for the bar, WIDTH and HEIGHT are the image size in pixels."
    (when (and (image-type-available-p 'pbm)
               (numberp width) (> width 0)
               (numberp height) (> height 0))
      (propertize
       " " 'display
       (let ((color (or (face-background face nil t) "None")))
         (ignore-errors
           (create-image
            (concat (format "P1\n%i %i\n" width height)
                    (make-string (* width height) ?1)
                    "\n")
            'pbm t :foreground color :ascent 'center))))))
  (advice-add 'doom-modeline--create-bar-image :override #'+ui-doom-modeline--create-bar-image))

;; Hide enabled minor modes behind behind a menu in the mode line.
(use-package minions
  :ensure t
  :hook (doom-modeline-mode . minions-mode)
  :config
  (setopt minions-prominent-modes '(emms org-tree-slide-mode)
          minions-mode-line-lighter ";-"))

;; Display key bindings in a popup.
(use-package which-key
  :ensure t
  :hook (on-first-input . which-key-mode)
  :init (setopt which-key-dont-use-unicode (not +ui-use-unicode)
                which-key-popup-type 'minibuffer
                which-key-sort-order #'which-key-key-order-alpha
                which-key-sort-uppercase-first nil
                which-key-use-C-h-commands nil))

;; Transient commands.
(use-package transient
  :defer t
  :config
  ;; Don't split bottom side windows for transient.
  (setopt transient-display-buffer-action '(display-buffer-at-bottom)))

;; Show number of the current search match in the mode-line.
(use-package anzu
  :ensure t
  :after doom-modeline
  :config (global-anzu-mode +1))

;; Emacs' built-in help system.
(use-package help
  :bind (("C-c h" . help-map)
         :map help-map
         ("P" . find-library)
         ("t" . load-theme)
         ("C-c" . describe-key-briefly)
         ("C-e" . view-echo-area-messages)
         ("C-f" . describe-function)
         ("C-o" . describe-symbol)
         ("C-t" . load-theme))
  :config
  (defalias 'help-map help-map))

;; Apropos help commands search more extensively.
(use-package apropos
  :defer t
  :init (setopt apropos-do-all t))

;; Man page viewer in pure elisp.
(use-package woman
  :defer t
  :bind ( :map help-map
          ("W" . woman))
  :init
  (when (featurep 'native-compile)
    (add-to-list 'warning-suppress-types
                 '(defvaralias losing-value woman-topic-history))))

;; Emacs viewer for DevDocs.
(use-package devdocs
  :ensure t
  :bind ( :map help-map
          ("D" . devdocs-lookup)))

;; The GNU Emacs calculator.
(use-package calc
  :defer t
  :init
  (setq calc-group-digits t))

;; Transient user interfaces for various modes.
(use-package casual
  :ensure t
  :defer t)

;; Transient UI for Agenda.
(use-package casual-agenda
  :after org-agenda
  :bind ( :map org-agenda-mode-map
          ("C-o" . casual-agenda-tmenu)))

;; Transient UI for Bookmarks.
(use-package casual-bookmarks
  :after bookmark
  :bind ( :map bookmark-bmenu-mode-map
          ("C-o" . casual-bookmarks-tmenu)))

;; Transient UI for Calc.
(use-package casual-calc
  :after calc
  :bind ( :map calc-mode-map
          ("C-o" . casual-calc-tmenu)))

;; Transient UI for Calendar.
(use-package casual-calendar
  :after calendar
  :bind ( :map calendar-mode-map
          ("C-o" . casual-calendar)))

;; Transient UI for Dired.
(use-package casual-dired
  :after dired
  :bind ( :map dired-mode-map
          ("C-o" . casual-dired-tmenu)
          ("s"   . casual-dired-sort-by-tmenu)
          ("/"   . casual-dired-search-replace-tmenu)))

;; Transient user interface library for editing commands.
(use-package casual-editkit
  :bind ("C-o" . casual-editkit-main-tmenu))

;; Transient UI for IBuffer.
(use-package casual-ibuffer
  :after ibuffer
  :bind ( :map ibuffer-mode-map
          ("C-o" . casual-ibuffer-tmenu)
          ("F"   . casual-ibuffer-filter-tmenu)
          ("s"   . casual-ibuffer-sortby-tmenu)))

;; Transient UI for Info.
(use-package casual-info
  :after info
  :bind ( :map Info-mode-map
          ("C-o" . casual-info-tmenu)))

;; Transient UI for I-Search.
(use-package casual-isearch
  :after isearch
  :bind ( :map isearch-mode-map
          ("C-o" . casual-isearch-tmenu)))

;; Transient UI for RE-Builder.
(use-package casual-re-builder
  :after re-builder
  :bind ( :map reb-mode-map
          ("C-o" . casual-re-builder-tmenu)
          :map reb-lisp-mode-map
          ("C-o" . casual-re-builder-tmenu)))

;; Highlight TODO and similar keywords.
(use-package hl-todo
  :ensure t
  :hook (prog-mode . hl-todo-mode)
  :hook (yaml-mode . hl-todo-mode)
  :config
  (setopt hl-todo-highlight-punctuation ":"
          hl-todo-keyword-faces
          '(;; For reminders to change or add something at a later date.
            ("TODO" warning bold)
            ;; For code (or code paths) that are broken, unimplemented, or slow,
            ;; and may become bigger problems later.
            ("FIXME" error bold)
            ;; For code that needs to be revisited later, either to upstream it,
            ;; improve it, or address non-critical issues.
            ("REVIEW" font-lock-keyword-face bold)
            ;; For code smells where questionable practices are used
            ;; intentionally, and/or is likely to break in a future update.
            ("HACK" font-lock-constant-face bold)
            ;; For sections of code that just gotta go, and will be gone soon.
            ;; Specifically, this means the code is deprecated, not necessarily
            ;; the feature it enables.
            ("DEPRECATED" font-lock-doc-face bold)
            ;; Extra keywords commonly found in the wild, whose meaning may vary
            ;; from project to project.
            ("NOTE" success bold)
            ("BUG" error bold)
            ("XXX" font-lock-constant-face bold))))

(provide '+ui)
