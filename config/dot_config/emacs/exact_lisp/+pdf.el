;;; +pdf.el -*- lexical-binding: t; -*-

;; Improved support for viewing and working with PDF documents.
(use-package pdf-tools
  :ensure t
  :mode ("\\.pdf\\'" . pdf-view-mode)
  :magic ("%PDF" . pdf-view-mode)
  :hook ((pdf-view-mode . pdf-view-auto-slice-minor-mode)
         (pdf-view-mode . pdf-view-themed-minor-mode))
  :config
  ;; Enable hiDPI support.
  (setopt pdf-view-use-scaling t
          pdf-view-use-imagemagick nil
          pdf-view-midnight-invert nil)

  ;; workaround for pdf-tools not reopening to last-viewed page of the pdf:
  ;; https://github.com/politza/pdf-tools/issues/18#issuecomment-269515117
  (defun +pdf-set-last-viewed-bookmark ()
    (interactive)
    (when (eq major-mode 'pdf-view-mode)
      (bookmark-set (+pdf-generate-bookmark-name))))

  (defun +pdf-jump-last-viewed-bookmark ()
    (bookmark-set "fake") ; this is new
    (when
        (+pdf-has-last-viewed-bookmark)
      (bookmark-jump (+pdf-generate-bookmark-name))))

  (defun +pdf-has-last-viewed-bookmark ()
    (assoc
     (+pdf-generate-bookmark-name) bookmark-alist))

  (defun +pdf-generate-bookmark-name ()
    (concat "PDF-LAST-VIEWED: " (buffer-file-name)))

  (defun +pdf-set-all-last-viewed-bookmarks ()
    (dolist (buf (buffer-list))
      (with-current-buffer buf
        (+pdf-set-last-viewed-bookmark))))

  (add-hook 'kill-buffer-hook '+pdf-set-last-viewed-bookmark)
  (add-hook 'pdf-view-mode-hook '+pdf-jump-last-viewed-bookmark)
  (unless noninteractive  ; as `save-place-mode' does
    (add-hook 'kill-emacs-hook #'+pdf-set-all-last-viewed-bookmarks))

  (pdf-tools-install))

(provide '+pdf)
