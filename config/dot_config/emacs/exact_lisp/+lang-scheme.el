;;; +lang-scheme.el -*- lexical-binding: t; -*-

;; GNU Emacs and Scheme talk to each other.
(use-package geiser
  :ensure t
  :defer t
  :init (setopt geiser-default-implementation 'guile))

;; Major mode for editing Scheme code.
(use-package scheme-mode
  :mode "\\.guile\\'"
  :init
  ;; Taken from Guix's .dir-locals.el
  (defun +lang-scheme-guix-indent-rules-h ()
    (make-local-variable 'scheme-indent-function)
    (put 'eval-when 'scheme-indent-function 1)
    (put 'call-with-prompt 'scheme-indent-function 1)
    (put 'test-assert 'scheme-indent-function 1)
    (put 'test-assertm 'scheme-indent-function 1)
    (put 'test-equalm 'scheme-indent-function 1)
    (put 'test-equal 'scheme-indent-function 1)
    (put 'test-eq 'scheme-indent-function 1)
    (put 'call-with-input-string 'scheme-indent-function 1)
    (put 'call-with-port 'scheme-indent-function 1)
    (put 'guard 'scheme-indent-function 1)
    (put 'lambda* 'scheme-indent-function 1)
    (put 'substitute* 'scheme-indent-function 1)
    (put 'match-record 'scheme-indent-function 3)
    (put 'match-record-lambda 'scheme-indent-function 2)

    (put 'let-keywords 'scheme-indent-function 3)

    ;; 'modify-inputs' and its keywords.
    (put 'modify-inputs 'scheme-indent-function 1)
    (put 'replace 'scheme-indent-function 1)

    ;; 'modify-phases' and its keywords.
    (put 'modify-phases 'scheme-indent-function 1)
    (put 'replace 'scheme-indent-function 1)
    (put 'add-before 'scheme-indent-function 2)
    (put 'add-after 'scheme-indent-function 2)

    (put 'modify-services 'scheme-indent-function 1)
    (put 'with-directory-excursion 'scheme-indent-function 1)
    (put 'with-file-lock 'scheme-indent-function 1)
    (put 'with-file-lock/no-wait 'scheme-indent-function 1)
    (put 'with-profile-lock 'scheme-indent-function 1)
    (put 'with-writable-file 'scheme-indent-function 2)

    (put 'package 'scheme-indent-function 0)
    (put 'package/inherit 'scheme-indent-function 1)
    (put 'origin 'scheme-indent-function 0)
    (put 'build-system 'scheme-indent-function 0)
    (put 'bag 'scheme-indent-function 0)
    (put 'graft 'scheme-indent-function 0)
    (put 'operating-system 'scheme-indent-function 0)
    (put 'file-system 'scheme-indent-function 0)
    (put 'manifest-entry 'scheme-indent-function 0)
    (put 'manifest-pattern 'scheme-indent-function 0)
    (put 'substitute-keyword-arguments 'scheme-indent-function 1)
    (put 'with-store 'scheme-indent-function 1)
    (put 'with-external-store 'scheme-indent-function 1)
    (put 'with-error-handling 'scheme-indent-function 0)
    (put 'with-mutex 'scheme-indent-function 1)
    (put 'with-atomic-file-output 'scheme-indent-function 1)
    (put 'call-with-compressed-output-port 'scheme-indent-function 2)
    (put 'call-with-decompressed-port 'scheme-indent-function 2)
    (put 'call-with-gzip-input-port 'scheme-indent-function 1)
    (put 'call-with-gzip-output-port 'scheme-indent-function 1)
    (put 'call-with-lzip-input-port 'scheme-indent-function 1)
    (put 'call-with-lzip-output-port 'scheme-indent-function 1)
    (put 'signature-case 'scheme-indent-function 1)
    (put 'emacs-batch-eval 'scheme-indent-function 0)
    (put 'emacs-batch-edit-file 'scheme-indent-function 1)
    (put 'emacs-substitute-sexps 'scheme-indent-function 1)
    (put 'emacs-substitute-variables 'scheme-indent-function 1)
    (put 'with-derivation-narinfo 'scheme-indent-function 1)
    (put 'with-derivation-substitute 'scheme-indent-function 2)
    (put 'with-status-report 'scheme-indent-function 1)
    (put 'with-status-verbosity 'scheme-indent-function 1)
    (put 'with-build-handler 'scheme-indent-function 1)

    (put 'mlambda 'scheme-indent-function 1)
    (put 'mlambdaq 'scheme-indent-function 1)
    (put 'syntax-parameterize 'scheme-indent-function 1)
    (put 'with-monad 'scheme-indent-function 1)
    (put 'mbegin 'scheme-indent-function 1)
    (put 'mwhen 'scheme-indent-function 1)
    (put 'munless 'scheme-indent-function 1)
    (put 'mlet* 'scheme-indent-function 2)
    (put 'mlet 'scheme-indent-function 2)
    (put 'mparameterize 'scheme-indent-function 2)
    (put 'run-with-store 'scheme-indent-function 1)
    (put 'run-with-state 'scheme-indent-function 1)
    (put 'wrap-program 'scheme-indent-function 1)
    (put 'wrap-script 'scheme-indent-function 1)
    (put 'with-imported-modules 'scheme-indent-function 1)
    (put 'with-extensions 'scheme-indent-function 1)
    (put 'with-parameters 'scheme-indent-function 1)
    (put 'let-system 'scheme-indent-function 1)
    (put 'with-build-variables 'scheme-indent-function 2)

    (put 'with-database 'scheme-indent-function 2)
    (put 'call-with-database 'scheme-indent-function 1)
    (put 'call-with-transaction 'scheme-indent-function 1)
    (put 'call-with-retrying-transaction 'scheme-indent-function 1)

    (put 'call-with-container 'scheme-indent-function 1)
    (put 'container-excursion 'scheme-indent-function 1)
    (put 'eventually 'scheme-indent-function 1)

    (put 'call-with-progress-reporter 'scheme-indent-function 1)
    (put 'with-repository 'scheme-indent-function 2)
    (put 'with-temporary-git-repository 'scheme-indent-function 2)
    (put 'with-environment-variables 'scheme-indent-function 1)
    (put 'with-fresh-gnupg-setup 'scheme-indent-function 1)

    (put 'with-paginated-output-port 'scheme-indent-function 1)

    (put 'with-shepherd-action 'scheme-indent-function 3)

    (put 'with-http-server 'scheme-indent-function 1)

    ;; This notably allows '(' in Paredit to not insert a space when the
    ;; preceding symbol is one of these.
    (modify-syntax-entry ?~ "'")
    (modify-syntax-entry ?$ "'")
    (modify-syntax-entry ?+ "'"))
  (add-hook 'scheme-mode-hook #'+lang-scheme-guix-indent-rules-h))

(provide '+lang-scheme)
