;;; +youtube-dl.el -*- lexical-binding: t; -*-

(defvar +youtube-dl-command
  (if (executable-find "yt-dlp")
      "yt-dlp"
    "youtube-dl")
  "Command used to run youtube-dl.")

(defvar +youtube-dl-add-to-emms-playlist t
  "If non-nil, automatically append downloads to the current EMMS playlist.")

;; HACK: override `server-visit-files' to add a file to the playlist
;; It's too hard to properly quote arguments for use in `emacsclient --eval`.

(defun +youtube-dl--add-to-emms-playlist-a (_orig-fn files _proc &optional _nowait)
  (emms-add-file (caar files))
  (advice-remove 'server-visit-files '+youtube-dl--add-to-emms-playlist-a))

(defun +youtube-dl--add-next-server-file-to-emms-playlist ()
  (advice-add 'server-visit-files :around #'+youtube-dl--add-to-emms-playlist-a))

(defun +youtube-dl-run-in-popup (&optional args)
  "Run 'youtube-dl' with ARGS."
  (require '+run)
  (let* ((postcmd (concat "emacsclient --eval '"
                          "(+youtube-dl--add-next-server-file-to-emms-playlist)"
                          "'; emacsclient"))
         (args (if (and (featurep '+emms)
                        +youtube-dl-add-to-emms-playlist)
                   (append args (list "--exec" postcmd))
                 args)))
    (apply #'+run-in-popup +youtube-dl-command args)))

(use-package browse-url
  :commands browse-url-interactive-arg)

(defun +youtube-dl/download-url (url &optional _same-window)
  "Run 'youtube-dl' with ARGS.  If ARGS is nil, use URL at point."
  (interactive (browse-url-interactive-arg "Video URL: "))
  (+youtube-dl-run-in-popup (list url)))

(provide '+youtube-dl)
