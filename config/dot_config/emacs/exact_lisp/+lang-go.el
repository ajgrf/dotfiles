;;; +lang-go.el -*- lexical-binding: t; -*-

;; Major mode for the Go programming language.
(use-package go-mode
  :ensure t
  :defer t
  :config
  (defun +lang-go-set-tab-width-h ()
    (setq-local tab-width 4))
  (add-hook 'go-mode-hook #'+lang-go-set-tab-width-h)
  (add-hook 'go-ts-mode-hook #'+lang-go-set-tab-width-h))

(provide '+lang-go)
