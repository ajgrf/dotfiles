;;; +dired.el -*- lexical-binding: t; -*-

;; Open files and URLs with the appropriate program.
(use-package browse-url
  :defer t
  :init
  (cond (IS-WSL
         (setopt browse-url-generic-program  "wslview"
                 browse-url-generic-args     '()
                 browse-url-browser-function #'browse-url-generic))
        ((or IS-LINUX IS-BSD)
         (setopt browse-url-browser-function #'browse-url-xdg-open))))

(use-package tramp
  :defer t
  :config
  ;; Respect PATH on remote machines.
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path)

  ;; Enable full-featured Dirvish over TRAMP on certain connections
  ;; https://www.gnu.org/software/tramp/#Improving-performance-of-asynchronous-remote-processes-1
  (add-to-list 'tramp-connection-properties
               (list "/ssh:.*\\.emu-blues\\.ts\\.net:"
                     "direct-async-process" t))
  ;; Tips to speed up connections
  (setopt tramp-verbose 0)
  (setopt tramp-chunksize 2000)
  (setopt tramp-use-ssh-controlmaster-options nil))

;; Dirvish adds features that greatly improve on dired.
(use-package dirvish
  :ensure t
  :demand t
  :after dired
  :custom
  (dirvish-quick-access-entries
   `(("h" "~/"                                "Home")
     ,@(if IS-WSL '(("w" "/mnt/c/Users/Alex/" "Windows Home")))
     ("t" "~/Desktop/"                        "Desktop")
     ("s" "~/Projects/"                       "Projects")
     ("o" "~/Org/"                            "Org")
     ("d" "~/Projects/dotfiles/"              "dotfiles")
     ("m" "/run/media/axgfn/"                 "Removable Devices")
     ("g" "/ssh:griffincloud:~/"              "griffincloud")
     ("E" "~/Share/Egnyte/"                   "Egnyte")
     ("F" "~/Share/Egnyte/Shared/FOCUS OPERATIONS/3-Finance-Accounting/"
      "FOCUS Finances")
     ("C" "~/Share/Egnyte/Shared/FOCUS OPERATIONS/1-Receipts-Purchasing/IT-Computers and Software/Computers/Software Product Keys/"
      "FOCUS Computers")
     ("L" "~/Share/Egnyte/Shared/CLIENTS/Lake Elmo/Projects/"
      "Lake Elmo Projects")
     ("B" "~/Share/Egnyte/Shared/CLIENTS/Lake Elmo/1-Client Management/Budget Summary Reports/"
      "Lake Elmo Budgets")))
  :bind ( :map dirvish-mode-map
          ("a"   . dirvish-quick-access)
          ("f"   . dirvish-file-info-menu)
          ("y"   . dirvish-yank-menu)
          ("N"   . dirvish-narrow)
          ("h"   . dirvish-history-jump)
          ("s"   . dirvish-quicksort)
          ("v"   . dirvish-vc-menu)
          ("TAB" . dirvish-subtree-toggle)
          ("M-f" . dirvish-history-go-forward)
          ("M-b" . dirvish-history-go-backward)
          ("M-l" . dirvish-ls-switches-menu)
          ("M-m" . dirvish-mark-menu)
          ("M-t" . dirvish-layout-toggle)
          ("M-s" . dirvish-setup-menu)
          ("M-e" . dirvish-emerge-menu)
          ("M-j" . dirvish-fd-jump))
  :config
  (setopt dirvish-path-separators '("  ~" "  /" " / ")
          dirvish-side-window-parameters nil
          dirvish-use-header-line (not EMACS30+)
          dirvish-attributes
          '(nerd-icons file-size git-msg subtree-state vc-state)
          dirvish-header-line-format
          '(:left (path) :right (free-space))
          dirvish-mode-line-format
          (unless EMACS30+
            '(:left (sort file-time symlink) :right (omit yank index))))

  (unless +ui-use-unicode
    (setopt dirvish-attributes '(file-size git-msg subtree-state vc-state)
            dirvish-subtree-prefix " |"
            dirvish-subtree-state-style 'plus))

  ;; Enable dirvish.
  (dirvish-override-dired-mode)
  ;; Enable custom file grouping (defined in `.dir-locals.el' files).
  (add-hook 'dirvish-setup-hook #'dirvish-emerge-mode)
  ;; Make the sidebar follow the current buffer.
  (dirvish-side-follow-mode))

;; Configure dired, the Emacs file manager.
(use-package dired
  :defer t
  :hook (dired-mode . dired-omit-mode)
  :config
  (setopt dired-dwim-target t
          dired-recursive-copies 'always
          dired-recursive-deletes 'always
          dired-listing-switches
          (concat "--group-directories-first "
                  "--almost-all "
                  "--human-readable "
                  "--sort=version "
                  "--time-style=long-iso "
                  "-l")
          dired-omit-files "\\`[.]"
          dired-omit-verbose nil
          image-dired-external-viewer nil))

;; Manage the trash can from within Emacs.
(use-package trashed
  :ensure t
  :commands trashed)

(defun +dired-trash-local-files-a (fn &rest args)
  "Delete remote files normally, but send local files to the trash."
  (let ((delete-by-moving-to-trash (not (file-remote-p default-directory))))
    (apply fn args)))
(advice-add 'delete-file :around #'+dired-trash-local-files-a)
(advice-add 'delete-directory :around #'+dired-trash-local-files-a)
(advice-add 'dired-delete-file :around #'+dired-trash-local-files-a)
(advice-add 'dired-do-delete :around #'+dired-trash-local-files-a)
(advice-add 'dired-do-flagged-delete :around #'+dired-trash-local-files-a)

(provide '+dired)
