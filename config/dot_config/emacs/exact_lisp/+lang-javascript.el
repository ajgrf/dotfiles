;;; +lang-javascript.el -*- lexical-binding: t; -*-

;; Major mode for editing JavaScript.
(use-package js
  :defer t
  :init
  (when (treesit-available-p)
    (add-to-list 'major-mode-remap-alist '(js-mode . js-ts-mode))
    (add-to-list 'major-mode-remap-alist '(javascript-mode . js-ts-mode))
    (add-to-list 'treesit-language-source-alist
                 '(javascript "https://github.com/tree-sitter/tree-sitter-javascript.git")))
  :config
  (setopt js-indent-level 2))

(provide '+lang-javascript)
