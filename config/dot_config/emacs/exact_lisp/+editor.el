;;; +editor.el -*- lexical-binding: t; -*-

(use-package emacs
  :config
  ;; Enable the more intuitive linear undo behavior.
  ;; Use vundo to access the full undo tree.
  (setopt undo-no-redo t)

  ;; Indent with spaces unless specified otherwise.
  ;; Tabs are 4 columns wide by default.
  (setq-default indent-tabs-mode nil)

  ;; Define sentences as ending in only a single space.
  (setopt sentence-end-double-space nil)

  ;; Wrap lines at whitespace, but disable wrapping by default.
  (setq-default word-wrap t
                truncate-lines t)

  ;; Enable soft line-wrapping in text mode.
  (add-hook 'text-mode-hook #'turn-on-visual-line-mode)

  ;; Don't add duplicates to the kill ring.
  (setopt kill-do-not-save-duplicates t)

  ;; https://protesilaos.com/codelog/2024-11-28-basic-emacs-configuration/#h:1e468b2a-9bee-4571-8454-e3f5462d9321
  (defun +editor-keyboard-quit-dwim ()
    "Do-What-I-Mean behaviour for a general `keyboard-quit'.

The generic `keyboard-quit' does not do the expected thing when
the minibuffer is open.  Whereas we want it to close the
minibuffer, even without explicitly focusing it.

The DWIM behaviour of this command is as follows:

- When the region is active, disable it.
- When a minibuffer is open, but not focused, close the minibuffer.
- When the Completions buffer is selected, close it.
- In every other case use the regular `keyboard-quit'."
    (interactive)
    (cond
     ((region-active-p)
      (keyboard-quit))
     ((derived-mode-p 'completion-list-mode)
      (delete-completion-window))
     ((> (minibuffer-depth) 0)
      (abort-recursive-edit))
     (t
      (keyboard-quit))))

  (keymap-set global-map "C-g" #'+editor-keyboard-quit-dwim))

;; Delete selection if you insert.
(use-package delsel
  :hook (after-init . delete-selection-mode))

(use-package files
  :config
  ;; Copy instead of renaming current file (clobbers links).
  (setopt backup-by-copying t)

  ;; Follow symlinks without warning about it.
  (setopt find-file-visit-truename t
          find-file-suppress-same-file-warnings t
          vc-follow-symlinks t)

  ;; Automatically add a newline at the end of the file.
  (setopt require-final-newline t)

  ;; Automatically save modified buffers when idle and
  ;; `auto-save-visited-mode' is set locally.
  (auto-save-visited-mode 1)
  (setq-default auto-save-visited-mode nil))

;; Save minibuffer history.
(use-package savehist
  :hook (on-first-input . savehist-mode))

;; Save cursor position to resume editing files.
(use-package saveplace
  :hook (on-first-file . save-place-mode))

;; Save list of recently opened files.
(use-package recentf
  :hook (on-first-file . recentf-mode)
  :commands recentf-open-files
  :config
  (add-to-list 'recentf-exclude no-littering-etc-directory)
  (add-to-list 'recentf-exclude no-littering-var-directory)
  (recentf-mode 1))

;; Revert buffers when the file changes.
(use-package autorevert
  :defer 1
  :init
  (setopt global-auto-revert-non-file-buffers t)  ; also revert dired
  :config
  (global-auto-revert-mode 1))

;; Run something every midnight.
(use-package midnight
  :hook (after-init . midnight-mode))

;; Set timers using a convenient notation.
(use-package tmr
  :ensure t
  :defer t
  :config
  (setopt tmr-sound-file "~/Share/Mobile/Alarms/material/Carbon.ogg"))

;; Visual undo tree.
(use-package vundo
  :ensure t
  :defer t
  :config
  (setopt vundo-compact-display t))

;; Persistent undo, available between sessions.
(use-package undo-fu-session
  :ensure t
  :hook (on-first-file . undo-fu-session-global-mode)
  :config
  (setopt undo-fu-session-incompatible-files
          '("\\`/tmp/"
            "\\`/dev/shm/"
            "/COMMIT_EDITMSG\\'"
            "/git-rebase-todo\\'")))

;; Tree-sitter utilities.
(use-package treesit
  :defer t
  :init
  (setq treesit-language-source-alist nil))

;; Automatically use tree-sitter enhanced major modes.
(use-package treesit-auto
  :ensure t
  :if (treesit-available-p)
  :hook (on-first-buffer . global-treesit-auto-mode)
  :config
  (setopt treesit-auto-langs
          '( bash c c-sharp clojure cmake cpp css dockerfile go gomod java
             javascript json python ruby rust toml tsx typescript yaml)))

;; Insert parentheses and other delimiters in pairs.
(use-package smartparens
  :ensure t
  :hook (on-first-buffer . smartparens-global-mode)
  :hook (eval-expression-minibuffer-setup . smartparens-mode)
  :config
  (require 'smartparens-config)
  (setopt sp-highlight-pair-overlay nil
          sp-highlight-wrap-overlay nil
          sp-highlight-wrap-tag-overlay nil)

  (sp-with-modes '(nix-mode nix-ts-mode)
    (sp-local-pair " = " ";"))

  ;; You're likely writing lisp in the minibuffer, therefore, disable these
  ;; quote pairs, which lisps doesn't use for strings:
  (sp-with-modes '(minibuffer-mode minibuffer-inactive-mode)
    (sp-local-pair "'" nil :actions nil)
    (sp-local-pair "`" nil :actions nil))

  (defun +editor-setup-indent-adjust-sexp-h ()
    "Automatically adjust closing parens when indenting Lisp code."
    (keymap-local-set "TAB"       #'sp-indent-adjust-sexp)
    (keymap-local-set "<backtab>" #'sp-dedent-adjust-sexp))
  (add-hook 'lisp-mode-hook #'+editor-setup-indent-adjust-sexp-h)
  (add-hook 'lisp-data-mode-hook #'+editor-setup-indent-adjust-sexp-h)
  (add-hook 'emacs-lisp-mode-hook #'+editor-setup-indent-adjust-sexp-h)
  (add-hook 'ielm-mode-hook #'+editor-setup-indent-adjust-sexp-h)
  (add-hook 'scheme-mode-hook #'+editor-setup-indent-adjust-sexp-h)
  (add-hook 'racket-mode-hook #'+editor-setup-indent-adjust-sexp-h)
  (add-hook 'hy-mode-hook #'+editor-setup-indent-adjust-sexp-h)
  (add-hook 'lfe-mode-hook #'+editor-setup-indent-adjust-sexp-h)
  (add-hook 'dune-mode-hook #'+editor-setup-indent-adjust-sexp-h)
  (add-hook 'clojure-mode-hook #'+editor-setup-indent-adjust-sexp-h)
  (add-hook 'clojure-ts-mode-hook #'+editor-setup-indent-adjust-sexp-h)
  (add-hook 'fennel-mode-hook #'+editor-setup-indent-adjust-sexp-h)

  ;; Set up paredit-like bindings.
  (sp-use-paredit-bindings))

;; Add/Change/Delete pairs based on `expand-region'.
(use-package embrace
  :ensure t
  :hook ((org-mode . embrace-org-mode-hook)
         (tex-mode . embrace-LaTeX-mode-hook)
         (emacs-lisp-mode . embrace-emacs-lisp-mode-hook)
         ((ruby-mode enh-ruby-mode) . embrace-ruby-mode-hook))
  :bind (("C-c i s" . embrace-commander))
  :config
  (defun +editor-embrace-markdown-mode-h ()
    "Add embrace pairs in `markdown-mode'."
    (dolist (pair '((?* "*" . "*")
                    (?\ "\\" . "\\")
                    (?$ "$" . "$")
                    (?/ "/" . "/")))
      (embrace-add-pair (car pair) (cadr pair) (cddr pair))))
  (add-hook 'markdown-mode-hook #'+editor-embrace-markdown-mode-h))

;; Visualize matching parentheses.
(use-package paren
  :hook (on-first-buffer . show-paren-mode))

;; Highlight brackets according to their depth.
(use-package rainbow-delimiters
  :ensure t
  :hook (( lisp-mode lisp-data-mode emacs-lisp-mode ielm-mode
           scheme-mode racket-mode hy-mode lfe-mode dune-mode
           clojure-mode clojure-ts-mode fennel-mode)
         . rainbow-delimiters-mode)
  :config
  ;; Only use for highlighting unmatched delimiters.
  (setq rainbow-delimiters-max-face-count 1))

;; Visualize bogus whitespace. Taken from Doom Emacs.
(use-package whitespace
  :defer t
  :init
  (setopt whitespace-line-column nil
          whitespace-style
          '( face indentation tabs tab-mark spaces space-mark
             newline newline-mark trailing lines-tail)
          whitespace-display-mappings
          '((tab-mark ?\t [8250 ?\t])
            (newline-mark ?\n [172 ?\n])
            (space-mark ?\  [183] [?.])))

  (defun +editor-highlight-non-default-indentation-h ()
    "Highlight whitespace at odds with `indent-tabs-mode'.
That is, highlight tabs if `indent-tabs-mode' is `nil', and highlight spaces at
the beginnings of lines if `indent-tabs-mode' is `t'. The purpose is to make
incorrect indentation in the current buffer obvious to you.

Does nothing if `whitespace-mode' or `global-whitespace-mode' is already active
or if the current buffer is read-only or not file-visiting."
    (unless (or (eq major-mode 'fundamental-mode)
                (bound-and-true-p global-whitespace-mode)
                (null buffer-file-name))
      (require 'whitespace)
      (set (make-local-variable 'whitespace-style)
           (cl-union (if indent-tabs-mode
                         '(indentation)
                       '(tabs tab-mark))
                     (when whitespace-mode
                       (remq 'face whitespace-active-style))))
      (cl-pushnew 'face whitespace-style) ; must be first
      (whitespace-mode +1)))
  (add-hook 'after-change-major-mode-hook #'+editor-highlight-non-default-indentation-h 90))

;; Unobtrusively trim extraneous white-space *ONLY* in lines edited.
(use-package ws-butler
  :ensure t
  :hook (on-first-buffer . ws-butler-global-mode))

;; EditorConfig Emacs plugin.
(use-package editorconfig
  :ensure t
  :hook (on-first-buffer . editorconfig-mode)
  :config (setopt editorconfig-trim-whitespaces-mode 'ws-butler-mode))

;; Run code formatters on save.
(use-package apheleia
  :ensure t
  :hook (on-first-file . apheleia-global-mode)
  :config
  ;; Format shell scripts with tabs.
  (add-to-list 'apheleia-mode-alist '(sh-mode . shfmt))
  (setf (alist-get 'shfmt apheleia-formatters) '("shfmt"))
  ;; Format nix-ts-mode with nixfmt.
  (add-to-list 'apheleia-mode-alist '(nix-ts-mode . nixfmt)))

;; A Collection of Ridiculously Useful eXtensions.
(use-package crux
  :ensure t
  :defer t)

(provide '+editor)
