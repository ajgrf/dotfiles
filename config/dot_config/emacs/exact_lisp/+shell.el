;;; +shell.el -*- lexical-binding: t; -*-

;; Set up inferior shell, for running a shell in an Emacs buffer.
(use-package shell
  :config
  ;; Recognize the password prompt from doas.
  (setopt comint-password-prompt-regexp
          (concat comint-password-prompt-regexp
                  "\\|^doas (.*@.*) password: \\'"))

  ;;; Kill buffer when the shell exits.
  ;; https://emacs.stackexchange.com/a/48307
  (defun +shell--kill-buffer-on-exit-h ()
    "Kill the buffer when the shell process exits."
    (require '+run)
    (set-process-sentinel (get-buffer-process (current-buffer))
                          #'+run--kill-buffer-sentinel))
  (add-hook 'shell-mode-hook #'+shell--kill-buffer-on-exit-h)

  ;; Make C-w behave more like bash.
  (defun +shell--modify-word-chars-h ()
    (dolist (c '(?_ ?- ?.))
      (modify-syntax-entry c "w"))
    (modify-syntax-entry ?/ "-"))
  (add-hook 'shell-mode-hook #'+shell--modify-word-chars-h)

  ;; Show =apt= progress bars in the minibuffer.
  ;; https://oremacs.com/2019/03/24/shell-apt/
  (defun +shell--apt-progress-message-a (begin end)
    "Fix progress bars for e.g. apt(8).
Display progress in the mode line instead."
    (let ((end-marker (copy-marker end))
          mb)
      (save-excursion
        (goto-char (copy-marker begin))
        (while (re-search-forward "\0337" end-marker t)
          (setq mb (match-beginning 0))
          (when (re-search-forward "\0338" end-marker t)
            (let ((progress (buffer-substring-no-properties
                             (+ mb 2) (- (point) 2))))
              (delete-region mb (point))
              (message (replace-regexp-in-string
                        "%" "%%"
                        (ansi-color-apply progress)))))))))
  (advice-add 'ansi-color-apply-on-region :before #'+shell--apt-progress-message-a))

;; Bash completion for the shell buffer.
(use-package bash-completion
  :ensure t
  :commands (bash-completion-dynamic-complete
             bash-completion-dynamic-complete-nocomint)
  :init (add-hook 'shell-dynamic-complete-functions
                  #'bash-completion-dynamic-complete))

(provide '+shell)
