;;; +eat.el -*- lexical-binding: t; -*-

;; Emulate A Terminal, in a region, in a buffer and in Eshell.
(use-package eat
  :ensure t
  :unless IS-WINDOWS
  :hook (eshell-load . eat-eshell-mode)
  :bind ( :map mode-specific-map
          ("T" . eat-project)
          :map project-prefix-map
          ("t" . eat-project))
  :config
  (setopt eat-enable-shell-prompt-annotation nil
          eat-kill-buffer-on-exit t
          eat-term-scrollback-size 1000000)

  (when (featurep '+meow)
    (defun +eat-meow-switch-to-normal-h ()
      "Update cursor and switch to `eat-emacs-mode' on entering normal state."
      (setq-local eat-default-cursor-type '(t nil nil))
      (eat-emacs-mode))

    (defun +eat-meow-switch-to-insert-h ()
      "Update cursor and switch to `eat-semi-char-mode' on entering insert state."
      (setq-local eat-default-cursor-type '(bar nil nil))
      (when eat-terminal
        (eat-semi-char-mode)))

    (defun +eat-meow-setup-h ()
      "Install Meow state hooks for `eat-mode'."
      (add-hook 'meow-normal-mode-hook '+eat-meow-switch-to-normal-h nil :local)
      (add-hook 'meow-insert-mode-hook '+eat-meow-switch-to-insert-h nil :local))
    (add-hook 'eat-mode-hook '+eat-meow-setup-h)))

;; HACK: enable terminal support in compilation buffer
;; https://codeberg.org/akib/emacs-eat/issues/33#issuecomment-1127805
(defun +eat-compilation-start-a (command &optional mode name-function highlight-regexp continue)
  (require 'eat)
  (let ((name-of-mode "compilation")
        (dir default-directory)
        outbuf)
    (if (or (not mode) (eq mode t))
        (setq mode #'compilation-minor-mode)
      (setq name-of-mode (replace-regexp-in-string "-mode\\'" "" (symbol-name mode))))
    (with-current-buffer
        (setq outbuf
              (get-buffer-create
               (compilation-buffer-name name-of-mode mode name-function)))
      (setq default-directory dir)
      (setq buffer-read-only nil)
      (erase-buffer)
      (compilation-insert-annotation
       "-*- mode: " name-of-mode
       "; default-directory: "
       (prin1-to-string (abbreviate-file-name default-directory))
       " -*-\n")
      (compilation-insert-annotation
       (format "%s started at %s\n\n"
               mode-name
               (substring (current-time-string) 0 19))
       command "\n")
      (eat-mode)
      (setq-local eat-kill-buffer-on-exit nil)
      (eat-exec outbuf "*compile*" shell-file-name nil (list "-lc" command))
      (run-hook-with-args 'compilation-start-hook (get-buffer-process outbuf))
      (eat-emacs-mode)
      (funcall mode)
      (setq next-error-last-buffer outbuf)
      (display-buffer outbuf '(nil (allow-no-window . t)))
      (when-let (w (get-buffer-window outbuf))
        (set-window-start w (point-min))))))
(advice-add #'compilation-start :override #'+eat-compilation-start-a)

(eval-after-load 'compile
  '(unless (fboundp 'compilation-insert-annotation)
     (defun compilation-insert-annotation (&rest args)
       "Insert ARGS at point, adding the `compilation-annotation' text property.
This property is used to distinguish output of the compilation
process from additional information inserted by Emacs."
       (let ((start (point)))
         (apply #'insert args)
         (put-text-property start (point) 'compilation-annotation t)))))

(provide '+eat)
