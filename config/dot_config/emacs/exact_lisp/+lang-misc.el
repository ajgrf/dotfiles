;;; +lang-misc.el -*- lexical-binding: t; -*-

;; Major mode for editing comma/char separated values.
(use-package csv-mode
  :ensure t
  :defer t)

;; Major mode for editing Docker's Dockerfiles.
(use-package dockerfile-mode
  :ensure t
  :defer t)

;; Major mode for Hashicorp Configuration Language.
(use-package hcl-mode
  :ensure t
  :defer t)

;; Major mode for Terraform configuration file.
(use-package terraform-mode
  :ensure t
  :defer t)

;; Justfile editing mode.
(use-package just-mode
  :ensure t
  :defer t)

;; Major mode for driving just files.
(use-package justl
  :ensure t
  :defer t
  ;; :bind ( :map mode-specific-map
  ;;         ("J" . justl-exec-recipe-in-dir))
  )

;; For configuring that *other* editor.
(use-package vimscript-ts-mode
  :ensure t
  :if (treesit-available-p)
  :mode "\\.vim\\(rc\\)?\\'"
  :init
  (add-to-list 'treesit-language-source-alist
               '(vim "https://github.com/neovim/tree-sitter-vim.git"))
  :config
  ;; HACK: Disable embedded Lua and Ruby parsers until I get those working.
  (unless EMACS30+
    (defun +lang-vimscript-disable-embedded-parsers-a (orig-fn &rest args)
      (defun +lang-vimscript-treesit-ready-p-a (orig-fn language &optional quiet)
        (let ((language* (if (memq language '(lua ruby))
                             nil
                           language)))
          (apply orig-fn language* quiet nil)))
      (advice-add 'treesit-ready-p :around #'+lang-vimscript-treesit-ready-p-a)
      (apply orig-fn args)
      (advice-remove 'treesit-ready-p #'+lang-vimscript-treesit-ready-p-a))
    (advice-add 'vimscript-ts-mode :around
                #'+lang-vimscript-disable-embedded-parsers-a)))

;; Major mode for editing YAML files.
(use-package yaml-mode
  :ensure t
  :defer t)

(provide '+lang-misc)
