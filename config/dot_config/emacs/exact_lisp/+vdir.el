;;; +vdir.el -*- lexical-binding: t; -*-

;; Import, edit and create calendar events through khal.
(use-package khalel
  :ensure t
  :if (executable-find "khal")
  :after org
  :config
  (setopt khalel-default-alarm "0"
          khalel-import-start-date "-14d"
          khalel-import-end-date "+60d"
          khalel-import-org-file (file-name-concat org-directory "calendar.org")
          khalel-import-org-file-confirm-overwrite nil)
  (khalel-add-capture-template)

  ;; Run vdirsyncer automatically.

  (defvar +vdir-update-interval 1800
    "Number of seconds between vdirsyncer updates.
If nil, don't update automatically.")

  (defvar +vdir--update-timer nil
    "The vdirsyncer update timer.")

  (defun +vdir--post-update-sentinel (process _signal)
    (let ((exit-status (process-exit-status process)))
      (when (memq (process-status process) '(exit stop))
        (kill-buffer (process-buffer process))
        (if (= exit-status 0)
            (progn
              (khalel-import-events)
              (message "Vdirsyncer process completed successfully!"))
          (message "Vdirsyncer process returned with non-zero exit code"))
        (sit-for 5))))

  (defun +vdir-update-in-background ()
    "Run vdirsyncer process to synchronize local calendar entries."
    (interactive)
    (let* ((buf (generate-new-buffer-name "*vdirsyncer*"))
           (proc (start-process
                  "*+vdir-update-process*"
                  buf
                  (or khalel-vdirsyncer-command
                      (executable-find "vdirsyncer"))
                  "sync")))
      (set-process-sentinel proc #'+vdir--post-update-sentinel)))

  (defun +vdir-cancel-timer ()
    "Cancel the vdirsyncer update timer."
    (interactive)
    (when +vdir--update-timer
      (cancel-timer +vdir--update-timer)
      (setq +vdir--update-timer nil)))

  (defun +vdir-start-timer (&rest _args)
    "Start the vdirsyncer update timer."
    (interactive)
    (when (and +vdir-update-interval (null +vdir--update-timer))
      (setq +vdir--update-timer
            (run-at-time 0 +vdir-update-interval
                         #'+vdir-update-in-background))))
  (advice-add 'org-agenda :before #'+vdir-start-timer)
  (advice-add 'org-timeblock :before #'+vdir-start-timer)

  ;; HACK: re-evaluate khalel.el to fix type error:
  ;; error in process sentinel: Wrong type argument: integer-or-marker-p, nil
  ;; https://gitlab.com/hperrey/khalel/-/issues/11
  (load "khalel.el" nil t t nil))

;; Integrate with khard.
(use-package khardel
  :ensure t
  :if (executable-find "khard")
  :defer t)

;; Support for links to khard contact buffers in Org.
(use-package khardel-org
  :if (executable-find "khard")
  :after org
  :commands ( khardel-org--follow-link
              khardel-org--link-complete
              khardel-org--store-link)
  :init
  (org-link-set-parameters "khardel"
                           :complete #'khardel-org--link-complete
                           :follow #'khardel-org--follow-link
                           :store #'khardel-org--store-link))

(provide '+vdir)
