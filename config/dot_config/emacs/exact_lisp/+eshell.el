;;; +eshell.el -*- lexical-binding: t; -*-

;; Command shell written in Emacs Lisp.
(use-package eshell
  :commands eshell
  :bind ( :map eshell-mode-map
          ("C-d"       . +eshell-ctrl-d)
          ("C-s"       . +eshell-toggle-scroll-to-bottom-on-output)
          ("<up>"      . eshell-previous-input)
          ("<down>"    . eshell-next-input)
          ("<prior>"   . eshell-previous-matching-input-from-input)
          ("<next>"    . eshell-next-matching-input-from-input)
          ("S-<prior>" . scroll-down-command)
          ("S-<next>"  . scroll-up-command))
  :config
  (setopt eshell-banner-message ""
          eshell-destroy-buffer-when-process-dies t
          eshell-error-if-no-glob t
          eshell-glob-case-insensitive t
          eshell-hist-ignoredups t
          eshell-history-size nil
          eshell-input-filter #'eshell-input-filter-initial-space
          eshell-kill-processes-on-exit t
          eshell-scroll-to-bottom-on-input nil
          eshell-scroll-to-bottom-on-output nil
          eshell-prompt-function #'+eshell-prompt-fn
          eshell-prompt-regexp "^[^#$\n]*[#$] "
          eshell-visual-commands nil)

  (setopt eshell-command-aliases-list
          '(("edit" "find-file $1")
            ("ls" "ls --color=auto --group-directories-first -v $*")
            ("la" "ls -A $*")
            ("ll" "ls --time-style=long-iso -lah $*")
            ("mkcd" "mkdir $1 && cd $1")
            ("youtube-dl" "+youtube-dl-run-in-popup $*")))

  (unless IS-WINDOWS
    (require 'em-tramp)
    (add-to-list 'eshell-command-aliases-list
                 '("sudo" "eshell/sudo $*")))

  (defun +eshell-prompt-fn ()
    "Return a string to use as the prompt for eshell."
    (require 'vc-git)
    (concat (when (not (= 0 eshell-last-command-status))
              (concat (number-to-string eshell-last-command-status) "|"))
            (abbreviate-file-name (eshell/pwd))
            (let* ((branch (car (vc-git-branches)))
                   (env (concat (if branch (concat "(" branch ")"))
                                (if (+eshell--nix-shell-p) "[nix]"))))
              (if env (concat " " env)))
            (if (= (user-uid) 0) "# " "$ ")))

  (defun +eshell--nix-shell-p ()
    "Return non-nil if we're in a nix shell environment."
    (let ((exclude-regexp "\\(?:emacs-packages-deps\\|libexec/emacs\\)"))
      (seq-find (lambda (path)
                  (and (string-prefix-p "/nix/store" path)
                       (not (string-match-p exclude-regexp path))))
                exec-path)))

  (defun +eshell-ctrl-d ()
    "Send EOF or exit Eshell, like Ctrl-D in terminal emulators."
    (interactive)
    ;; Send EOF if a process is running.
    (cond (eshell-process-list
           (eshell-send-eof-to-process))
          ;; Otherwise exit eshell if current input is empty.
          ((save-excursion
             (eshell-bol)
             (= (point) (point-at-eol)))
           (kill-buffer-and-window))))

  (defun +eshell-toggle-scroll-to-bottom-on-output ()
    "Toggle `eshell-scroll-to-bottom-on-output'."
    (interactive)
    (setopt eshell-scroll-to-bottom-on-output
            (not eshell-scroll-to-bottom-on-output)))

  (defun +eshell-export-INSIDE_EMACS-h ()
    (setenv "INSIDE_EMACS" (format "%s,eshell" emacs-version)))
  (add-hook 'eshell-mode-hook #'+eshell-export-INSIDE_EMACS-h)

  ;; Make cat with syntax highlight.
  ;; https://github.com/manateelazycat/aweshell/blob/master/aweshell.el
  (defun +eshell-cat-with-syntax-highlight (filename)
    "Like cat(1) but with syntax highlighting."
    (let ((existing-buffer (get-file-buffer filename))
          (buffer (find-file-noselect filename)))
      (eshell-print
       (with-current-buffer buffer
         (if (fboundp 'font-lock-ensure)
             (font-lock-ensure)
           (with-no-warnings
             (font-lock-fontify-buffer)))
         (let ((contents (buffer-string)))
           (remove-text-properties 0 (length contents) '(read-only nil) contents)
           contents)))
      (unless existing-buffer
        (kill-buffer buffer))
      nil))
  (advice-add 'eshell/cat :override #'+eshell-cat-with-syntax-highlight))

;; Bash completion for eshell.
(use-package bash-completion
  :ensure t
  :unless IS-WINDOWS
  :after eshell
  :config
  (defun +eshell-bash-completion-capf-nonexclusive ()
    "Bash completion function for `completion-at-point-functions'.

Returns the same list as the one returned by
`bash-completion-dynamic-complete-nocomint' appended with
\(:exclusive no) so that other completion functions are tried
when bash-completion fails to match the text at point."
    (let* ((bol-pos (save-mark-and-excursion
                      (eshell-bol)
                      (point)))
           (compl (bash-completion-dynamic-complete-nocomint
                   bol-pos
                   (point) t)))
      (when compl
        (append compl '(:exclusive no)))))

  (defun +eshell-setup-bash-completion-h ()
    (add-hook 'completion-at-point-functions
              #'+eshell-bash-completion-capf-nonexclusive nil t))
  (add-hook 'eshell-mode-hook #'+eshell-setup-bash-completion-h))

;; Highlight eshell commands.
(use-package eshell-syntax-highlighting
  :ensure t
  :hook (eshell-mode . eshell-syntax-highlighting-mode))

;; Integrate eshell with atuin, a shell history tool.
(use-package eshell-atuin
  :ensure t
  :after eshell
  :config
  (eshell-atuin-mode))

(provide '+eshell)
