;;; +check.el -*- lexical-binding: t; -*-

;; A universal on-the-fly syntax checker.
(use-package flymake
  :hook (prog-mode . +check-maybe-enable-flymake-h)
  :config
  (defun +check-maybe-enable-flymake-h ()
    (unless (eq major-mode 'emacs-lisp-mode)
      (flymake-mode +1))))

;; Collection of checkers for flymake.
(use-package flymake-collection
  :ensure t
  :hook (flymake-mode . flymake-collection-hook-setup))

;; Enchanted spell checker.
(use-package jinx
  :ensure t
  :if (and module-file-suffix
           (or IS-LINUX IS-MAC)
           (not IS-TERMUX))
  :hook (on-first-buffer . global-jinx-mode)
  :bind ([remap ispell-word] . jinx-correct)
  :config
  (setopt global-jinx-modes '( (not nxml-mode)
                               text-mode
                               prog-mode
                               conf-mode)))

(provide '+check)
