;;; +lang-sh.el -*- lexical-binding: t; -*-

;; Shell-script editing commands for Emacs.
(use-package sh-script
  :mode ("\\.shinit\\'" . sh-mode)
  :config
  (defun +lang-sh-configure-indent-h ()
    "Configure shell script indentation style to match shfmt."
    (setq-local indent-tabs-mode t
                tab-width 4
                sh-basic-offset tab-width
                sh-indent-after-continuation 'always
                sh-indent-for-case-alt '+
                sh-indent-for-case-label 0))
  (add-hook 'sh-mode-hook #'+lang-sh-configure-indent-h)
  (add-hook 'bash-ts-mode-hook #'+lang-sh-configure-indent-h))

;; Mode for editing PowerShell scripts.
(use-package powershell
  :ensure t
  :defer t)

(provide '+lang-sh)
