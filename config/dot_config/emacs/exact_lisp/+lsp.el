;;; +lsp.el -*- lexical-binding: t; -*-

;; TODO: add debug adapter protocol support with dape

;; Emacs PolyGLOT - Language Server Protocol (LSP) client.
(use-package eglot
  :ensure t
  :defer t
  :init
  (setopt eglot-sync-connect 1
          eglot-connect-timeout 10
          eglot-autoshutdown t))

;; Jump to workspace symbols with eglot and consult.
(use-package consult-eglot
  :ensure t
  :after eglot
  :bind ( :map eglot-mode-map
          ([remap xref-find-apropos] . consult-eglot-symbols)))

;; Embark integration for `consult-eglot'.
(use-package consult-eglot-embark
  :ensure t
  :after (embark consult-eglot)
  :config
  (consult-eglot-embark-mode))

(provide '+lsp)
