;;; +workspaces.el -*- lexical-binding: t; -*-

;; TODO: improve tab navigation bindings

;; Operations on the current project.
(use-package project
  :defer t
  :config
  (setopt project-switch-commands #'project-dired
          project-vc-extra-root-markers '(".project")))

;; Support for `direnv' that operates buffer-locally.
(use-package envrc
  :ensure t
  :when (executable-find "direnv")
  :hook (on-first-file . envrc-global-mode)
  :config
  ;; Ensure babel's execution environment matches the host buffer's.
  (advice-add 'org-babel-execute-src-block :around #'envrc-propagate-environment))

;; Frame-local tabs with named persistent window configurations.
(use-package tab-bar
  :hook (on-init-ui . tab-bar-mode)
  :init
  (setopt tab-bar-show 1
          tab-bar-tab-hints t
          tab-bar-new-tab-to 'right
          tab-bar-close-tab-select 'recent
          tab-bar-new-tab-choice "*scratch*"
          tab-bar-close-button-show nil
          tab-bar-new-button-show nil)

  ;; New frames follow `tab-bar-new-tab-choice' too.
  (add-to-list 'after-make-frame-functions
               '+workspaces-after-make-frame)
  :config
  (dotimes (i 9)
    (defalias (intern (format "+workspaces/switch-to-%d" (1+ i)))
      (lambda () (interactive) (tab-bar-select-tab (1+ i)))
      (format "Switch to tab #%d" (1+ i))))

  (defun +workspaces-tab-name-non-popup ()
    "Generate tab name from the last selected non-popup buffer."
    (let* ((windows (window-list nil 'no-minibuf nil))
           (window (seq-find (lambda (w)
                               (when (fboundp 'popper-popup-p)
                                 (not (popper-popup-p (window-buffer w)))))
                             windows
                             (seq-first windows))))
      (buffer-name (window-buffer window))))

  (defun +workspaces-close-last-tab (tab)
    "Closing the last tab replaces it with `tab-bar-new-tab-choice'."
    (tab-bar-new-tab)
    (tab-bar-switch-to-recent-tab)
    (tab-bar-close-tab))

  (setopt tab-bar-tab-name-function #'+workspaces-tab-name-non-popup
          tab-bar-close-last-tab-choice #'+workspaces-close-last-tab)

  (defun +workspaces-after-make-frame (frame)
    "Follow `tab-bar-new-tab-choice' when opening new frames too."
    (tab-bar-new-tab)
    (tab-bar-move-tab-to-frame 1 nil nil frame 1)
    (with-selected-frame frame
      (tab-bar-close-other-tabs 1))))

;; Save/restore sets of windows, tabs/frames, and their buffers.
(use-package activities
  :ensure t
  :hook (emacs-startup . activities-mode)
  :bind (("C-x C-a C-n" . activities-new)
         ("C-x C-a C-d" . activities-define)
         ("C-x C-a C-a" . activities-resume)
         ("C-x C-a C-s" . activities-suspend)
         ("C-x C-a C-k" . activities-kill)
         ("C-x C-a RET" . activities-switch)
         ("C-x C-a b" . activities-switch-buffer)
         ("C-x C-a g" . activities-revert)
         ("C-x C-a l" . activities-list))
  :init
  ;; Prevent `edebug' default bindings from interfering.
  (setq edebug-inhibit-emacs-lisp-mode-bindings t)
  :config
  (activities-tabs-mode))

(provide '+workspaces)
