;;; +org.el -*- lexical-binding: t; -*-

;; TODO: add org-web-tools package

(use-package org
  :ensure t
  :hook (org-mode . org-indent-mode)
  ;; Fix (invalid-function org-assert-version) error when install from ELPA.
  :autoload org-assert-version
  :bind ( :map org-mode-map
          ("C-c C-x l" . org-toggle-link-display)
          ("C-c C-x y" . org-cliplink)
          ("C-c C-x z" . org-anki-cloze-dwim))
  :preface
  (defvar org-modules
    '(ol-bibtex
      org-habit))
  :init
  (setq org-directory (if IS-ANDROID
                          "/storage/emulated/0/Org"
                        (file-truename "~/Org"))
        org-agenda-files (list (file-name-as-directory org-directory)))

  ;; Open archive files in org-mode.
  (add-to-list 'auto-mode-alist '("\\.org_archive\\'" . org-mode))
  :config
  (setq org-agenda-default-appointment-duration 30
        org-agenda-log-mode-items '(closed clock state)
        org-agenda-search-headline-for-time nil
        org-agenda-span 'day
        org-agenda-start-day nil
        org-agenda-start-on-weekday 0
        org-agenda-time-grid
        '((daily remove-match)
          (800 1000 1200 1400 1600 1800 2000)
          "........" "")
        org-agenda-timegrid-use-ampm t
        org-agenda-todo-ignore-scheduled t
        org-agenda-window-setup 'current-window
        org-archive-location (file-name-concat org-directory
                                               "archive"
                                               (format-time-string "%Y")
                                               "%s_archive::")
        org-default-notes-file (file-name-concat org-directory "inbox.org")
        org-export-show-temporary-export-buffer nil
        org-id-link-to-org-use-id t
        org-image-actual-width nil
        org-fontify-done-headline nil
        org-link-abbrev-alist '(("attach" . org-attach-expand-link))
        org-log-done 'time
        org-log-into-drawer "LOGBOOK"
        org-outline-path-complete-in-steps nil
        org-property-format "%-15s %s"
        org-refile-allow-creating-parent-nodes 'confirm
        org-refile-targets '((org-agenda-files :maxlevel . 3))
        org-refile-use-outline-path 'file
        org-return-follows-link t
        org-src-preserve-indentation t
        org-startup-folded 'showall
        org-startup-with-inline-images t
        org-todo-keywords '((sequence "TODO(t)" "NEXT(n)" "WAIT(w@)"
                                      "REM(r)" "|" "DONE(d)" "KILL(k)"))
        system-time-locale "C"        ; always use English timestamps
        calendar-christian-all-holidays-flag t
        holiday-bahai-holidays nil
        holiday-hebrew-holidays nil
        holiday-islamic-holidays nil
        holiday-oriental-holidays nil)

  ;; Improve agenda formatting, excluding the category on mobile
  ;; and using the title as category when possible.
  (setq org-agenda-prefix-format
        (if (or IS-ANDROID IS-TERMUX)
            '((agenda . " %i %?-12t% s")
              (todo . " %i ")
              (tags . " %i ")
              (search . " %i "))
          '((agenda . " %i %(+org-agenda-category 12)%?-12t% s")
            (todo . " %i %(+org-agenda-category 12) ")
            (tags . " %i %(+org-agenda-category 12) ")
            (search . " %i %(+org-agenda-category 12) "))))

  ;; Disable tag inheritance for the "agenda" tag.
  (add-to-list 'org-tags-exclude-from-inheritance "agenda")

  ;; Load sensitive configuration, like capture templates,
  ;; from `org-directory'.

  (defvar +org-local-config-file
    (file-name-concat org-directory "config.eld")
    "Local configuration file for `org-mode'.")

  (defvar +org-local-config-alist nil
    "Local configuration data for `org-mode'.")

  (defun +org-reload-local-config ()
    "Re-read local `org-mode' configuration."
    (interactive)
    (when (file-readable-p +org-local-config-file)
      (setq +org-local-config-alist
            (unless (zerop
                     (or (file-attribute-size (file-attributes +org-local-config-file))
                         0))
              (with-temp-buffer
                (insert-file-contents +org-local-config-file)
                (read (current-buffer))))))
    (dolist (element +org-local-config-alist)
      (set (car element) (cdr element))))

  ;; Read local `org-mode' configuration.
  (+org-reload-local-config)

  ;; Load snippets from `org-directory'.
  (add-to-list 'yas-snippet-dirs
               (file-name-concat org-directory "snippets/"))

  ;; https://d12frosted.io/posts/2020-06-24-task-management-with-roam-vol2.html
  (defun +org-agenda-category (&optional len)
    "Get category or title of item at point for agenda."
    (let (file-name title category category* result)
      (when buffer-file-name
        (setq file-name (file-name-sans-extension
                         (file-name-nondirectory buffer-file-name)))
        (setq title (org-with-point-at 1
                      (when (re-search-forward (concat "^#\\+title: *\\(.*\\)")
                                               (point-max) t)
                        (buffer-substring-no-properties
                         (match-beginning 1)
                         (match-end 1)))))
        (setq category (org-get-category))
        (setq category* (unless (or (string-equal category "???")
                                    (null category))
                          (concat category ":"))))
      (let ((result (or (if (and title
                                 (string-equal category file-name))
                            (concat title ":")
                          category*)
                        "")))
        (if (numberp len)
            (string-replace
             " :" ": "
             (s-truncate len
                         (s-pad-right len " " result)
                         ": "))
          result))))

  (defun +org-workspace-p (buffer &optional _action)
    (if-let* ((buffer (if (stringp buffer)
                          (get-buffer buffer)
                        buffer))
              (file-name (buffer-file-name buffer)))
        (file-in-directory-p file-name org-directory)))

  (defun +org-auto-save-h ()
    "Automatically save my main Org files."
    (when (+org-workspace-p (current-buffer))
      (setq-local auto-save-visited-mode t)))
  (add-hook 'org-mode-hook #'+org-auto-save-h)

  (defvar +org-sound-file (expand-file-name "task_complete.mp3" org-directory)
    "Path to sound file used by `+org-play-task-complete-sound-h'.")

  (defun +org-play-task-complete-sound-h (change-plist)
    "Play a sound when marking a task as done."
    (let ((sound +org-sound-file)
          (ffplay (executable-find "ffplay"))
          (from-state (plist-get change-plist :from))
          (to-state (plist-get change-plist :to)))
      (when (and (string-equal to-state "DONE")
                 (not (equal from-state to-state))
                 (file-exists-p sound)
                 ffplay)
        (call-process-shell-command
         (format "ffplay -nodisp -autoexit %s >/dev/null 2>&1" sound) nil 0))))
  (add-hook 'org-trigger-hook #'+org-play-task-complete-sound-h))

(use-package org-agenda
  :defer t
  :config
  ;; Visit narrowed subtree of agenda items.
  (advice-add 'org-agenda-goto      :around #'+org-visit-indirect-buffer-a)
  (advice-add 'org-agenda-switch-to :around #'+org-visit-indirect-buffer-a))

(use-package org-attach
  :defer t
  :init
  (setq org-attach-store-link-p t
        org-attach-use-inheritance t)
  :config
  (setq org-attach-id-dir (file-name-concat org-directory ".attach/")))

;; Growl-style notification system for Emacs.
(use-package alert
  :ensure t
  :defer t
  :config
  (setq alert-default-style
        (cond (IS-TERMUX 'termux)
              (IS-WSL 'message)
              (IS-LINUX 'notifications)
              (t 'message))))

;; Customizable org-agenda notifications.
(use-package org-wild-notifier
  :ensure t
  :after org
  :config
  (setq org-wild-notifier-alert-time '(0)
        org-wild-notifier-keyword-whitelist nil)

  (org-wild-notifier-mode))

;; Custom link type for files on a shared Egnyte drive.
(use-package ol
  :defer t
  :config
  (org-link-set-parameters "egnyte" :follow #'+org-egnyte-open)

  (defvar +org-egnyte-prefix
    (cond (IS-WINDOWS "Z:/")
          (IS-WSL "/mnt/z/")
          (t (expand-file-name "~/Share/Egnyte/")))
    "Prefix to append to the path of `egnyte:' links.")

  (defun +org-egnyte-open (path arg)
    "Visit the file on Egnyte at PATH."
    (if arg
        (org-link-open-as-file (concat +org-egnyte-prefix path) arg)
      (browse-url (concat +org-egnyte-prefix path)))))

;; Modern Org style.
(use-package org-modern
  :ensure t
  :after org
  :config
  (setq org-modern-star '("\u25cf" "\u25cb" "\u25ca" "\u25ac" "\u2022")
        org-modern-checkbox (when (display-graphic-p)
                              `((?X . ,(compose-chars #x25a1 #x00d7))
                                (?- . ,(compose-chars #x25a1 #x2013))
                                (?\s . "\u25a1")))
        org-modern-progress nil
        org-modern-internal-target '(" \u2192 " t " ")
        org-modern-radio-target '(" \u25ca " t " ")
        org-modern-table (display-graphic-p)
        org-modern-table-horizontal 1.0)

  (unless +ui-use-unicode
    (setq org-modern-star nil
          org-modern-checkbox nil
          org-modern-internal-target nil
          org-modern-radio-target nil))

  ;; Fix table alignment in `mixed-pitch-mode'.
  ;; See https://github.com/minad/org-modern/issues/99
  (defun +org-modern--pre-redisplay-a (_)
    "Compute font parameters before redisplay."
    (when-let ((box (and org-modern-label-border
                         (face-attribute 'org-modern-label :box nil t))))
      (unless (equal (and (listp box) (plist-get box :color))
                     (face-attribute 'default :background nil t))
        (org-modern--update-label-face)))
    (setf org-modern--table-sp-width
          (let* ((default-height (cadr (assoc :height (assoc 'default face-remapping-alist))))
                 (face-remapping-alist `((default
                                          ,(append '(:inherit org-table) (when default-height `(:height ,default-height)))
                                          default)
                                         ,@face-remapping-alist)))
            (default-font-width)))
    (setf (cadr org-modern--table-overline) (face-attribute 'org-table :foreground nil t)))
  (advice-add 'org-modern--pre-redisplay :override #'+org-modern--pre-redisplay-a)

  ;; Apply to all Org buffers.
  (global-org-modern-mode))

;; Supercharge your agenda.
(use-package org-super-agenda
  :ensure t
  :after org-agenda
  :config
  ;; Note that `org-super-agenda-groups' is configured in config.eld
  (setq org-super-agenda-hide-empty-groups t
        org-super-agenda-show-message nil)
  (org-super-agenda-mode +1))

;; Interactive SVG calendar for org-mode tasks.
(use-package org-timeblock
  ;; HACK: Disable org-timeblock until it works with Emacs 30.
  :disabled
  :ensure t
  :if (image-type-available-p 'svg)
  :bind ( :map mode-specific-map
          ("B" . org-timeblock))
  :config
  ;; Display hours from 6am until midnight.
  (setq org-timeblock-scale-options '(6 . 21))

  ;; Visit narrowed subtree of timeblock items.
  (advice-add 'org-timeblock-goto :around #'+org-visit-indirect-buffer-a)
  (advice-add 'org-timeblock-goto-other-window :around #'+org-visit-indirect-buffer-a)
  (advice-add 'org-timeblock-list-goto :around #'+org-visit-indirect-buffer-a)
  (advice-add 'org-timeblock-list-goto-other-window :around #'+org-visit-indirect-buffer-a))

;; Org Query Language, search command, and agenda-like view.
(use-package org-ql
  :ensure t
  :defer t)

;; Insert org-mode links from the clipboard.
(use-package org-cliplink
  :ensure t
  :commands org-cliplink)

;; A presentation tool for org-mode.
(use-package org-tree-slide
  :ensure t
  :defer t
  :config
  (setopt org-tree-slide-slide-in-effect nil))

;; Sync Org notes to Anki via AnkiConnect.
(use-package org-anki
  :ensure t
  :defer t)

(defun +org-agenda ()
  "Open a default org-agenda view."
  (interactive)
  (org-agenda nil "n" nil))

;; Open Emacs straight to Org Mode.
(defun +org-goto-today-h ()
  "Show daily Org context."
  (interactive)
  (require 'org)
  (require 'denote)
  (when (and (= (length command-line-args) 1)
             (or (display-graphic-p)
                 IS-TERMUX)
             (file-directory-p org-directory))
    (find-file org-default-notes-file)
    (+org-agenda)))
(add-hook 'on-init-ui-hook #'+org-goto-today-h 95)

(defun +org-visit-indirect-buffer-a (orig-fn &rest args)
  "Visit narrowed subtree of Org agenda/timeblock items."
  (let ((buf nil)
        (org-indirect-buffer-display 'current-window))
    (save-window-excursion
      (apply orig-fn args)
      (org-tree-to-indirect-buffer)
      (setq buf (current-buffer)))
    (pop-to-buffer-same-window buf)))

;; Taken from Doom Emacs.

(defun +org--suppress-delete-other-windows-a (fn &rest args)
  "Prevent Org from killing other windows."
  (cl-letf (((symbol-function #'delete-other-windows)
             #'ignore)
            ((symbol-function #'delete-window)
             #'ignore))
    (apply fn args)))
(advice-add 'org-add-log-note :around #'+org--suppress-delete-other-windows-a)
(advice-add 'org-capture-place-template :around #'+org--suppress-delete-other-windows-a)
(advice-add 'org-export--dispatch-ui :around #'+org--suppress-delete-other-windows-a)
(advice-add 'org-agenda-get-restriction-and-command :around #'+org--suppress-delete-other-windows-a)
(advice-add 'org-goto-location :around #'+org--suppress-delete-other-windows-a)
(advice-add 'org-fast-tag-selection :around #'+org--suppress-delete-other-windows-a)
(advice-add 'org-fast-todo-selection :around #'+org--suppress-delete-other-windows-a)

;; Wait until an org-protocol link is opened via emacsclient to load
;; `org-protocol'. Normally you'd simply require `org-protocol' and use it,
;; but the package loads all of org for no compelling reason, so...
(defun +org--server-visit-files-a (fn files &rest args)
  "Advise `server-visit-files' to load `org-protocol' lazily."
  (if (not (cl-loop with protocol =
                    (if IS-WINDOWS
                        ;; On Windows, the file arguments for `emacsclient'
                        ;; get funnelled through `expand-file-path' by
                        ;; `server-process-filter'. This substitutes
                        ;; backslashes with forward slashes and converts each
                        ;; path to an absolute one. However, *all* absolute
                        ;; paths on Windows will match the regexp ":/+", so we
                        ;; need a more discerning regexp.
                        (regexp-quote
                         (or (bound-and-true-p org-protocol-the-protocol)
                             "org-protocol"))
                      ;; ...but since there is a miniscule possibility users
                      ;; have changed `org-protocol-the-protocol' I don't want
                      ;; this behavior for macOS/Linux users.
                      "")
                    for var in files
                    if (string-match-p (format "%s:/+" protocol) (car var))
                    return t))
      (apply fn files args)
    (require 'org-protocol)
    (apply #'org--protocol-detect-protocol-server fn files args)))
(advice-add 'server-visit-files :around #'+org--server-visit-files-a)
(eval-after-load 'org-protocol
  '(advice-remove 'server-visit-files #'org--protocol-detect-protocol-server))

;; Open org-capture in an external frame.

(defvar +org-capture-frame-parameters
  `((name . "org-capture")
    (width . 70)
    (height . 25)
    (transient . t)
    ,@(when IS-LINUX
        `((window-system . ,(if (boundp 'pgtk-initialized) 'pgtk 'x))
          (display . ,(or (getenv "WAYLAND_DISPLAY")
                          (getenv "DISPLAY")
                          ":0"))))
    ,(if IS-MAC '(menu-bar-lines . 1)))
  "TODO")

(defun +org-capture-frame-p (&rest _)
  "Return t if the current frame is an org-capture frame opened by
`+org-capture/open-frame'."
  (and (equal (alist-get 'name +org-capture-frame-parameters)
              (frame-parameter nil 'name))
       (frame-parameter nil 'transient)))

(defun +org-capture-cleanup-frame-h ()
  "Closes the org-capture frame once done adding an entry."
  (when (and (+org-capture-frame-p)
             (not org-capture-is-refiling))
    (delete-frame nil t)))
(add-hook 'org-capture-after-finalize-hook #'+org-capture-cleanup-frame-h)

(defun +org-capture/open-frame (&optional initial-input key)
  "Opens the org-capture window in a floating frame that cleans itself up once
you're done. This can be called from an external shell script."
  (interactive)
  (when (and initial-input (string-empty-p initial-input))
    (setq initial-input nil))
  (when (and key (string-empty-p key))
    (setq key nil))
  (let* ((frame-title-format "")
         (frame (if (+org-capture-frame-p)
                    (selected-frame)
                  (make-frame +org-capture-frame-parameters))))
    (select-frame-set-input-focus frame)  ; fix MacOS not focusing new frames
    (with-selected-frame frame
      (require 'org-capture)
      (condition-case ex
          (cl-letf (((symbol-function #'pop-to-buffer)
                     #'switch-to-buffer))
            (switch-to-buffer "*scratch*")
            (let ((org-capture-initial initial-input)
                  org-capture-entry)
              (when (and key (not (string-empty-p key)))
                (setq org-capture-entry (org-capture-select-template key)))
              (funcall #'org-capture)))
        ('error
         (message "org-capture: %s" (error-message-string ex))
         (delete-frame frame))))))

(defun +org-capture-available-keys ()
  "TODO"
  (string-join (mapcar #'car org-capture-templates) ""))

(defun +org-protocol-capture-open-frame-a (fn info)
  "Advise `org-protocol-capture' to open in a floating frame."
  (let* ((frame-title-format "")
         (frame (if (+org-capture-frame-p)
                    (selected-frame)
                  (make-frame +org-capture-frame-parameters))))
    (select-frame-set-input-focus frame) ; fix MacOS not focusing new frames
    (with-selected-frame frame
      (require 'org-capture)
      (condition-case ex
          (cl-letf (((symbol-function #'pop-to-buffer)
                     #'switch-to-buffer))
            (switch-to-buffer "*scratch*")
            (funcall fn info))
        ('error
         (message "org-capture: %s" (error-message-string ex))
         (delete-frame frame))))))
(advice-add 'org-protocol-capture :around #'+org-protocol-capture-open-frame-a)

(provide '+org)
