;;; +lang-cc.el -*- lexical-binding: t; -*-

;; Major mode for editing C and similar languages.
(use-package cc-mode
  :defer t
  :config
  (add-to-list 'c-default-style '(c-mode . "linux")))

;; Tree-sitter support for C and C++.
(use-package c-ts-mode
  :defer t
  :config
  (setopt c-ts-mode-indent-style 'linux))

;; Default to sane indent rules for C.
(defun +lang-cc-set-style-h ()
  (setq-local indent-tabs-mode t
              tab-width 8
              c-ts-mode-indent-offset tab-width))
(add-hook 'c-mode-hook    #'+lang-cc-set-style-h)
(add-hook 'c-ts-mode-hook #'+lang-cc-set-style-h)

(provide '+lang-cc)
