;;; +run.el -*- lexical-binding: t; -*-

(defun +run--kill-buffer-sentinel (process _signal)
  "Sentinel to kill buffer when its process exits."
  (when (memq (process-status process) '(exit stop))
    (kill-buffer (process-buffer process))))

(defun +run--notify-kill-buffer-sentinel (process signal)
  "Sentinel to kill buffer and print a message when its process exits."
  (when (memq (process-status process) '(exit stop))
    (kill-buffer (process-buffer process))
    (+run--status-message process)))

(defun +run--status-message (process)
  (let ((command (string-join (mapcar #'shell-quote-argument
                                      (process-command process))
                              " "))
        (exit-status (process-exit-status process)))
    (if (= exit-status 0)
        (message "Executed `%s' successfully!" command)
      (message "Failed executing `%s'" command))))

(defun +run-in-popup (command &rest args)
  "Run COMMAND in a popup with ARGS."
  (let* ((buffer-name (generate-new-buffer-name (concat "*" command "*")))
         (new-buffer (apply 'make-comint-in-buffer command buffer-name
                            command nil args))
         (proc (get-buffer-process new-buffer)))
    (set-process-sentinel proc #'+run--notify-kill-buffer-sentinel)
    (display-buffer new-buffer)))

(provide '+run)
