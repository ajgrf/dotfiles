;;; +lang-ledger.el -*- lexical-binding: t; -*-

;; Helper code for use with the `ledger' command-line tool.
(use-package ledger-mode
  :ensure t
  :mode "\\.journal\\'"
  :init
  (setq ledger-clear-whole-transactions nil
        ledger-narrow-on-reconcile nil
        ledger-post-amount-alignment-column 52
        ledger-reconcile-buffer-line-format "%(date)s  %-30(payee)s %-25(account)s %10(amount)s\n"
        ledger-reconcile-buffer-account-max-chars 25
        ledger-reconcile-buffer-payee-max-chars 30)

  ;; Make it work with hledger.
  (setq ledger-binary-path "hledger"
        ledger-mode-should-check-version nil
        ledger-report-auto-width nil
        ledger-report-links-in-register nil
        ledger-report-native-highlighting-arguments '("--color=always"))

  (setq ledger-reports
        '(("bal"             "%(binary) -f %(ledger-file) bal --tree")
          ("reg"             "%(binary) -f %(ledger-file) reg")
          ("payee"           "%(binary) -f %(ledger-file) reg payee:%(payee)")
          ("account"         "%(binary) -f %(ledger-file) areg %(account)")
          ("balancesheet"    "%(binary) -f %(ledger-file) balancesheet")
          ("incomestatement" "%(binary) -f %(ledger-file) incomestatement")
          ("cashflow"        "%(binary) -f %(ledger-file) cashflow not:tag:clopen")
          ("reconcile"       "%(binary) -f %(ledger-file) bal --no-total --empty status:'*' %(account)")))
  :config
  ;; Completion in `ledger-mode' seems to get stuck once there
  ;; are no matches available, so just abort when this happens.
  (defun +lang-ledger-setup-corfu-h ()
    "Configure Corfu completion for `ledger-mode'."
    (setq-local corfu-quit-no-match t))
  (add-hook 'ledger-mode-hook #'+lang-ledger-setup-corfu-h))

(use-package ledger-reconcile
  :bind ( :map ledger-reconcile-mode-map
          ("C-t" . ledger-reconcile-toggle)))

;; Flymake module to check hledger journals.
(use-package flymake-hledger
  :ensure t
  :hook (ledger-mode . flymake-hledger-enable)
  :config
  (setq flymake-hledger-checks '("ordereddates")))

(provide '+lang-ledger)
