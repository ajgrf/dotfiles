;;; +lang-haskell.el -*- lexical-binding: t; -*-

;; A Haskell editing mode.
(use-package haskell-mode
  :ensure t
  :defer t)

(provide '+lang-haskell)
