;;; +core.el -*- lexical-binding: t; -*-

;; TODO: set up elpa-mirror for reproducibility
;; TODO: switch from use-package to setup.el

;; Some useful constants, mostly borrowed from Doom Emacs.
(defconst EMACS30+   (> emacs-major-version 29))
(defconst HAS-GIT    (not (null (executable-find "git"))))
(defconst IS-MAC     (eq system-type 'darwin))
(defconst IS-LINUX   (eq system-type 'gnu/linux))
(defconst IS-WINDOWS (memq system-type '(cygwin windows-nt ms-dos)))
(defconst IS-BSD     (or IS-MAC (eq system-type 'berkeley-unix)))
(defconst IS-ANDROID (eq system-type 'android))
(defconst IS-TERMUX  (not (null (getenv "TERMUX_VERSION"))))
(defconst IS-WSL     (and IS-LINUX (string-match ".*microsoft.*"
                                                 operating-system-release)))

(when (< emacs-major-version 29)
  (error "Detected Emacs %s. Minimum supported version is 29.1."
         emacs-version))

;; A configuration macro for simplifying your init.el.
(use-package use-package
  :config
  (setopt use-package-expand-minimally t))

;; Simple package system for Emacs.
(use-package package
  :init
  ;; Add MELPA as an additional package repository.
  (setopt package-archives
          '(("gnu" . "https://elpa.gnu.org/packages/")
            ("nongnu" . "https://elpa.nongnu.org/nongnu/")
            ("melpa" . "https://melpa.org/packages/"))))

;; Keep the emacs directory clean.
(use-package no-littering
  :ensure t
  :config
  ;; Save backup files in a single directory.
  (no-littering-theme-backups)

  ;; Store customization settings separate from dotfiles.
  (setopt custom-file (no-littering-expand-etc-file-name "custom.el"))

  ;; Automatically load the custom file at the end of startup.
  (defun +core-load-custom-file-h ()
    "Load user customization information."
    (load custom-file t))
  (add-hook 'after-init-hook #'+core-load-custom-file-h -50))

;; Primitive `package-vc' integration into `use-package'.
(unless (or EMACS30+ (not HAS-GIT) (package-installed-p 'vc-use-package))
  (package-vc-install "https://github.com/slotThe/vc-use-package.git")
  (use-package vc-use-package))

;; Additional hooks for faster startup.
(use-package on
  :ensure t)

(use-package advice
  :defer t
  :init
  ;; Silence ad-handle-definition warnings.
  (setopt ad-redefinition-action 'accept))

;; Start the Emacs server.
(use-package server
  :if (or (display-graphic-p)
          IS-TERMUX)
  :defer 1
  :config
  (if (server-running-p)
      ;; Load random theme to indicate that another instance
      ;; is running the Emacs server.
      (eval-after-load '+ui
        '(+ui-load-random-theme))
    (server-start)))

(defun +core-update-config (&optional branch)
  "Download and apply my latest Emacs configuration."
  (interactive)
  (let* ((branch (or branch "master"))
         (default-directory user-emacs-directory)
         (src (concat "https://gitlab.com/axgfn/dotfiles/-/archive/" branch
                      "/dotfiles-" branch ".tar?path=config/dot_config/emacs"))
         (basename (concat "dotfiles-" branch "-config-dot_config-emacs"))
         (tarball (concat basename ".tar")))
    (url-copy-file src tarball :clobber)
    (with-current-buffer (find-file-noselect tarball)
      (let ((warning-suppress-types '((tar))))
        (tar-untar-buffer))
      (kill-current-buffer))
    (delete-file "pax_global_header")
    (rename-file (concat basename "/config/dot_config/emacs/early-init.el")
                 "early-init.el" :clobber)
    (rename-file (concat basename "/config/dot_config/emacs/init.el")
                 "init.el" :clobber)
    (delete-directory "lisp" :recursive)
    (rename-file (concat basename "/config/dot_config/emacs/exact_lisp")
                 "lisp")
    (delete-directory basename :recursive)
    (delete-file tarball)))

(provide '+core)
