;;; +llm.el -*- lexical-binding: t; -*-

;; TODO: integrate with the tabby ai coding assistant
;; TODO: investigate elisa package for rag queries

(defvar +llm-default-model
  "llama3.2:3b-instruct-q5_K_M"
  "Large Language Model to use by default.")

;; Tool for interacting with LLMs.
(use-package ellama
  :ensure t
  ;; BUG: must use normal bindings before it works from meow keypad
  :bind-keymap ("C-x l" . ellama-command-map)
  :config
  ;; Save sessions in `no-littering-var-directory'.
  (setopt ellama-sessions-directory (file-truename
                                     (file-name-concat
                                      no-littering-var-directory
                                      "ellama-sessions")))

  ;; Use the normal `fill-column' when formatting responses.
  (setopt ellama-long-lines-length fill-column)

  ;; Suppress warning when using ChatGPT provider.
  (setopt llm-warn-on-nonfree nil)

  (setopt ellama-provider
          (make-llm-ollama
           :chat-model +llm-default-model
           :embedding-model +llm-default-model))

  (defun +llm-init-ellama-providers-a ()
    (require 'llm-openai)
    (let ((openai-api-key (+auth-get-secret "openai.com" "apikey")))
      (setopt ellama-providers
              `(("gpt-3.5-turbo"
                 . (make-llm-openai
                    :key ,openai-api-key
                    :chat-model "gpt-3.5-turbo"))
                ("gpt-4"
                 . (make-llm-openai
                    :key ,openai-api-key
                    :chat-model "gpt-4"))
                ("gpt-4-turbo"
                 . (make-llm-openai
                    :key ,openai-api-key
                    :chat-model "gpt-4-turbo"))))))
  (advice-add 'ellama-provider-select :before #'+llm-init-ellama-providers-a))

;; A simple ChatGPT client.
(use-package gptel
  :ensure t
  :bind ( :map mode-specific-map
          ("l" . gptel-send))
  :config
  (setq gptel-default-mode 'org-mode)

  ;; Use Ollama backend by default.
  (when (executable-find "ollama")
    (defvar +llm-ollama-gptel-backend
      (gptel-make-ollama
       "Ollama"
       :host "localhost:11434"
       ;; Taken from ellama - auto discover available ollama models.
       :models (mapcar (lambda (s)
                         (car (split-string s)))
                       (seq-drop
                        (process-lines
                         (executable-find "ollama") "ls")
                        1))
       :stream t)
      "Local LLM backend for GPTel using Ollama.")

    (setq-default gptel-backend +llm-ollama-gptel-backend
                  gptel-model (intern +llm-default-model))))

(provide '+llm)
