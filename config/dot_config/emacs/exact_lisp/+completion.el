;;; +completion.el -*- lexical-binding: t; -*-

;; VERTical Interactive COmpletion - better minibuffer completion.
(use-package vertico
  :ensure t
  :hook (on-first-input . vertico-mode)
  :config
  (setopt vertico-resize nil
          vertico-cycle t
          vertico-count 17)

  ;; Use `consult-completion-in-region' if Vertico is enabled.
  ;; Otherwise use the default `completion--in-region' function.
  (setq completion-in-region-function
        (lambda (&rest args)
          (apply (if vertico-mode
                     #'consult-completion-in-region
                   #'completion--in-region)
                 args))))

(use-package vertico-grid
  :after vertico
  :config
  (unless (< (frame-width) 80)
    (setopt vertico-grid-min-columns 3)))

(use-package vertico-multiform
  :after vertico
  :config
  (add-to-list 'vertico-multiform-categories
               '(embark-keybinding grid))
  (vertico-multiform-mode))

(use-package vertico-directory
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy)
  :bind ( :map vertico-map
          ("C-w" . vertico-directory-delete-word)))

(use-package vertico-repeat
  :hook (minibuffer-setup . vertico-repeat-save))

;; Mouse support for Vertico.
(use-package vertico-mouse
  :after vertico
  :config (vertico-mouse-mode +1))

;; Match all of the space-separated components in any order.
(use-package orderless
  :ensure t
  :after vertico
  :init
  (setopt completion-styles '(orderless basic)
          completion-category-overrides '((file (styles basic partial-completion))))
  (setq completion-category-defaults nil))

;; Add annotations to minibuffer completion candidates.
(use-package marginalia
  :ensure t
  :hook (on-first-input . marginalia-mode)
  :bind ( :map minibuffer-local-map
          ("M-A" . marginalia-cycle)))

;; Add icons to completion candidates.
(use-package nerd-icons-completion
  :ensure t
  :after marginalia
  :config
  (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))

;; Enhance various completion functions.
(use-package consult
  :ensure t
  :defer t
  :bind (([remap apropos]                       . consult-apropos)
         ([remap bookmark-jump]                 . consult-bookmark)
         ([remap goto-line]                     . consult-goto-line)
         ([remap imenu]                         . consult-imenu)
         ([remap Info-search]                   . consult-info)
         ([remap locate]                        . consult-locate)
         ([remap load-theme]                    . consult-theme)
         ([remap man]                           . consult-man)
         ([remap recentf-open-files]            . consult-recent-file)
         ([remap switch-to-buffer]              . consult-buffer)
         ([remap switch-to-buffer-other-window] . consult-buffer-other-window)
         ([remap switch-to-buffer-other-frame]  . consult-buffer-other-frame)
         ([remap yank-pop]                      . consult-yank-pop))
  :init
  (advice-add #'multi-occur :override #'consult-multi-occur)
  ;; Use ripgrep when available.
  (when (executable-find "rg")
    (keymap-substitute mode-specific-map #'consult-grep #'consult-ripgrep))
  :config
  (setopt consult-narrow-key "<")

  (when IS-ANDROID
    (setopt consult-grep-args
            '("grep" (consult--grep-exclude-args)
              "--line-buffered -i -H -n -I -r"))
    (setq consult--grep-match-regexp
          "\\`\\(?:\\./\\)?\\([^\n:]+\\):\\([0-9]+\\)\\(:\\)"))

  ;; Disable live previews for these commands.
  (consult-customize
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-recent-file
   consult--source-project-recent-file
   :preview-key "C-SPC")

  ;; Delay live previews for these commands.
  (consult-customize
   consult-theme
   :preview-key '("C-SPC" :debounce 0.5 any)))

;; Run context sensitive commands on point, both in minibuffers
;; and normal buffers.
(use-package embark
  :ensure t
  :defer t
  :bind (([remap describe-bindings] . embark-bindings)
         ("C-;" . embark-act)
         :map minibuffer-local-map
         ("C-;" . embark-act)
         ("C-c C-;" . embark-export)
         ("C-c C-l" . embark-collect)
         :map embark-identifier-map
         ("R" . eglot-rename)
         ("I" . eglot-code-actions))
  :init
  (setopt embark-indicators '(embark-minimal-indicator
                              embark-highlight-indicator
                              embark-isearch-highlight-indicator))

  ;; Replace the key help with a completing-read interface.
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  (push 'embark--allow-edit
        (alist-get 'eglot-rename embark-target-injection-hooks))
  (push 'embark--ignore-target
        (alist-get 'eglot-code-actions embark-target-injection-hooks)))

(use-package embark-consult
  :ensure t
  :demand t
  :after (embark consult)
  :hook (embark-collect-mode . consult-preview-at-point-mode))

;; Completion Overlay Region FUnction - enhanced `completion-at-point'.
(use-package corfu
  :ensure t
  :hook (on-first-input . global-corfu-mode)
  :bind ( :map corfu-map
          ("C-SPC"      . corfu-insert-separator)
          ("RET"        . nil)
          ("TAB"        . corfu-next)
          ([tab]        . corfu-next)
          ("S-TAB"      . corfu-previous)
          ([backtab]    . corfu-previous)
          ("S-<return>" . corfu-insert))
  :config
  (setopt corfu-cycle t
          corfu-auto t
          corfu-auto-prefix 2
          corfu-auto-delay 0.5
          corfu-quit-at-boundary 'separator
          corfu-preselect 'prompt
          corfu-preview-current 'insert)

  ;; Show candidate documentation in echo area.
  (corfu-echo-mode)

  ;; Sort completions by most recently used.
  (corfu-history-mode)
  (eval-after-load 'savehist
    '(add-to-list 'savehist-additional-variables 'corfu-history))

  ;; Quit completion when exiting insert state.
  (when (featurep '+meow)
    (add-hook 'meow-insert-exit-hook #'corfu-quit)))

;; Display Corfu completions using overlays in terminal Emacs,
;; instead of child frames.
(use-package corfu-terminal
  :ensure t
  :after corfu
  :config
  (unless (display-graphic-p)
    (corfu-terminal-mode +1)))

;; Icons for Corfu via nerd-icons.
(use-package nerd-icons-corfu
  :ensure t
  :after corfu
  :config
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

;; Additional backends for `completion-at-point'.
(use-package cape
  :ensure t
  :after corfu
  ;; Bind dedicated completion commands.
  :bind (("C-c i p"  . completion-at-point) ;; capf
         ("C-c i t"  . complete-tag)        ;; etags
         ("C-c i d"  . cape-dabbrev)        ;; or dabbrev-completion
         ("C-c i h"  . cape-history)
         ("C-c i f"  . cape-file)
         ("C-c i k"  . cape-keyword)
         ("C-c i y"  . cape-elisp-symbol)
         ("C-c i e"  . cape-elisp-block)
         ("C-c i a"  . cape-abbrev)
         ("C-c i l"  . cape-line)
         ("C-c i w"  . cape-dict)
         ("C-c i :"  . cape-emoji)
         ("C-c i \\" . cape-tex)
         ("C-c i _"  . cape-tex)
         ("C-c i ^"  . cape-tex)
         ("C-c i &"  . cape-sgml)
         ("C-c i r"  . cape-rfc1345))
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-elisp-block)
  ;; (add-to-list 'completion-at-point-functions #'cape-history)
  ;; (add-to-list 'completion-at-point-functions #'cape-keyword)
  ;; (add-to-list 'completion-at-point-functions #'cape-tex)
  ;; (add-to-list 'completion-at-point-functions #'cape-sgml)
  ;; (add-to-list 'completion-at-point-functions #'cape-rfc1345)
  ;; (add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;; (add-to-list 'completion-at-point-functions #'cape-ispell)
  ;; (add-to-list 'completion-at-point-functions #'cape-dict)
  ;; (add-to-list 'completion-at-point-functions #'cape-symbol)
  ;; (add-to-list 'completion-at-point-functions #'cape-line)
  )

(provide '+completion)
