;;; +lang-clojure.el -*- lexical-binding: t; -*-

;; Major mode for Clojure code.
(use-package clojure-mode
  :ensure t
  :defer t)

;; Major mode for Clojure code.
(use-package clojure-ts-mode
  :ensure t
  :defer t
  :config
  ;; Disable markdown_inline grammar for docstrings.
  (setopt clojure-ts-ensure-grammars nil))

;; Clojure Interactive Development Environment that Rocks.
(use-package cider
  :ensure t
  :after (:any clojure-mode clojure-ts-mode)
  :config
  (with-eval-after-load 'clojure-mode
    (keymap-substitute clojure-mode-map    #'cider-jack-in-clj #'cider-jack-in-universal))
  (with-eval-after-load 'clojure-ts-mode
    (keymap-substitute clojure-ts-mode-map #'cider-jack-in-clj #'cider-jack-in-universal)))

(provide '+lang-clojure)
