;;; +popup.el -*- lexical-binding: t; -*-

;; Undo and redo window management actions.
(use-package winner
  :hook (on-first-buffer . winner-mode))

;; Quickly switch windows.
(use-package ace-window
  :ensure t
  :bind (([remap other-window] . ace-window)))

;; Manage "popup" windows.
(use-package popper
  :ensure t
  :hook (on-init-ui . popper-mode)
  :bind (("C-`" . popper-toggle)
         ("M-`" . popper-cycle)
         ("C-~" . popper-toggle-type))
  :config
  (setopt popper-display-control 'user
          popper-mode-line nil
          popper-window-height 19)

  (defun +popup-add-rules (conditions action)
    "Add rules to display buffers matching CONDITIONS using ACTION.
CONDITIONS is a list of conditions, each of which may be a regular
expression to match buffer names against, or a symbol to match
major modes against.
ACTION is added directly to each `display-buffer-alist' entry."
    (dolist (condition conditions)
      (let ((condition* (if (symbolp condition)
                            (cons 'derived-mode condition)
                          condition)))
        (add-to-list 'popper-reference-buffers
                     condition
                     :append)
        (add-to-list 'display-buffer-alist
                     (cons condition* action)
                     :append))))

  ;; Rules for matching popup buffers.
  (+popup-add-rules '("^\\*Messages\\*$"
                      "^\\*\\(?:Wo\\)?Man "
                      help-mode
                      "^\\*eldoc"
                      "^\\*Flymake diagnostics"
                      "^ \\*Agenda Commands\\*$"
                      "^\\*Calendar"
                      "^\\*chezmoi"
                      "^\\*detached-list"
                      "^\\*devdocs"
                      "^\\*envrc"
                      "^ellama"
                      "^\\*cider-"
                      "^\\*ielm"
                      "^\\*eat"    eat-mode    "-eat\\*$"
                      "^\\*eshell" eshell-mode "-eshell\\*$"
                      "^\\*shell"  shell-mode  "-shell\\*$"
                      "^Trash Can")
                    '((popper-select-popup-at-bottom)))

  ;; Don't automatically select these popups.
  (+popup-add-rules '("^\\*Warnings"
                      "^\\*Backtrace"
                      compilation-mode
                      "^\\*Ledger \\(Error\\|Report\\)")
                    '((popper-display-popup-at-bottom)))

  ;; Stack smaller youtube-dl windows at the bottom of the frame.
  (+popup-add-rules '("^\\*youtube-dl" "^\\*yt-dlp")
                    '((display-buffer-at-bottom)
                      (dedicated . t)
                      (window-height . 6))))

(provide '+popup)
