;;; +snippets.el -*- lexical-binding: t; -*-

;; Yet another snippet extension for Emacs.
(use-package yasnippet
  :ensure t
  :hook (on-first-buffer . yas-global-mode)
  :commands (yas-insert-snippet yas-visit-snippet-file)
  :init (setq yas-verbosity 2))

;; Collection of yasnippet snippets.
(use-package yasnippet-snippets
  :ensure t
  :after yasnippet)

;; A consulting-read interface for yasnippet.
(use-package consult-yasnippet
  :ensure t
  :commands (consult-yasnippet consult-yasnippet-visit-snippet-file)
  :bind (([remap yas-insert-snippet] . consult-yasnippet)
         ([remap yas-visit-snippet-file] . consult-yasnippet-visit-snippet-file)))

(provide '+snippets)
