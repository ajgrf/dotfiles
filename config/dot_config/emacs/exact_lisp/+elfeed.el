;;; +elfeed.el -*- lexical-binding: t; -*-

;; An Emacs Atom/RSS feed reader.
(use-package elfeed
  :ensure t
  :bind ( :map mode-specific-map
          ("R" . +elfeed)
          :map elfeed-search-mode-map
          ("d" . +elfeed-search-save-enclosure)
          ("Y" . +elfeed-search-youtube-dl)
          :map elfeed-show-mode-map
          ("Y" . +elfeed-show-youtube-dl)
          ([remap elfeed-kill-buffer] . delete-window))
  :init
  (defun +elfeed ()
    "Open elfeed in a dedicated tab."
    (interactive)
    (let ((workspace "*elfeed*")
          (tabs (mapcar (lambda (tab)
                          (alist-get 'name tab))
                        (tab-bar-tabs))))
      (if (member workspace tabs)
          (tab-bar-switch-to-tab workspace)
        (tab-bar-new-tab)
        (tab-bar-rename-tab workspace)
        (elfeed)
        (set-window-prev-buffers nil nil)
        (set-window-parameter nil 'quit-restore
                              `(tab tab ,(selected-window) ,(current-buffer))))))
  :config
  (require 'xdg)
  (setopt elfeed-enclosure-default-dir (concat (or (xdg-user-dir "DESKTOP")
                                                   "~/Desktop")
                                               "/")
          elfeed-show-entry-switch #'pop-to-buffer
          elfeed-show-entry-delete #'delete-window
          elfeed-search-filter "@3-months-ago +unread ")
  (setq elfeed-show-enclosure-filename-function #'+elfeed-enclosure-filename)

  ;; Display entries below the entry list.
  (add-to-list 'display-buffer-alist
               '("^\\*elfeed-entry"
                 (display-buffer-below-selected)
                 (window-height . 0.75))
               :append)

  (defun +elfeed-enclosure-filename (entry url-enclosure)
    "Return the local filename for an enclosure (podcast)."
    (let* ((title (elfeed-entry-title entry))
           (url-path (url-unhex-string
                      (car (url-path-and-query (url-generic-parse-url
                                                url-enclosure)))))
           (extension (file-name-extension url-path)))
      (replace-regexp-in-string
       "[?*:<>|\"\000-\037]" "-"
       (concat title "." extension))))

  (defun +elfeed-search-youtube-dl ()
    "Run 'youtube-dl' on the selected elfeed-entry's URL."
    (interactive)
    (let ((default-directory elfeed-enclosure-default-dir)
          (entries (elfeed-search-selected)))
      (+youtube-dl-run-in-popup (mapcar #'elfeed-entry-link entries))
      (elfeed-search-untag-all-unread)))

  (defun +elfeed-show-youtube-dl ()
    "Run 'youtube-dl' on the current entry's URL."
    (interactive)
    (let ((default-directory elfeed-enclosure-default-dir)
          (link (elfeed-entry-link elfeed-show-entry)))
      (when link
        (+youtube-dl-run-in-popup (list link)))))

  (defun +elfeed-search-save-enclosure (&optional multi)
    "Offer to save enclosure(s) of the selected elfeed-entry.
If MULTI (prefix-argument) is nil, save a single one, otherwise,
offer to save a range of enclosures."
    (interactive "P")
    (let ((entries (elfeed-search-selected)))
      (cl-loop for entry in entries
               do (if multi
                      (elfeed-show-save-enclosure-multi entry)
                    (elfeed-show-save-enclosure-single entry))))))

;; Configure elfeed with one or more org-mode files.
(use-package elfeed-org
  :ensure t
  :after elfeed
  :preface (setopt rmh-elfeed-org-files
                   (list (expand-file-name "elfeed.org" org-directory)))
  :config (elfeed-org))

;; Sync feeds with Miniflux.
(use-package elfeed-protocol
  :ensure t
  :after elfeed-org
  :config
  (setopt elfeed-protocol-enabled-protocols '(fever))

  ;; Make elfeed-org autotags rules works with elfeed-protocol.
  (setq elfeed-protocol-feeds
        `(("fever+https://axgfn@reader.griffin1.org"
           :api-url "https://reader.griffin1.org/fever/"
           :use-authinfo t)))
  (setq elfeed-protocol-feeds (append elfeed-protocol-feeds elfeed-feeds))

  ;; Enable two-way sync of read state.
  ;; https://github.com/fasheng/elfeed-protocol/issues/28
  (defun +elfeed/sync ()
    "Sync feeds with Miniflux, including read state."
    (interactive)
    (mark-whole-buffer)
    (cl-loop for entry in (elfeed-search-selected)
             do (elfeed-untag-1 entry 'unread))
    (elfeed-search-update--force)
    (elfeed-protocol-fever-reinit "https://axgfn@reader.griffin1.org")
    (deactivate-mark))
  (keymap-substitute elfeed-search-mode-map #'elfeed-search-fetch #'+elfeed/sync)

  (elfeed-protocol-enable))

(provide '+elfeed)
