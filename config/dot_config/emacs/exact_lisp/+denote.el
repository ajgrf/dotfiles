;;; +denote.el -*- lexical-binding: t; -*-

(defvar +denote-org-agenda-files org-agenda-files
  "Initial value of `org-agenda-files' before adding Denote files.")

;; Simple notes with an efficient file-naming scheme.
(use-package denote
  :ensure t
  :defer t
  :init
  (setq denote-directory (file-name-concat org-directory "notes/"))

  (defun +denote-agenda-files-update (&rest _)
    "Add notes with KEYWORD to `org-agenda-files'."
    (interactive)
    (setq org-agenda-files
          (append +denote-org-agenda-files
                  (list org-default-notes-file)
                  (directory-files denote-directory t "_agenda"))))
  (advice-add 'org-agenda-files :before #'+denote-agenda-files-update)
  :config
  (setopt denote-date-prompt-use-org-read-date t
          denote-known-keywords nil
          denote-dired-directories-include-subdirectories t
          denote-rename-buffer-format "%t%b"
          denote-rename-confirmations '(add-front-matter modify-file-name))

  ;; Highlight Denote file name components in dired buffers.
  (add-hook 'dired-mode-hook #'denote-dired-mode-in-directories)

  (defun +denote-title-in-modeline-h ()
    "Display Denote title in the mode-line instead of the filename."
    (when-let ((buffer (current-buffer))
               (file (buffer-file-name buffer))
               ((denote-file-has-identifier-p file))
               (new-name (denote-rename-buffer--format (or buffer (current-buffer))))
               ((not (string-blank-p new-name))))
      (setq-local doom-modeline-buffer-file-name-style 'buffer-name)))
  (add-hook 'denote-after-new-note-hook #'+denote-title-in-modeline-h)
  (add-hook 'denote-after-rename-file-hook #'+denote-title-in-modeline-h)
  (add-hook 'find-file-hook #'+denote-title-in-modeline-h)

  ;; Name buffers after a note's title instead of its filename.
  (denote-rename-buffer-mode)

  ;; https://protesilaos.com/emacs/denote#h:bbc7a769-19e8-4598-a2b7-06e1d673ae80

  (require 'denote-sort)

  (defun +denote--note-has-no-contents-p (file)
    "Return non-nil if FILE is an empty note.
This means that FILE conforms with `denote-file-is-note-p' and either
has no contents or has only the front matter."
    (and (denote-file-is-note-p file)
         (or (denote--file-with-temp-buffer file
               (re-search-forward "^$" nil t)
               (if (re-search-forward "[^\s\t\n\r]+" nil t)
                   nil
                 t))
             ;; This must come later because here we consider a file
             ;; "empty" even if it only has front matter.
             (denote--file-empty-p file))))

  (defun +denote-sort-dired-empty-files (files-matching-regexp sort-by-component reverse)
    "Like `denote-sort-dired' but only cover empty files.
Empty files are those that satisfy `+denote--note-has-no-contents-p'."
    (interactive
     (append (list (denote-files-matching-regexp-prompt)) (denote-sort-dired--prompts)))
    (let ((component (or sort-by-component
                         denote-sort-dired-default-sort-component
                         'identifier))
          (reverse-sort (or reverse
                            denote-sort-dired-default-reverse-sort
                            nil)))
      (if-let* ((default-directory (denote-directory))
                (files (denote-sort-get-directory-files files-matching-regexp component reverse-sort))
                (empty-files (seq-filter #'+denote--note-has-no-contents-p files))
                ;; NOTE 2023-12-04: Passing the FILES-MATCHING-REGEXP as
                ;; buffer-name produces an error if the regexp contains a
                ;; wildcard for a directory. I can reproduce this in emacs
                ;; -Q and am not sure if it is a bug. Anyway, I will report
                ;; it upstream, but even if it is fixed we cannot use it
                ;; for now (whatever fix will be available for Emacs 30+).
                (denote-sort-dired-buffer-name (format "Denote sort `%s' by `%s'" files-matching-regexp component))
                (buffer-name (format "Denote sort by `%s' at %s" component (format-time-string "%T"))))
          (let ((dired-buffer (dired (cons buffer-name (mapcar #'file-relative-name empty-files)))))
            (setq denote-sort--dired-buffer dired-buffer)
            (with-current-buffer dired-buffer
              (setq-local revert-buffer-function
                          (lambda (&rest _)
                            (kill-buffer dired-buffer)
                            (denote-sort-dired files-matching-regexp component reverse-sort))))
            ;; Because of the above NOTE, I am printing a message.  Not
            ;; what I want, but it is better than nothing...
            (message denote-sort-dired-buffer-name))
        (message "No matching files for: %s" files-matching-regexp))))

  (defun +denote-sort-dired-without-empty-files (files-matching-regexp sort-by-component reverse)
    "Like `denote-sort-dired' but only cover non-empty files.
Empty files are those that satisfy `+denote--note-has-no-contents-p'."
    (interactive
     (append (list (denote-files-matching-regexp-prompt)) (denote-sort-dired--prompts)))
    (let ((component (or sort-by-component
                         denote-sort-dired-default-sort-component
                         'identifier))
          (reverse-sort (or reverse
                            denote-sort-dired-default-reverse-sort
                            nil)))
      (if-let* ((default-directory (denote-directory))
                (files (denote-sort-get-directory-files files-matching-regexp component reverse-sort))
                (empty-files (seq-remove #'+denote--note-has-no-contents-p files))
                ;; NOTE 2023-12-04: Passing the FILES-MATCHING-REGEXP as
                ;; buffer-name produces an error if the regexp contains a
                ;; wildcard for a directory. I can reproduce this in emacs
                ;; -Q and am not sure if it is a bug. Anyway, I will report
                ;; it upstream, but even if it is fixed we cannot use it
                ;; for now (whatever fix will be available for Emacs 30+).
                (denote-sort-dired-buffer-name (format "Denote sort `%s' by `%s'" files-matching-regexp component))
                (buffer-name (format "Denote sort by `%s' at %s" component (format-time-string "%T"))))
          (let ((dired-buffer (dired (cons buffer-name (mapcar #'file-relative-name empty-files)))))
            (setq denote-sort--dired-buffer dired-buffer)
            (with-current-buffer dired-buffer
              (setq-local revert-buffer-function
                          (lambda (&rest _)
                            (kill-buffer dired-buffer)
                            (denote-sort-dired files-matching-regexp component reverse-sort))))
            ;; Because of the above NOTE, I am printing a message.  Not
            ;; what I want, but it is better than nothing...
            (message denote-sort-dired-buffer-name))
        (message "No matching files for: %s" files-matching-regexp))))

  (defun +denote-sort-dired-all-empty-files ()
    "List all empty files in a Dired buffer.
This is the same as calling `+denote-sort-dired' with a
FILES-MATCHING-REGEXP of \".*\"."
    (declare (interactive-only t))
    (interactive)
    (let* ((other-prompts (denote-sort-dired--prompts))
           (sort-key (nth 1 other-prompts))
           (reverse (nth 2 other-prompts)))
      (funcall-interactively #'+denote-sort-dired-empty-files ".*" sort-key reverse)))

  (defun +denote-sort-dired-without-all-empty-files ()
    "List all non-empty files in a Dired buffer.
This is the same as calling `+denote-sort-dired' with a
FILES-MATCHING-REGEXP of \".*\"."
    (declare (interactive-only t))
    (interactive)
    (let* ((other-prompts (denote-sort-dired--prompts))
           (sort-key (nth 1 other-prompts))
           (reverse (nth 2 other-prompts)))
      (funcall-interactively #'+denote-sort-dired-without-empty-files ".*" sort-key reverse))))

;; Convenience functions for daily journaling.
(use-package denote-journal-extras
  :after denote
  :config
  (setopt denote-journal-extras-directory nil
          denote-journal-extras-title-format "%Y-%m-%d %a %H:%M")

  (defun +denote-completing-read-first (_prompt collection &optional _predicate _require-match _initial-input _hist _def _inherit-input-method)
    "Non-interactively return the first item of COLLECTION."
    (when collection
      (car collection)))

  (defun +denote-update-default-notes-file-h ()
    "Set `org-default-notes-file' to today's daily journal."
    (interactive)
    (let ((completing-read-function #'+denote-completing-read-first))
      (setq org-default-notes-file
            (save-window-excursion
              (denote-journal-extras-new-or-existing-entry)
              (buffer-file-name (current-buffer))))))
  (add-hook 'midnight-hook '+denote-update-default-notes-file-h)

  (+denote-update-default-notes-file-h))

;; Use Consult in tandem with Denote.
(use-package consult-denote
  :ensure t
  :after denote
  :config
  (consult-denote-mode +1))

(defun +denote-rename-file ()
  "Dispatch a file rename command based on context."
  (interactive)
  (call-interactively
   (cond (denote-dired-mode
          #'denote-dired-rename-files)
         ((denote-file-is-note-p (buffer-file-name))
          #'denote-rename-file)
         (t
          #'crux-rename-file-and-buffer))))

(provide '+denote)
