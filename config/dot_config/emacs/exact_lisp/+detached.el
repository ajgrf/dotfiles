;;; +detached.el -*- lexical-binding: t; -*-

;; Launch and manage detached process with `dtach'.
(use-package detached
  :ensure t
  :bind (;; Replace `async-shell-command' with `detached-shell-command'
         ([remap async-shell-command] . detached-shell-command)
         ;; Replace `compile' with `detached-compile'
         ([remap compile] . detached-compile)
         ([remap recompile] . detached-compile-recompile)
         ;; Replace built in completion of sessions with `consult'
         ([remap detached-open-session] . detached-consult-session))
  :init
  (detached-init)
  (setopt detached-detach-key (kbd "C-\\"))
  :config
  ;; HACK: work around bad defcustom type specs
  (setq detached-session-context-lines 10
        detached-terminal-data-command system-type)

  (setopt detached-notification-function #'detached-state-transitionion-echo-message
          detached-list-filters `(("Today" . ((detached-list-narrow-unique)
                                              (detached-list-narrow-after-time "12h")))))
  (setq detached-metadata-annotators-alist '((branch . detached--metadata-git-branch)))

  (setopt detached-list-config
          '((:name "Command" :function detached-list--command-str :length 40)
            (:name "Status" :function detached-list--status-str :length 8)
            (:name "Directory" :function detached--working-dir-str :length 24 :face detached-working-dir-face)
            (:name "Metadata" :function detached--metadata-str :length 16 :face detached-metadata-face)
            (:name "Duration" :function detached--duration-str :length 8 :face detached-duration-face)
            (:name "Host" :function detached--host-str :length 16 :face detached-host-face)
            (:name "Created" :function detached--creation-str :length 16 :face detached-creation-face))))

(provide '+detached)
