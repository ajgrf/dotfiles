;;; +git.el -*- lexical-binding: t; -*-

;; Built-in version control support.
(use-package vc
  :init
  (setopt vc-follow-symlinks t
          vc-ignore-dir-regexp (format "%s\\|%s\\|%s"
                                       vc-ignore-dir-regexp
                                       tramp-file-name-regexp
                                       "[/\\\\]node_modules")
          vc-handled-backends '(SVN Git Hg)))

;; It's Magit! A Git Porcelain inside Emacs.
(use-package magit
  :ensure t
  :defer t
  :config
  ;; Save buffers in the current repository before Magit commands.
  (setopt magit-save-repository-buffers 'dontask)

  ;; Highlight word-level differences within diff hunks.
  (setopt magit-diff-refine-hunk t)

  ;; Open most Magit buffers in the same window.
  (setopt magit-display-buffer-function
          #'magit-display-buffer-same-window-except-diff-v1)

  ;; Always show unpulled and unpushed commits.
  (add-to-list 'magit-section-initial-visibility-alist '(unpulled . show))
  (add-to-list 'magit-section-initial-visibility-alist '(unpushed . show))

  ;; Taken from Doom Emacs.
  (defun +git-start-in-insert-state-maybe-h ()
    "Start git-commit-mode in insert state if in a blank commit message,
otherwise in default state."
    (when (and (bound-and-true-p meow-global-mode)
               (bobp) (eolp))
      (meow-insert-mode)))
  (add-hook 'git-commit-setup-hook #'+git-start-in-insert-state-maybe-h)

  (defun +git-restore-magit-bindings-h ()
    "Restore magit bindings after customizations."
    (keymap-local-unset "SPC"))
  (add-hook 'magit-status-mode-hook #'+git-restore-magit-bindings-h))

;; Fix bookmarks to magit buffers by making sure magit is loaded.
(use-package magit-section
  :defer t
  :config
  (require 'magit))

;; List git repositories.
(use-package magit-repos
  :defer t
  :config
  (defun +git-set-magit-repository-directories-a ()
    "Set `magit-repository-directories' with mr (myrepos)."
    (let* ((output (shell-command-to-string "mr --directory ~ list"))
           (lines (split-string output "\n\n"))
           (repos (mapcar (lambda (s)
                            `(,(string-remove-prefix "mr list: " s) . 0))
                          (butlast lines))))
      (setopt magit-repository-directories repos)))
  (advice-add 'magit-list-repositories :before
              #'+git-set-magit-repository-directories-a))

;; Show source file TODOs in Magit.
(use-package magit-todos
  :ensure t
  :after magit
  :config
  ;; BUG: todo keyword faces are the wrong color sometimes
  (magit-todos-mode +1))

;; Ignore case when grepping magit-log.
(use-package transient
  :defer t
  :config
  (add-to-list 'transient-levels
               '(magit-log (transient:magit-log:--regexp-ignore-case . 4)))
  (add-to-list 'transient-values
               '(magit-log:magit-log-mode
                 "-n256" "--regexp-ignore-case" "--graph" "--decorate")))

;; Direct access to the core SQLite3 API.
(use-package sqlite3
  :ensure t
  :unless (and (fboundp 'sqlite-available-p)
               (sqlite-available-p))
  :defer t)

;; Access Git forges from Magit.
(use-package forge
  :ensure t
  :demand t
  :after magit
  :config
  (setopt forge-topic-list-limit -5)

  ;; Let magit-annex take the "@" key.
  (keymap-set magit-mode-map "#" #'forge-dispatch)
  (transient-append-suffix 'magit-dispatch "!"
    '("#" "Forge" forge-dispatch)))

;; Mode for easy editing of git-annex'd files.
(use-package git-annex
  :ensure t
  :after dired)

;; Control git-annex from Magit.
(use-package magit-annex
  :ensure t
  :after (magit forge)
  :config
  (transient-insert-suffix 'magit-dispatch "i"
    '("@" "Annex" magit-annex-dispatch)))

;; Major modes for editing Git configuration files.
(use-package git-modes
  :ensure t
  :defer t)

;; Walk through git revisions of a file.
(use-package git-timemachine
  :ensure t
  :defer t)

;; Highlight uncommitted changes.
(use-package diff-hl
  :ensure t
  :hook ((on-first-file       . global-diff-hl-mode)
         (global-diff-hl-mode . diff-hl-flydiff-mode)
         (magit-pre-refresh   . diff-hl-magit-pre-refresh)
         (magit-post-refresh  . diff-hl-magit-post-refresh)))

;; Wrapper for difftastic.
(use-package difftastic
  :ensure t
  :bind ( :map magit-blame-read-only-mode-map
          ("D" . difftastic-magit-show)
          ("S" . difftastic-magit-show))
  :init
  (eval-after-load 'magit-diff
    '(transient-append-suffix 'magit-diff '(-1 -1)
       [("D" "Difftastic diff (dwim)" difftastic-magit-diff)
        ("S" "Difftastic show" difftastic-magit-show)])))

(provide '+git)
