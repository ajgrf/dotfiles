# don't use colors outside of my theme
bleopt term_index_colors=16

# up/down don't go to prev/next line
ble-bind -m emacs -f up history-prev
ble-bind -m emacs -f down history-next
