{:user
 ;; Use XDG basedirs for local Maven cache.
 ;; https://wiki.archlinux.org/title/Clojure#m2_repo_location
 {:local-repo #=(eval (str (System/getenv "XDG_CACHE_HOME") "/m2/repository"))
  :repositories {"local" {:url #=(eval (str "file://" (System/getenv "XDG_CACHE_HOME") "/m2/repository"))
                          :releases {:checksum :ignore}}}}}
