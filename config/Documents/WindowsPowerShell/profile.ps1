# Use Emacs-style keybindings
Set-PSReadLineOption -EditMode Emacs

# PageUp/PageDown search the history for whatever you've typed so far
Set-PSReadLineKeyHandler -Key PageUp -Function HistorySearchBackward
Set-PSReadLineKeyHandler -Key PageDown -Function HistorySearchForward

# Change Write-Progress popup colors
$Host.PrivateData.ProgressForegroundColor = "Black";
$Host.PrivateData.ProgressBackgroundColor = "DarkBlue";
